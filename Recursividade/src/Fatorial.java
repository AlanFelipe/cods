import java.util.Scanner;

public class Fatorial {
    public int fatorial(int num){
        if(num == 0){
            return  1;
        } else {
            return num * fatorial(num-1);
        }
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        Fatorial fat = new Fatorial();
        System.out.println("Informe um numero");
        int num = input.nextInt();
        System.out.println(fat.fatorial(num));
    }
}
