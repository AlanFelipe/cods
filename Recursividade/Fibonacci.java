import java.util.Scanner;

public class Fibonacci {
    public int fibo(int num){
        if(num < 2){
            return 1;
        } else {
            return fibo(num-1) + fibo(num-2);
        }
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        Fibonacci fibo = new Fibonacci();
        System.out.print("Informe a quantidade de termos: ");
        int quant = input.nextInt();

        for (int i = 0; i < quant; i++) {
            System.out.print(fibo.fibo(i) + " ");
        }
    }
}
