package Arvore.ArvoreBinaria;

import Arvore.IPrint;
import Arvore.No;
import org.omg.CORBA.NO_IMPLEMENT;

public class ArvoreBinaria<T extends Comparable> {
    //raiz da arvore
    private No<T> raiz;

    //construtor
    public ArvoreBinaria() {
        this.raiz = null;

    }

    public void inserir(T dado){
        //chama o metodo de inserir passando a raiz como noAtual
        inserir(raiz,dado);
    }

    //inserir elementos na arvore
    private void inserir(No<T> noAtual, T dado){
        //veririfa se a raiz esta vazia
        //se a raiz estiver vazia, instacia o primeiro dado na raiz
        if(estahVazia()){
            this.raiz = new No<T>(dado);
            System.out.println(dado + " inserido na raiz");
            } else {
            //se a raiz não estiver vazia, é criado um novoNo
            //se o valor do dado for MENOR que o valor do noAtual verifica se o lado ESQUERDO do noAtual esta vazio
            //se o lado ESQUERDO do noAtual estiver vazio é estanciado o novoNo a ESQUERDA do noAtual
            //se o lado ESQUERDO não estiver vazio é chamado o metodo de inserir com o valor do novoNo como noAtual
            No<T> novoNo = new No<T>(dado);
            if(dado.compareTo(noAtual.getInfo()) < 0){
                if(noAtual.getEsq() == null){
                    noAtual.setEsq(novoNo);
                    System.out.println(dado + " foi inserido a esquerda de " + noAtual.getInfo());
                } else {
                    inserir(noAtual.getEsq(), dado);
                }
                //se o valor do dado for MAIOR que o valor do noAtual verifica se o lado DIREITO do noAtual esta vazio
                // se o lado DIREITO do noAtual estiver vazio é estanciado o novoNo a DIREITA do noAtual
                // se o lado DIREITO não estiver vazio é chamado o metodo de inserir com o valor do novoNo como noAtual
            } else if(dado.compareTo(noAtual.getInfo()) > 0){
                if(noAtual.getDir() == null){
                    noAtual.setDir(novoNo);
                    System.out.println(dado + " foi inserido a direita de " + noAtual.getInfo());
                } else {
                    inserir(noAtual.getDir(), dado);
                }
            }
        }
    }

    public void remover(T dado){
        if(!estahVazia()){
            if(this.raiz.getInfo() == dado &&
                    this.raiz.getEsq()  == null &&
                    this.raiz.getDir()  == null){
                this.raiz = null;
            } else {
                remover(this.raiz, null, dado);
            }
        }
    }

    private void remover(No<T> atual, No<T>anterior, T dado){
        //se o no atual for diferente do dado a ser removido, fazer a busca até encontrar o dado
        if(atual.getInfo().compareTo(dado) != 0){
            if(atual.getInfo().compareTo(dado) > 0){
                if(atual.getEsq() != null){
                    remover(atual.getEsq(), atual, dado);
                }
            } else {
                if(atual.getDir() != null){
                    remover(atual.getDir(), atual, dado);
                }
            }
        }
        //se o no atual for igual ao dado a ser removido
        else if(atual.getInfo().compareTo(dado) == 0){
            //remoção caso seja no folha
            if((atual.getEsq() == null) && (atual.getDir() == null)){
                //se o no a ser removido for a esquerda no pai
                if(anterior.getEsq().getInfo() == dado){
                    anterior.setEsq(null);
                } else {
                    anterior.setDir(null);
                }
            }
            else if((atual.getEsq() == null) || (atual.getDir() == null)){
                //remocao caso tenha so 1 filho
                if(atual.getEsq() != null){
                    if (atual.getInfo().compareTo(this.raiz.getInfo()) > 0) {
                        anterior.setDir(atual.getDir());
                    } else {
                        anterior.setEsq(atual.getEsq());
                    }
                } else if(atual.getDir() != null) {
                    if (atual.getInfo().compareTo(this.raiz.getInfo()) > 0) {
                        anterior.setDir(atual.getEsq());
                    } else {
                        anterior.setEsq(atual.getDir());
                    }
                }
            } else {
                //remocao caso tenha 2 filhos, caso simples quando a direita da esquerda do pai for nula
                if(atual.getInfo().compareTo(this.raiz.getInfo()) > 0){
                    if(atual.getDir().getEsq() == null){
                        anterior.setEsq(atual.getEsq());
                        atual.getEsq().setDir(atual.getDir());
                    }
                } else {
                    if(atual.getEsq().getDir() == null){
                        anterior.setEsq(atual.getEsq());
                        atual.getEsq().setDir(atual.getDir());
                    }
                }
                //remoção caso tenha 2 filhos
            }
        }
    }

    public void imprimirCrescente(IPrint print){
        imprimirLDR(this.raiz, print);
    }
    public void imprimirDecrescente(IPrint print){
        imprimirRDL(this.raiz, print);
    }

    private void imprimirLDR(No<T> atual, IPrint print){
        if(atual != null){
            imprimirLDR(atual.getEsq(), print);
            print.print(atual.getInfo());
            imprimirLDR(atual.getDir(),print);
        }
    }

    private void imprimirRDL(No<T> atual, IPrint print){
        if(atual != null){
            imprimirRDL(atual.getDir(),print);
            print.print(atual.getInfo());
            imprimirRDL(atual.getEsq(), print);
        }
    }

    public int altura(){
        return altura(this.raiz);
    }

    private int altura(No<T> atual){
        if(atual != null){
           int contEsq = altura(atual.getEsq());
           int contDir = altura(atual.getDir());

           if(contEsq > contDir){
               return 1 + contEsq;
           } else {
               return 1 + contDir;
           }
       }
       return 0;
    }

    public boolean estahVazia(){
        return this.raiz == null;
    }


    public boolean estahCheia(){ //(Math.pow(2,altura()) - 1)
        if(!estahVazia()){
            if ((Math.pow(2,altura()) - 1) == quantNos(this.raiz)){
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

     private int quantNos(No<T> atual){
        if(atual != null){
            int contEsq = quantNos(atual.getEsq());
            int contDir = quantNos(atual.getDir());
            return 1 + (contDir + contEsq);
        }
        return 0;
    }

    public void no(){
        noBalanceado(this.raiz);
    }

    private boolean noBalanceado(No<T> atual){
        if(!estahVazia()) {
            if (atual.getEsq() != null) {
                noBalanceado(atual.getEsq());
            }
            if (verificarBalanceamento(atual) >= -1 && verificarBalanceamento(atual) <= 1) {
                if(atual.getDir() != null){
                    noBalanceado(atual.getDir());
                }
                if(verificarBalanceamento(atual) >= -1 && verificarBalanceamento(atual) <= 1){
                    return true;
                }
            }
            System.out.println(atual.getInfo());
            return false;
        }
        return false;
    }

    private int verificarBalanceamento(No<T> atual){
        return altura(atual.getEsq()) - altura(atual.getDir());
    }

    private No<T> noMaisEsquerda(No<T> atual){
        if(atual.getEsq() == null){
            return atual;
        }
        return noMaisEsquerda(atual.getEsq());
    }

    private No<T> noMaisDireita(No<T> atual){
        if(atual.getDir() == null){
            return atual;
        }
        return noMaisDireita(atual.getDir());
    }

    public No<T> getRaiz() {
        return raiz;
    }

    public void setRaiz(No<T> raiz) {
        this.raiz = raiz;
    }
}
