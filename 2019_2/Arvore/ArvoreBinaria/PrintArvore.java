package Arvore.ArvoreBinaria;

import Arvore.IPrint;

public class PrintArvore<T extends Comparable> implements IPrint<T> {
    @Override
    public void print(T dados) {
        System.out.println(dados);
    }
}
