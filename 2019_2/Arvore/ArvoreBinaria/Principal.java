package Arvore.ArvoreBinaria;

import java.util.Scanner;


public class Principal {
    public static void main(String[] args) {
        Principal p = new Principal();
        //p.menu();
        ArvoreBinaria<Integer> arvore = new ArvoreBinaria<Integer>();
        arvore.inserir(10);
        arvore.inserir(7);
        //arvore.inserir(5);
        //arvore.inserir(3);
        //arvore.inserir(6);
        //arvore.inserir(8);
        //arvore.inserir(13);
        //arvore.inserir(14);
        //arvore.inserir(18);
        //arvore.inserir(20);
        //arvore.inserir(15);
        //arvore.inserir(12);
        //arvore.inserir(11);
        System.out.println("---");
        arvore.imprimirCrescente(new PrintArvore());
        System.out.println("--- APOS REMOVER ---");
        arvore.imprimirCrescente(new PrintArvore());
    }

    public void menu(){
        Scanner input = new Scanner(System.in);
        ArvoreBinaria<Integer> arvore = new ArvoreBinaria<Integer>();

        int opc;
        do{
            System.out.println("");
            System.out.println("===== MENU ARVORE BINARIA =====");
            System.out.println("[1] INSERIR ELEMENTOS");
            System.out.println("[2] REMOVER ELEMENTOS");
            System.out.println("[3] BUSCAR ELEMENTOS");
            System.out.println("[4] VERIFICAR ALTURA");
            System.out.println("[5] VERIFICAR SE ESTÁ CHEIA");
            System.out.println("[9] LISTAR");
            System.out.println("[0] SAIR");
            System.out.print("OPÇÃO: ");
            opc = input.nextInt();

            switch (opc){
                case 1:
                    System.out.println("--- INSERIR ELEMENTOS ---");
                    System.out.print("Informe um numero: ");
                    int num1 = input.nextInt();
                    arvore.inserir(num1);
                    break;
                case 2:
                    System.out.println("--- REMOVER ELEMENTOS ---");
                    break;
                case 3:
                    System.out.println("--- BUSCAR ELEMENTOS ---");
                    arvore.no();
                    break;
                case 4:
                    System.out.println("--- ALTURA DA ARVORE---");
                    System.out.println(arvore.altura());
                    break;
                case 5:
                    System.out.println(arvore.estahCheia());
                    break;
                case 9:
                    int opc2;
                    do{
                        System.out.println("");
                        System.out.println("--- LISTAR ARVORE ---");
                        System.out.println("[1] ORDEM CRESCENTE");
                        System.out.println("[2] ORDEM DECRESCENTE");
                        System.out.print("OPÇÃO: ");
                        opc2 = input.nextInt();

                        switch (opc2){
                            case 1:
                                arvore.imprimirCrescente(new PrintArvore());
                                break;
                            case 2:
                                arvore.imprimirDecrescente(new PrintArvore());
                                break;
                                default:
                                    opc2 = 0;
                                    break;
                        }
                    }while (opc2 != 0);
                    break;
                    default:
                        opc = 0;
                        System.out.println("SAINDO ...");
                        break;
            }
        }while (opc != 0);
    }
}
