package Generics.Fila.Fila;

import Generics.Fila.IFila;

public class Fila<T> implements IFila<T> {
    private T[] dados;
    private int iniFila;
    private int fimFila;

    public Fila(){
        this.iniFila = 0;
        this.fimFila = 0;
    }

    public Fila(int capacidade){
        this.dados = (T[]) new Object[capacidade];
        this.iniFila = 0;
        this.fimFila = 0;
    }

    @Override
    public void queue(T dado) throws Exception {
        aumentaCapacidade();
        if(this.fimFila < this.dados.length){
            this.dados[fimFila] = dado;
            this.fimFila++;
        } else {
            throw new Exception("## ERRO: A FILA ESTA CHEIA ##");
        }
    }

    @Override
    public T deQueue() throws Exception {
        if(!estahVazia()){
            T dadoRemovido = this.dados[this.iniFila];
            this.dados[iniFila] = null;
            this.iniFila++;
            return dadoRemovido;
        } else {
            throw new Exception("## ERRO: A FILA ESTA VAZIA ##");
        }
    }

    @Override
    public boolean estahVazia() throws Exception {
        if(this.fimFila == iniFila){
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int getTamanho() throws Exception {
        return (this.fimFila - this.iniFila);
    }

    private void aumentaCapacidade(){
        int percentual = ((this.dados.length * 80) / 100);
        if(this.fimFila == percentual){
            organizar();
            T[] dadosNovos = (T[])new Object[this.dados.length * 2];
            for (int i = 0; i < this.dados.length; i++) {
                dadosNovos[i] = this.dados[i];
            }
            this.dados = dadosNovos;

        }
    }

    private void organizar(){
        for (int i = 0; i < this.fimFila - 1; i++) {
            if(iniFila < fimFila) {
                this.dados[i] = this.dados[iniFila];
                this.iniFila++;
            }
        }
        this.iniFila = 0;
    }

    public T[] getDados() {
        return dados;
    }

    public void setDados(T[] dados) {
        this.dados = dados;
    }

    public int getIniFila() {
        return iniFila;
    }

    public void setIniFila(int iniFila) {
        this.iniFila = iniFila;
    }

    public int getFimFila() {
        return fimFila;
    }

    public void setFimFila(int fimFila) {
        this.fimFila = fimFila;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < this.dados.length; i++) {
            result.append("[" + this.dados[i] + "]");
        }
        return result.toString();
    }
}
