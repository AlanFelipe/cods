package Generics.Fila.FilaEncadeada;

import Generics.Fila.IFila;
import Generics.Lista.ListaEncadeada.ListaEncadeada;

public class FilaEncadeada<T extends Comparable> extends ListaEncadeada<T> implements IFila<T> {

    @Override
    public void queue(T dado) throws Exception {
        super.incluir(dado);
    }

    @Override
    public T deQueue() throws Exception {
        return super.removeInicio();
    }

    public int tamanho() throws Exception{
        return getTamanho();
    }
}
