package Generics.Fila;

public interface IFila<T> {
    void queue(T dado) throws Exception;
    T deQueue() throws Exception;
    boolean estahVazia() throws Exception;
    int getTamanho() throws Exception;
}
