package Generics.Lista.ListaEncadeada;

import Generics.Lista.ILista;

public class ListaEncadeada<T extends Comparable> implements ILista<T> {
    private No<T> inicio;
    private No<T> fim;
    private int tamanho;
    private boolean inicializar;

    public ListaEncadeada() {
        inicializar();
    }

    @Override
    public void inicializar() {
        this.inicio = null;
        this.fim = null;
        this.tamanho = 0;
        this.inicializar = true;
    }

    @Override
    public void incluir(T elemento) throws Exception {
        No<T> novo = new No<T>(elemento);
        if(this.inicio == null){
            this.inicio = novo;
            this.fim = novo;
        } else {
            this.fim.setProx(novo);
            this.fim = novo;
        }
        this.tamanho++;
    }

    @Override
    public void incluirInicio(T elemento) throws Exception {
        No<T> novo = new No<T>(elemento);
        if(this.inicio == null){
            this.inicio = novo;
            this.fim = novo;
        } else {
            novo.setProx(this.inicio);
            this.inicio = novo;
        }
        this.tamanho++;
    }

    @Override
    public void incluir(T elemento, int posicao) throws Exception {
        No<T> novo = new No<T>(elemento);
        No<T> anterior = null;
        No<T> aux = this.inicio;
        int cont = 0;

        if (posicao == 0) {
            incluirInicio(elemento);
        } else if (posicao == (this.tamanho)) {
            incluir(elemento);
        } else if (posicao > this.tamanho) {
            incluir(elemento);
            throw new Exception("## ERRO: POSIÇÃO INVALIDA, FOI ADCIONADO NO FINAL DA LISTA ##");
        } else {
            while ((aux != null) && (cont != posicao)){
                anterior = aux;
                aux = aux.getProx();
                cont++;
            }
            anterior.setProx(novo);
            novo.setProx(aux);
            this.tamanho++;
        }
    }

    //incluir elementos ordenado
    public void incluirOrdenado(T elemento){
        No<T> novo = new No<T>(elemento);
        No<T> atual = this.inicio;
        No<T> anterior = null;

        if(atual == null){
            this.inicio = novo;
            this.fim = novo;
        } else {
            while (atual != null && elemento.compareTo(atual.getInfo()) > 0){
                anterior = atual;
                atual = atual.getProx();
            }
            novo.setProx(atual);
            if(anterior == null){
                this.inicio = novo;
            } else {
                anterior.setProx(novo);
            }
        }
        this.tamanho++;
    }

    @Override
    public T obterDaPosicao(int posicao) throws Exception {
        if(!estahVazia()){
            if(posicao < this.tamanho){
                No<T> aux = this.inicio;
                for (int i = 0; i < posicao; i++) {
                    aux = aux.getProx();
                }
                return aux.getInfo();
            } else {
                throw new Exception("## ERRO: POSIÇÃO INVALIDA ##");
            }
        } else {
            throw new Exception("## A LISTA ESTÁ VAZIA ##");
        }
    }

    @Override
    public int obter(T item) throws Exception {
        No<T> aux = this.inicio;
        for (int i = 0; i < this.tamanho; i++) {
            if(aux.getInfo().equals(item)){
                return i;
            }
            aux = aux.getProx();
        }
        throw new Exception("## ERRO: NÃO EXISTE ESTE ELEMENTO NA LISTA ##");
    }

    private No<T> obterNo(int posicao) throws Exception {

        int i;
        if (posicao >= 0 || posicao < tamanho) {

            No<T> temporario = this.inicio;

            for (i = 0; i < posicao; i++) {
                temporario = temporario.getProx();
            }

            return temporario;

        } else {
            throw new Exception ("## ERRO: POSIÇÃO INVALIDA ##");
        }
    }

    @Override
    public void remover(int posicao) throws Exception {
        if(!estahVazia()){
            No<T> aux = this.inicio;
            No<T> anterior = null;

            if (posicao == 0) {
                this.removeInicio();
            } else if (posicao == this.tamanho - 1) {
                this.removeFim();
            } else if (posicao >= this.tamanho) {
                throw new Exception("## ERRO: POSIÇÃO INVALIDA ##");
            } else {
                for (int i = 0; i < posicao; i++) {
                    anterior = aux;
                    aux = aux.getProx();
                }
                anterior.setProx(aux.getProx());
                this.tamanho--;
            }
        } else {
            throw new Exception("## A LISTA ESTÁ VAZIA ##");
        }
    }

    protected T removeInicio() throws Exception {
        No<T> aux;
        if (!estahVazia()) {
            aux = this.inicio;
            this.inicio = this.inicio.getProx();
            this.tamanho--;
        } else {
            throw new Exception("## A LISTA ESTÁ VAZIA ##");
        }
        return aux.getInfo();
    }

    protected T removeFim() throws Exception {
        No<T> temp = this.fim;
        if (!estahVazia()) {
            if (this.fim == this.inicio) {
                this.inicio = null;
                this.fim = null;
                this.tamanho--;
            } else {
                No<T> aux, aux2;
                aux = this.inicio;
                aux2 = aux;
                while (aux.getProx() != null) {
                    aux2 = aux;
                    aux = aux.getProx();
                }
                aux2.setProx(null);
                this.fim = aux2;
                this.tamanho--;
            }
        } else {
            throw new Exception("## A LISTA ESTÁ VAZIA ##");
        }
        return temp.getInfo();
    }

    @Override
    public void limpar() throws Exception {
        if(this.tamanho != 0){
            inicializar();
            System.out.println("REMOVENDO TODOS OS DADOS...");
        } else {
            throw new Exception("## A LISTA ESTÁ VAZIA ##");
        }
    }

    @Override
    public int getTamanho() throws Exception {
        return this.tamanho;
    }

    @Override
    public boolean contem(T item) throws Exception {
        No<T> aux = this.inicio;
        for (int i = 0; i < this.tamanho; i++) {
            if(aux.getInfo().equals(item)){
                return true;
            }
            aux = aux.getProx();
        }
        throw new Exception("## ERRO: NÃO CONTÉM ESTE ELEMENTO NA LISTA ##");
    }

    @Override
    public boolean verificarInicializacao() throws Exception {
        if (this.inicializar) {
            return true;
        } else {
            throw new Exception("## ERRO: A LISTA NÃO FOI INICIALIZADA ##");
        }
    }

    @Override
    public boolean estahCheia() throws Exception {
        return false;
    }

    @Override
    public boolean estahVazia() throws Exception {
        if(this.tamanho == 0){
            return true;
        } else {
            return false;
        }
    }

    public No<T> getInicio() {
        return inicio;
    }

    public void setInicio(No<T> inicio) {
        this.inicio = inicio;
    }

    public No<T> getFim() {
        return fim;
    }

    public void setFim(No<T> fim) {
        this.fim = fim;
    }

    public void setTamanho(int tamanho) {
        this.tamanho = tamanho;
    }

    public boolean isInicializar() {
        return inicializar;
    }

    public void setInicializar(boolean inicializar) {
        this.inicializar = inicializar;
    }

    public String toString() {
        if(this.tamanho != 0){
            StringBuilder s = new StringBuilder();
            No<T> aux = this.inicio;

            while (aux != null) {
                s.append("[" + aux.getInfo() + "]");
                aux = aux.getProx();
            }

            return s.toString();
        } else {
            return "## A LISTA ESTÁ VAZIA ##";
        }
    }
}
