package Generics.Lista;

import Generics.Lista.ListaDupEncadeada.ListaDupEncadeada;
import Generics.Lista.ListaDupEncadeada.MenuListaDupEncadeada;
import Generics.Lista.ListaDupEncadeadaCircular.ListaDupEncadeadaCircular;
import Generics.Lista.ListaDupEncadeadaCircular.MenuListaDupEncadeadaCircular;
import Generics.Lista.ListaEncadeada.ListaEncadeada;
import Generics.Lista.ListaEncadeada.MenuListaEncadeada;
import Generics.Lista.ListaEncadeadaCircular.MenuListaEncadeadaCircular;
import Generics.Lista.ListaSequencial.ListaSequencial;
import Generics.Lista.ListaSequencial.MenuListaSequencial;

import java.util.Scanner;

public class Principal {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int opc;

        do{
            System.out.println("[1] LISTA SEQUENCIAL");
            System.out.println("[2] LISTA ENCADEADA");
            System.out.println("[3] LISTA DUPLAMENTE ENCADEADA");
            System.out.println("[4] LISTA ENCADEADA CIRCULAR");
            System.out.println("[5] LISTA DUPLAMENTE ENCADEADA CIRCULAR");
            System.out.println("[0] SAIR");
            System.out.print("OPÇÃO: ");
            opc = input.nextInt();

            switch (opc){
                case 1:
                    //MENU LISTA SEQUENCIAL
                    MenuListaSequencial<ListaSequencial> listaSequencial = new MenuListaSequencial<>();
                    listaSequencial.menu();
                    break;
                case 2:
                    //MENU LISTA ENCADEADA
                    MenuListaEncadeada<ListaEncadeada> listaEncadeada = new MenuListaEncadeada<>();
                    listaEncadeada.menu();
                    break;
                case 3:
                    //MENU LISTA DUPLAMENTE ENCADEADA
                    MenuListaDupEncadeada<ListaDupEncadeada> listaDupEncadeada = new MenuListaDupEncadeada<>();
                    listaDupEncadeada.menu();
                    break;
                case 4:
                    //MENU LISTA ENCADEADA CIRCULAR
                    MenuListaEncadeadaCircular<ListaDupEncadeadaCircular> listaEncadeadaCircular = new MenuListaEncadeadaCircular<>();
                    listaEncadeadaCircular.menu();
                    break;
                case 5:
                    //MENU LISTA DUPLAMENTE ENCADEADA CIRCULAR
                    MenuListaDupEncadeadaCircular<ListaDupEncadeadaCircular> listaDupEncadeadaCircular = new MenuListaDupEncadeadaCircular<>();
                    listaDupEncadeadaCircular.menu();
                    break;
                default:
                    opc = 0;
                    System.out.println("SAINDO...");
                    break;
            }
        }while (opc != 0);
    }
}
