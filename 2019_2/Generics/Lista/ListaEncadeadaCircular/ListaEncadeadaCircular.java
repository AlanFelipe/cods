package Generics.Lista.ListaEncadeadaCircular;

import Generics.Lista.ListaEncadeada.ListaEncadeada;
import Generics.Lista.ListaEncadeada.No;

public class ListaEncadeadaCircular<T extends Comparable> extends ListaEncadeada<T> {
    @Override
    public void incluir(T elemento) throws Exception {
        No<T> novo = new No<>(elemento);
        if (getInicio() == null) {
            novo.setProx(novo);
            setFim(novo);
            setInicio(novo);
        } else {
            novo.setProx(getInicio());
            getFim().setProx(novo);
            setFim(novo);
        }
        setTamanho(getTamanho() + 1);
    }

    @Override
    public void incluirInicio(T elemento) throws Exception {
        if (getInicio() == null) {
            No<T> novo = new No<>(elemento);
            setFim(novo);
            setInicio(novo);
            novo.setProx(novo);
        } else {
            No<T> novo = new No<>(elemento);
            novo.setProx(getInicio());
            getFim().setProx(novo);
            setInicio(novo);
        }
        setTamanho(getTamanho() + 1);
    }

    @Override
    protected T removeInicio() throws Exception {
        No<T> temp = getInicio();
        if (!estahVazia()) {
            No<T> aux, aux2;
            aux = getInicio();
            aux2 = getInicio();
            while(aux.getProx() != getInicio()){
                aux = aux.getProx();
            }
            aux.setProx(aux2.getProx());
            setInicio(getInicio().getProx());
            setTamanho(getTamanho() - 1);
        } else {
            throw new Exception("## A LISTA ESTÁ VAZIA ##");
        }
        return temp.getInfo();
    }

    @Override
    protected T removeFim() throws Exception {
        No<T> temp = getFim();
        if (!estahVazia()) {
            No<T> aux, aux2;
            aux = getInicio();
            aux2 = null;
            while (aux.getProx() != getInicio()) {
                aux2 = aux;
                aux = aux.getProx();
            }
            aux2.setProx(aux.getProx());
            setFim(aux2);
            setTamanho(getTamanho() - 1);
        } else {
            throw new Exception("## A LISTA ESTÁ VAZIA ##");
        }
        return getFim().getInfo();
    }

    @Override
    public String toString(){
        try {
            if(getTamanho() != 0){
                StringBuilder s = new StringBuilder();

                No<T> aux = getInicio();

                if (aux == getInicio()) {
                    s.append("[" + aux.getInfo() + "]" + "\n");
                    aux = aux.getProx();
                }

                while (aux != getInicio()) {
                    s.append("[" + aux.getInfo() + "]" + "\n");
                    aux = aux.getProx();

                }
                return s.toString();
            } else {
                return "## A LISTA ESTÁ VAZIA ##";
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return null;
    }
}
