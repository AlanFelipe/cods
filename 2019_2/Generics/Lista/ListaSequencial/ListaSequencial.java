package Generics.Lista.ListaSequencial;

import Generics.Lista.ILista;

public class ListaSequencial<T> implements ILista {
    T[] lista;
    private int iniLista;
    private int fimLista;
    private boolean inicializar;

    public ListaSequencial(){
    }

    public ListaSequencial(int capacidade) {
        this.lista = (T[])new Object[capacidade];
        inicializar();
    }
    @Override
    public void inicializar() {
        this.iniLista = 0;
        this.fimLista = 0;
        this.inicializar = true;
    }

    @Override
    public void incluir(Comparable elemento) throws Exception {
        verificarInicializacao();
        if (this.fimLista < this.lista.length) {
            this.lista[fimLista] = (T) elemento;
            this.fimLista++;
        } else {
            throw new Exception("## ERRO: A LISTA ESTÁ CHEIA ##");
        }
    }

    @Override
    public void incluirInicio(Comparable elemento) throws Exception {
        verificarInicializacao();
        if(this.fimLista < this.lista.length){
            deslocaDireita(this.iniLista);
            this.lista[iniLista] = (T) elemento;
            this.fimLista++;
        } else {
            throw new Exception("## ERRO: A LISTA ESTÁ CHEIA ##");
        }
    }

    @Override
    public void incluir(Comparable elemento, int posicao) throws Exception {
        verificarInicializacao();
        if(this.fimLista < this.lista.length) {
            if(posicao >= fimLista){
                incluir(elemento);
            } else {
                deslocaDireita(posicao);
                this.lista[posicao] = (T) elemento;
                this.fimLista++;
            }
        } else {
            throw new Exception("## ERRO: A LISTA ESTÁ CHEIA ##");
        }
    }

    public void incluirOrdenado(Comparable elemento) throws Exception {
        verificarInicializacao();
        if (this.fimLista < this.lista.length) {
            int i = fimLista;
            while ((i > 0) && (elemento.compareTo(lista[i - 1]) < 0)){
                lista[i] = lista[i - 1];
                i--;
            }
            this.lista[i] = (T) elemento;
            this.fimLista++;
        }
        else {
            throw new Exception("## ERRO: A LISTA ESTÁ CHEIA ##");
        }
    }

    @Override
    public Comparable obterDaPosicao(int posicao) throws Exception {
        verificarInicializacao();
        if(posicao < this.fimLista){
            return (Comparable) this.lista[posicao];
        } else {
            throw new Exception("## ERRO: NÃO EXISTE DADOS DESSA POSIÇÃO ##");
        }
    }

    @Override
    public int obter(Comparable item) throws Exception {
        verificarInicializacao();
        for (int i = 0; i < this.fimLista; i++) {
            if(this.lista[i].equals(item)){
                return i;
            }
        }
        throw new Exception("## ERRO: NÃO EXISTE ESTE ELEMENTO NA LISTA ##");
    }

    public int buscaBinaria(Comparable elemento) throws Exception{
        verificarInicializacao();
        int ini = this.iniLista;
        int fim = this.fimLista - 1;
        int meio;

        while (ini <= fim){
            meio = (ini + fim) / 2;
            if(this.lista[meio].equals(elemento)){
                return meio;
            }
            if((elemento.compareTo(this.lista[meio]) < 0)){
                fim = meio - 1;
            } else {
                ini = meio + 1;
            }
        }
        throw new Exception("## ERRO: NÃO EXISTE ESTE ELEMENTO NA LISTA ##");
    }

    @Override
    public void remover(int posicao) throws Exception {
        verificarInicializacao();
        if(!estahVazia()){
            if(posicao < this.fimLista){
                deslocarEsquerda(posicao);
                this.fimLista--;
            } else {
                throw new Exception("## ERRO: NÃO EXISTE DADOS DESSA POSIÇÃO ##");
            }
        } else {
            throw new Exception("## A LISTA ESTA VAZIA ##");
        }
    }

    public void removerEmOrdem(Comparable elemento) throws Exception {
        verificarInicializacao();
        if(!estahVazia()){
            int posicao = buscaBinaria(elemento);
            if((Integer) posicao >= this.iniLista ){
                deslocarEsquerda((Integer)posicao);
                this.fimLista--;
            } else {
                throw new Exception("## ERRO: NÃO EXISTE ESTE ELEMENTO NA LISTA ##");
            }
        } else {
            throw new Exception("## A LISTA ESTA VAZIA ##");
        }
    }

    @Override
    public void limpar() throws Exception {
        verificarInicializacao();
        if(!estahVazia()){
            inicializar();
        } else {
            throw new Exception("## A LISTA ESTA VAZIA ##");
        }
    }

    @Override
    public int getTamanho() throws Exception {
        verificarInicializacao();
        if(!estahVazia()){
            return this.fimLista;
        } else {
            throw new Exception("## A LISTA ESTA VAZIA ##");
        }
    }

    @Override
    public boolean contem(Comparable item) throws Exception {
        verificarInicializacao();
        for (int i = 0; i < this.fimLista; i++) {
            if(this.lista[i].equals(item)){
                return true;
            }
        }
        throw new Exception("## NÃO CONTÉM ESTE ELEMENTO NA LISTA ##");
    }

    @Override
    public boolean verificarInicializacao() throws Exception {
        if(this.inicializar){
            return true;
        } else {
            throw new Exception("## ERRO: A LISTA NÃO FOI INICIALIZADA ##");
        }
    }

    @Override
    public boolean estahCheia() throws Exception {
        if(this.fimLista == this.lista.length){
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean estahVazia() throws Exception {
        if(this.fimLista == 0){
            return false;
        } else {
            return false;
        }
    }

    private void deslocaDireita(int pos){
        for (int i = this.fimLista - 1; i >= pos; i--) {
            this.lista[i + 1] = this.lista[i];
        }
    }


    private void deslocarEsquerda(int pos){
        for (int i = pos; i < this.fimLista - 1; i++) {
            this.lista[i] = this.lista[i+1];
        }
    }

    public T[] getLista() {
        return lista;
    }

    public void setLista(T[] lista) {
        this.lista = lista;
    }

    public int getIniLista() {
        return iniLista;
    }

    public void setIniLista(int iniLista) {
        this.iniLista = iniLista;
    }

    public int getFimLista() {
        return fimLista;
    }

    public void setFimLista(int fimLista) {
        this.fimLista = fimLista;
    }

    public boolean isInicializar() {
        return inicializar;
    }

    public void setInicializar(boolean inicializar) {
        this.inicializar = inicializar;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < this.fimLista; i++) {
            result.append("[" + this.lista[i] + "]");
        }
        return result.toString();
    }
}
