package Generics.Pilha.Pilha;

import Generics.Pilha.IPilha;

public class Pilha <T> implements IPilha<T>{
    private T[] dados;
    private int topo;

    public Pilha(){}

    public Pilha(int capacidade){
        this.dados = (T[]) new Object[capacidade];
        this.topo = 0;
    }

    @Override
    public void push(T dado) throws Exception {
        aumentaCapacidade();
        if(this.topo < this.dados.length){
            this.dados[this.topo] = dado;
            this.topo++;
        } else {
            throw new Exception("## ERRO: A PILHA ESTA CHEIA ##");
        }
    }

    @Override
    public T pop() throws Exception {
        if(!estahVazia()){
            T dadoRemovido = this.dados[this.topo-1];
            this.dados[topo-1] = null;
            this.topo--;
            return dadoRemovido;
        } else {
            throw new Exception("## ERRO: A PILHA ESTA VAZIA ##");
        }
    }

    @Override
    public T topo() throws Exception {
        if(!this.estahVazia()){
            return this.dados[this.topo-1];
        } else {
            throw new Exception("## ERRO: A PILHA ESTA VAZIA ##");
        }
    }

    @Override
    public boolean estahVazia() {
        if(this.topo == 0){
            return true;
        } else {
            return false;
        }
    }

    private void aumentaCapacidade(){
        if(this.topo == this.dados.length){
            T[] dadosNovos = (T[])new Object[this.dados.length * 2];
            for (int i = 0; i < this.dados.length; i++) {
                dadosNovos[i] = this.dados[i];
            }
            this.dados = dadosNovos;
        }
    }

    public T[] getDados() {
        return dados;
    }

    public void setDados(T[] dados) {
        this.dados = dados;
    }

    public int getTopo() {
        return topo;
    }

    public void setTopo(int topo) {
        this.topo = topo;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < this.topo; i++) {
            result.append("[" + this.dados[i] + "]");
        }
        return result.toString();
    }
}
