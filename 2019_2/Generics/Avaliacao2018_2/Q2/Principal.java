package Generics.Avaliacao2018_2.Q2;

import Generics.Pilha.Pilha.Pilha;

import java.util.Scanner;

public class Principal<T> {
    final static int TAMANHO = 10;

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        String resp;
        do {
            System.out.println(" ----- VERIFICAR SE É POLIMODRO -----");
            System.out.print("Informe a frase: ");
            String frase = input.nextLine();
            System.out.println("A frase " + frase + " é palindroma? -> " + testaPalimodro(frase));
            System.out.println(" ");
            System.out.print("Deseja verificar uma nova frase? [S/N]: ");
            resp = input.nextLine();
        }while (resp.equalsIgnoreCase("s"));

    }

    public static boolean testaPalimodro(String frase){
        Pilha<Character> pilha = new Pilha<>(TAMANHO);
        for (int i = 0; i < frase.length(); i++) {
            try {
                pilha.push(frase.charAt(i));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        String fraseInversa = "";
        while (!pilha.estahVazia()){
            try {
                fraseInversa += pilha.pop();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if(fraseInversa.equalsIgnoreCase(frase)){
            return true;
        }
        return false;
    }
}
