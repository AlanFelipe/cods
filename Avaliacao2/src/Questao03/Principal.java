package Questao03;

import java.util.Scanner;

public class Principal{
    static Scanner input = new Scanner(System.in);

    public static void main(String[] args) {

        Progama lista = new Progama();
        int opc;

        do{
            System.out.println(" ");
            System.out.println("[1] INSERIR ELEMENTOS");
            System.out.println("[2] ORDENAR ELEMENTOS EM UMA NOVA LISTA");
            System.out.println("[3] LISTAR");
            System.out.println("[0] SAIR");
            System.out.print("OPCAO: ");
            opc = input.nextInt();

            switch (opc){
                case 1:
                    System.out.print("Informe o elemento: ");
                    int elemento = input.nextInt();
                    try {
                        if(!lista.estahCheia()){
                            lista.incluir(elemento);
                        }
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                    break;
                case 2:
                    System.out.print("Informe o elemento da lista: ");
                    int ele = input.nextInt();
                    try {
                        System.out.println("--- NOVA LISTA ORDENADA ---");
                        System.out.println(lista.obterItensMenoresQue(lista, ele));
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                    break;
                case 3:
                    System.out.println("--- LISTA PRINCIPAL ---");
                    System.out.println(lista);
                    break;
                    default:
                        opc = 0;
                        break;
            }
        }while (opc != 0);
    }
}
