package Questao03;

import Listas.ILista;
import Listas.ListaEncadeada.ListaEncadeada;
import Listas.ListaEncadeada.No;

public class Progama extends ListaEncadeada {

    public ILista obterItensMenoresQue(ILista L, int v) throws Exception {
        ListaEncadeada L1 = new ListaEncadeada();
        No aux = ((ListaEncadeada) L).getInicio();
        if(!L.estahVazia()){
            if(L.contem(v)) {
                while (aux != null) {
                    if ((Integer) aux.getInfo() <= v) {
                        L1.incluirOrdenado((Integer) aux.getInfo());
                    }
                    aux = aux.getProx();
                }
            }
        } else {
            throw new Exception("## A LISTA ESTA VAZIA ##");
        }
        return L1;
    }

    @Override
    public boolean estahCheia() throws Exception {
        if(getTamanho() == 100){
            return true;
        } else {
            return false;
        }
    }
}
