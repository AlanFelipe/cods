package Questao02;

import Listas.ILista;
import Listas.ListaDupEncadeada.No;
import Listas.ListaDupEncadeadaCircular.ListaDupEncadeadaCircular;

public class Elementos extends ListaDupEncadeadaCircular {

    public boolean estahOrdenada(ILista lista){
        No aux = ((ListaDupEncadeadaCircular) lista).getInicio();
        boolean ordenado = false;
        while (aux != getFim()){
            if((Integer) aux.getInfo() <= (Integer) aux.getProx().getInfo()){
                ordenado = true;
            } else {
                ordenado = false;
                break;
            }
            aux = aux.getProx();
        }
        return ordenado;
    }
}
