package Questao01;

import Listas.ILista;
import Listas.ListaEncadeada.ListaEncadeada;
import Listas.ListaEncadeada.No;

public class Progama extends ListaEncadeada {

    public void inverterElementos(No lista, No ant){
        if(lista.getProx() != null){
            inverterElementos(lista.getProx(), lista);
        }
        lista.setProx(ant);
    }

    public ILista inverter(ILista lista) throws Exception{
        if(!lista.estahVazia()){
            inverterElementos(((ListaEncadeada) lista).getInicio(), null);
            No aux = ((ListaEncadeada) lista).getInicio();
            ((ListaEncadeada) lista).setInicio(((ListaEncadeada) lista).getFim());
            ((ListaEncadeada) lista).setFim(aux);
            return lista;
        }
        throw new Exception("## A LISTA ESTA VAZIA ##");
    }
}
