import java.util.Arrays;

public class Pilha<G extends Comparable> implements IPilha<G>{

    private Object[] dados;
    private int topo;

    public Pilha(int capacidade){
        this.dados = new Object[capacidade];
        this.topo = 0;
    }


    @Override
    public void empilhar(Object dado) throws Exception {
        if(this.topo < dados.length){
            this.dados[this.topo] = dado;
            this.topo++;
        } else {
            throw new Exception("A pilha está cheia");
        }
    }

    @Override
    public Object desempilhar() throws Exception {
        if(estaVazia()){
            throw new Exception("A pilha está vazia");
        }
        Object dado = this.dados[this.topo - 1];
        topo--;
        return dado;
    }

    @Override
    public boolean estaCheia() {
        return this.topo == this.dados.length && true;
    }

    @Override
    public boolean estaVazia() {
        return this.topo == 0 && true;
    }

    public Object[] getDados() {
        return dados;
    }

    public void setDados(Object[] dados) {
        this.dados = dados;
    }

    public int getTopo() {
        return topo;
    }

    public void setTopo(int topo) {
        this.topo = topo;
    }
}
