
import java.util.Scanner;

public class Principal {
    Scanner input = new Scanner(System.in);

    public static void main(String[] args) throws Exception {
        Principal p = new Principal();
        p.inicializar();
    }

    public void inicializar() throws Exception {
        Scanner input = new Scanner(System.in);
        int opc;

        do {
            System.out.println(" ");
            System.out.println("===== MENU =====");
            System.out.println("[1] Cadastrar Pessoa");
            System.out.println("[2] Remover Pessoa");
            System.out.println("[3] Listar Pessoas");
            System.out.println("[4] Atualizar dados");
            System.out.println("[5] Pesquisar pessoa");
            System.out.println("[0] Sair");
            System.out.print("Opcão: ");
            opc = input.nextInt();

            switch (opc) {
                case 1:
                    cadastrarPessoa();
                    break;
                case 2:
                    System.out.print("Infome a posição: ");
                    int pos = input.nextInt();
                    removerPessoa(pos);
                    break;
                case 3:
                    int opc1;
                    do {
                        System.out.println(" ");
                        System.out.println("[1] Listar todas as pessoas");
                        System.out.println("[2] Listar pessoas por idade");
                        System.out.println("[3] Listar quantidade de ocorrencias");
                        System.out.println("[4] Listar pessoas c/ salario maior que a media");
                        System.out.println("[0] Voltar");
                        System.out.print("Opção: ");
                        opc1 = input.nextInt();

                        switch (opc1) {
                            case 1:
                                listarPessoas();
                                break;
                            case 2:
                                break;
                            case 3:
                                break;
                            case 4:
                                break;
                            default:
                                opc1 = 0;
                                break;
                        }
                    } while (opc1 != 0);
                    break;
                case 4:
                    System.out.print("Informe a posicao: ");
                    int pos1 = input.nextInt();
                    atualizarDados(pos1);
                    break;
                case 5:
                    int opc2;
                    do {
                        System.out.println(" ");
                        System.out.println("[1] Pesquisar por nome");
                        System.out.println("[2] Pesquisar por salario");
                        System.out.println("[0] Voltar");
                        System.out.print("Opção: ");
                        opc2 = input.nextInt();

                        switch (opc2) {
                            case 1:
                                System.out.print("Informe o nome: ");
                                String nome = input.next();
                                break;
                            case 2:
                                System.out.print("Informe o salario: ");
                                float salario = input.nextFloat();
                                break;
                            default:
                                opc2 = 0;
                                break;
                        }
                    } while (opc2 != 0);
                default:
                    opc = 0;
                    System.out.println("Saindo...");
                    break;
            }
        } while (opc != 0);
    }

    Pilha pilha = new Pilha(50);
    Pessoa pessoa;

    public void cadastrarPessoa() throws Exception {
        pessoa = new Pessoa();

        System.out.print("Informe o nome: ");
        pessoa.setNome(input.next());
        System.out.print("Infome a idade: ");
        pessoa.setIdade(input.nextInt());
        System.out.print("Informe o salario: R$");
        pessoa.setSalario(input.nextFloat());

        try {
            pilha.empilhar(pessoa);
        } catch (Exception erro) {
            System.out.println(erro.getMessage());
        }
    }

    public void removerPessoa(int pos) throws Exception {
        listarPessoas();
        System.out.println(" ");
        Pilha pilha2 = new Pilha(pilha.getDados().length);

        try {
            for (int i = pilha.getTopo() - 1; i > pos; i--) {
                pilha2.empilhar(pilha.getDados()[i]);
                pilha.desempilhar();
            }
            System.out.println("Removendo " + pilha.desempilhar());

            for (int i = pos; i < pilha.getDados().length; i++) {
                pilha.empilhar(pilha2.getDados()[pilha2.getTopo() - 1]);
                pilha2.desempilhar();
            }

        } catch (Exception erro) {
            System.out.println(erro.getMessage());
        }
    }

    public void listarPessoas() throws Exception {
        if (pilha.estaVazia()) {
            throw new Exception("A pilha está vazia");
        }
        for (int i = 0; i < pilha.getTopo(); i++) {
            if (pilha.getDados()[i] instanceof Pessoa) {
                System.out.println(pilha.getDados()[i]);
            }
        }
    }

    public void atualizarDados(int pos) throws Exception {
        Pessoa pessoa;

        try {
            for (int i = 0; i < pilha.getTopo(); i++) {
                if (pilha.getDados()[i].equals(pilha.getDados()[pos])) {
                    if (pilha.getDados()[i] instanceof Pessoa) {
                        pessoa = (Pessoa) pilha.getDados()[i];
                        System.out.print("Informe o nome: ");
                        pessoa.setNome(input.next());
                        System.out.print("Informe a idade: ");
                        pessoa.setIdade(input.nextInt());
                        System.out.print("Informe o salario: ");
                        pessoa.setSalario(input.nextFloat());
                    }
                }
            }
        } catch (Exception erro) {
            throw new Exception("Não existe pessoa nessa posição! ");
        }
    }
}