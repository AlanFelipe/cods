public interface IPilha<G> {
    void empilhar(Object dado) throws Exception;
    Object desempilhar() throws Exception;
    boolean estaCheia();
    boolean estaVazia();
}
