public class Pessoa implements Comparable {
    private String nome;
    private int idade;
    private float salario;

    public Pessoa(){
    }

    public Pessoa(String nome, int idade, float salario) {
        this.nome = nome;
        this.idade = idade;
        this.salario = salario;
    }

    @Override
    public int compareTo(Object outra) {
        if(this.idade > ((Pessoa) outra).getIdade()){
            return 1;
        }
        if(this.idade < ((Pessoa) outra).getIdade()){
            return -1;
        }
        return 0;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public float getSalario() {
        return salario;
    }

    public void setSalario(float salario) {
        this.salario = salario;
    }

    @Override
    public String toString() {
        return "Pessoa{" +
                "nome='" + nome + '\'' +
                ", idade=" + idade +
                ", salario=" + salario +
                '}';
    }
}
