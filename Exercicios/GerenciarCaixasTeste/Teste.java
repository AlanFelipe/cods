package Teste;

import java.util.Arrays;
import java.util.Scanner;

public class Teste {
    public static void main(String[] args) {

        int op = -1;
        int cont = 0;

        int idCaixa = 0;

        Fila fila = new Fila(10);
        Pilha pilha = new Pilha();
        pilha.inicializar(5);

        Scanner scanner = new Scanner(System.in);

        while (op != 0) {
            System.out.println("1 - Incluir nova caixa");
            System.out.println("2 - Consumir nova caixa");
            System.out.println("3 - Descartar um lote");
            System.out.println("0 - SAIR");
            op = scanner.nextInt();

            if (op == 1) {
                for(int i = 0; i< 5; i++){
                    Caixa caixa = new Caixa();
                    caixa.setId(idCaixa);
                    idCaixa++;
                    pilha.empilhar(caixa);
                    cont++;
                }
                //Caixa caixa = new Caixa();
                //System.out.println("ID caixa:");
                //Caixa caixa = new Caixa(scanner.nextInt());
                //pilha.empilhar(caixa);
                //cont++;
                System.out.println(Arrays.toString(pilha.getList()));
                if (cont == 5) {
                    fila.Queue(pilha);
                    pilha = new Pilha();
                    pilha.inicializar(5);
                    cont = 0;
                }

            }
            if (op == 2) {
                try {
                    fila.getPilhas()[fila.getInicio()].desempilhar();
                    if (fila.getPilhas()[fila.getInicio()].estaVazia()) {
                        fila.DeQueue();
                    }
                } catch (FilaVaziaException e) {
                    System.out.println(e.getMessage());
                }
            }
            if (op == 3) {
                try{
                    fila.DeQueue();
                }catch (Exception e){
                    e.getMessage();
                }

            }
            System.out.println(Arrays.toString(fila.getPilhas()));
        }
    }
}
