package Teste;

import java.util.Arrays;

public class Fila implements FilaInterface{
    private Pilha[] Pilhas;
    private int inicio = 0;
    private int fim = -1;
    private int max;

    public Fila(int max) {
        this.max = max;
        Pilhas = new Pilha[max];
    }

    public void Queue(Pilha Pilha) {
        if(Size() < max){
            this.fim++;
            Pilhas[this.fim] = Pilha;
        }else{
            throw new FilaCheiaException("Fila cheia!");
        }
    }

    public Pilha DeQueue() {
        if(!empty()){
            Pilha PilhaAux = this.Pilhas[this.inicio];
            for (int i = 1; i <= this.fim; i++){
                this.Pilhas[i-1] = Pilhas[i];
            }
            this.fim--;
            return PilhaAux;
        }else {
            throw new FilaVaziaException("Fila vazia!");
        }

    }

    public boolean empty() {
        if(this.fim < 0){
            return true;
        }else{
            return false;
        }
    }

    public int Size() {
        return fim+1;
    }

    public Pilha[] getPilhas() {
        return Pilhas;
    }

    public void setPilhas(Pilha[] pilhas) {
        Pilhas = pilhas;
    }

    public int getInicio() {
        return inicio;
    }

    public void setInicio(int inicio) {
        this.inicio = inicio;
    }

    @Override
    public String toString() {
        return "Fila{" +
                "Pilhas=" + Arrays.toString(Pilhas) +
                ", inicio=" + inicio +
                ", fim=" +
                '}';
    }
}

class FilaCheiaException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public FilaCheiaException() {
        super();
    }

    public FilaCheiaException(String message) {
        super(message);
    }

}

class FilaVaziaException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public FilaVaziaException() {
        super();
    }

    public FilaVaziaException(String message) {
        super(message);
    }
}
