package GerenciarCaixasGenerico;

import java.util.Arrays;

public class PilhaDeCaixas<G> implements IPilha<G> {
    private G[] caixas;
    private int topo;

    public PilhaDeCaixas(){
        this.caixas = (G[]) new Object[5];
        this.topo = 0;
    }


    public void empilhar(G caixa) throws Exception {
        if(this.topo < this.caixas.length){
            this.caixas[this.topo] = caixa;
            this.topo++;
        } else {
            throw new Exception("## O lote está cheio ##");
        }
    }

    public G desempilhar() throws Exception {
        if(!estaVazia()){
            G Novacaixa = this.caixas[this.topo - 1];
            topo--;
            return Novacaixa;
        } else {
            throw new Exception("## O lote está vazio ##");
        }
    }

    public boolean estaCheia() {
        return this.topo == this.caixas.length && true;
    }

    public boolean estaVazia() {
        return this.topo == 0 && true;
    }

    public G[] getCaixas() {
        return caixas;
    }

    public void setCaixas(G[] caixas) {
        this.caixas = caixas;
    }

    public int getTopo() {
        return topo;
    }

    public void setTopo(int topo) {
        this.topo = topo;
    }


    @Override
    public String toString() {
        return "PilhaDeCaixas [caixas=" + Arrays.toString(caixas) + "]";
    }
}
