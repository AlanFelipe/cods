package Teste;

public class Caixa {
    private int id;

    public Caixa(int id) {
        this.id = id;
    }

    public Caixa(){

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Caixa{" +
                "id=" + id +
                '}';
    }
}
