package Teste;

public interface FilaInterface {
    public void Queue(Pilha pilha);
    public Pilha DeQueue();
    public boolean empty();
    public int Size();
}
