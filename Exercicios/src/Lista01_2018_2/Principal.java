package Lista01_2018_2;

import java.util.Scanner;

public class Principal {
    int opc = 0;

    Scanner input = new Scanner(System.in);
    public static void main(String[] args) throws Exception {
        Principal p = new Principal();
        p.menu();
    }

    public void menu() throws Exception {
        do{
            System.out.println("==== MENU ====");
            System.out.println("[1] Questão 1 - Elementos");
            System.out.println("[2] Questão 2 - Cadastrar Pessoas");
            System.out.println("[3]");
            System.out.println("[4] Questão 4 - Fatorial");
            System.out.println("[5] Questão 5 - Resolver Formulas");
            System.out.println("[6] Questão 6 - Numeros");
            System.out.println("[0] Sair");
            System.out.print("Opção: ");
            opc = input.nextInt();


            switch (opc) {
                case 1: //questão 1
                    Questao1_Elementos elementos = new Questao1_Elementos();
                    System.out.println("--- QUESTÃO 01 ---");
                    for (int i = 0; i < elementos.getElementos().length; i++) {
                        try{
                            System.out.print("Informe o " + (i+1) + "º elemento: ");
                            int ele1 = input.nextInt();
                            elementos.cadastrarElemento(ele1);
                        } catch (Exception exe){
                            System.out.println(exe.getMessage());
                        }
                    }
                    elementos.listarElementos();
                    respostaNova(opc);
                    break;
                case 2: //questao 2
                    System.out.println("----- QUESTÃO 02 -----");
                    int opc1;
                    Questao2_Pessoa pessoa = new Questao2_Pessoa();
                    do{
                        System.out.println("[1] CADASTRAR PESSOA");
                        System.out.println("[2] LISTAR PESSOAS");
                        System.out.println("[3] REMOVER PESSOA");
                        System.out.println("[0] SAIR");
                        System.out.print("OPCÃO: ");
                        opc1 = input.nextInt();

                        switch (opc1){
                            case 1:
                                Questao2_Pessoa outra = new Questao2_Pessoa();
                                System.out.print("Informe o nome: ");
                                outra.setNome(input.next());
                                System.out.print("Informe o cpf: ");
                                outra.setCpf(input.next());
                                if (pessoa.validarCpf(outra.getCpf()) == -1){
                                    pessoa.cadastrarPessoa(outra);
                                } else {
                                    System.out.println("ERRO: CPF JA CADASTRADO");
                                }
                                break;
                            case 2:
                                pessoa.listarPessoas();
                                break;
                            case 3:
                                System.out.println("-- REMOVER PESSOA --");
                                System.out.print("Informe o cpf: ");
                                String cpf = input.next();
                                pessoa.removerPessoa(cpf);
                                break;
                            default:
                                break;
                        }

                    }while(opc1 != 0);
                    respostaNova(opc);
                    break;
                case 3: //questao 3
                    break;
                case 4: //questao 4
                    System.out.println("---- QUESTÃO 4 ----");
                    System.out.print("Informe num numero para calcular: ");
                    int num = input.nextInt();
                    Questao4_Fatorial fat = new Questao4_Fatorial(num);
                    fat.calcularFatorial(num);
                    System.out.println(num + "! = " + fat.calcularFatorial(num) + "\n");
                    System.out.println("Quantidade de vezes para resolver: " + fat.getQuantVezes());
                    respostaNova(opc);
                    break;
                case 5: //questao 5
                    System.out.println("---- QUESTÃO 4 ----");
                    int opc2 = 0;
                    Questao5_Formulas forms = new Questao5_Formulas();
                    do{
                        System.out.println("[1] Formula 1");
                        System.out.println("[2] Formula 2");
                        System.out.println("[3] Formula 3");
                        System.out.println("[0] Voltar");
                        System.out.print("Opção: ");
                        opc2 = input.nextInt();

                        switch (opc2){
                            case 1:
                                System.out.print("Informe o salario: ");
                                forms.setSalario(input.nextInt());
                                System.out.print("Informe a idade: ");
                                forms.setIdade(input.nextInt());
                                System.out.print("Informe a quantidade de filhos: ");
                                forms.setQuantFilhos(input.nextInt());
                                System.out.println("f(n) = salario + idade! / qtdFilhos");
                                System.out.println("f(n) = " + forms.formula1());
                                break;
                            case 2:
                                System.out.print("Informe um numero: ");
                                forms.setNum(input.nextInt());
                                System.out.println("f(x) = 2(x)! / (x-1)!");
                                System.out.println("f(x) = " + forms.formula2());
                                break;
                            case 3:
                                System.out.print("Informe a quantidade de numeros da serie Fibonnaci: ");
                                int num1 = input.nextInt();
                                for (int i = 1; i <= num1; i++) {
                                    System.out.println(forms.fib(i) + " ");
                                }
                                break;
                            default:
                                break;
                        }
                    }while (opc2 != 0);
                    respostaNova(opc);
                case 6:
                    System.out.println("----- QUESTÃO 06 -----");
                    int opc6;
                    Questao6_Numeros fatEle = new Questao6_Numeros();
                    for (int i = 0; i < 10; i++) {
                        System.out.print("Informe o " + (i+1) + "º numero: ");
                        int num4 = input.nextInt();
                        fatEle.cadastrarNumero(num4);
                    }

                    do{
                        System.out.println("[1] ALTERNATIVA A");
                        System.out.println("[2] ALTERNATIVA B");
                        System.out.println("[3] ALTERNATIVA C");
                        System.out.println("[0] SAIR");
                        System.out.print("OPÇÃO: ");
                        opc6 = input.nextInt();

                        switch (opc6){

                            case 1:
                                for (int i = 0; i < 10; i++) {
                                    System.out.println(fatEle.getNum()[i] + "! = " + fatEle.calcularFatorial(fatEle.getNum()[i]));
                                }
                                break;
                            case 2:
                                System.out.println("Media: " + fatEle.calcularMedia());
                                System.out.println("Numero de vezes que o fatorial é menor: " + fatEle.quantFatorial());
                                break;
                            case 3:
                                System.out.println("Resposta: " + fatEle.quantImpar());
                                break;
                            default:
                                break;
                        }
                    }while (opc6 != 0);
                    respostaNova(opc);
                    default:
                        System.out.println(" ");
                        System.out.println("Saindo...");
                        opc = 0;
                        break;
            }
        }while (opc != 0);
    }

    public void respostaNova(int opc){
        System.out.println(" ");
        System.out.println("Deseja responder mais uma questão: [S/N]");
        String resp = input.next();
        if(resp.equalsIgnoreCase("n")){
            System.out.println(" ");
            System.out.println(" Saindo...");
            this.opc = 0;
        } else {

        }
    }
}
