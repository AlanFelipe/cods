package Lista01_2018_2;

public class Questao6_Numeros {
    private int[] num;
    private int quantNum;

    public Questao6_Numeros() {
        this.num = new int[10];
        this.quantNum = 0;
    }

    public Questao6_Numeros(int[] num, int quantNum) {
        this.num = num;
        this.quantNum = quantNum;
    }

    public boolean cadastrarNumero(int n){
        if(quantNum < num.length){
            this.num[quantNum] = n;
            quantNum++;
            return true;
        } else {
            return false;
        }
    }

    public int calcularFatorial(int num){
        Questao4_Fatorial fat = new Questao4_Fatorial();
        return fat.calcularFatorial(num);
    }

    public double calcularMedia(){
        double media = 0;
        double soma = 0;
        for (int i = 0; i < quantNum; i++) {
            soma += getNum()[i];
        }
        media = soma / this.quantNum;
        return media;
    }

    public int quantFatorial(){
        int quant = 0;
        for (int i = 0; i < quantNum; i++) {
            if(calcularFatorial(num[i]) < calcularMedia()){
                quant++;
            }
        }
        return quant;
    }

    public int quantImpar(){
        boolean impar;
        boolean par = false;
        int quant = 0;

        for (int i = 0; i < quantNum; i++) {
            if ((getNum()[i] % 2 != 0) && (calcularFatorial(getNum()[i]) % 2 == 0) && (calcularFatorial(getNum()[i]) > calcularMedia())) {
                quant++;
            }
        }
        return quant;
    }

    public int[] getNum() {
        return num;
    }

    public void setNum(int[] num) {
        this.num = num;
    }

    public int getQuantNum() {
        return quantNum;
    }

    public void setQuantNum(int quantNum) {
        this.quantNum = quantNum;
    }
}
