package Lista01_2018_2;

import java.util.Arrays;

public class Questao1_Elementos {
    private int[] elementos;
    private int quantElementos;

    public Questao1_Elementos() {
        this.elementos = new int[10];
        quantElementos = 0;
    }

    public Questao1_Elementos(int[] elementos, int quantElementos) {
        this.elementos = elementos;
        this.quantElementos = quantElementos;
    }

    public void cadastrarElemento(int ele) throws Exception {
        if(this.quantElementos < this.elementos.length){
            this.elementos[quantElementos] = ele;
            quantElementos++;
        } else {
            throw new Exception("# NÃO É POSSIVEL CADASTRAR ELEMENTO #");
        }
    }

    public void listarElementos(){
        System.out.println("--- ELEMENTOS ---");
        for (int i = 0; i < quantElementos; i++) {
            System.out.print(elementos[i] + " ");
        }
    }

    public int[] getElementos() {
        return elementos;
    }

    public void setElementos(int[] elementos) {
        this.elementos = elementos;
    }

    public int getQuantElementos() {
        return quantElementos;
    }

    public void setQuantElementos(int quantElementos) {
        this.quantElementos = quantElementos;
    }

    @Override
    public String toString() {
        return "Questao1_Elementos{" +
                "elementos=" + Arrays.toString(elementos) +
                '}';
    }
}
