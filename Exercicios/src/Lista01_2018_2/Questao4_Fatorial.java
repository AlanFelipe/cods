package Lista01_2018_2;

public class Questao4_Fatorial {
    private int num;
    private int quantVezes;

    public Questao4_Fatorial() {
    }

    public Questao4_Fatorial(int num) {
        this.num = num;
        this.quantVezes = 0;
    }

    public Questao4_Fatorial(int num, int quantVezes) {
        this.num = num;
        this.quantVezes = quantVezes;
    }

    public int calcularFatorial(int num){
        int fat = 1;
        for (int i = 1; i <= num ; i++) {
            fat *= i;
            quantVezes++;
        }
        return fat;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public int getQuantVezes() {
        return quantVezes;
    }

    public void setQuantVezes(int quantVezes) {
        this.quantVezes = quantVezes;
    }

    @Override
    public String toString() {
        return "Questao4_Fatorial{" +
                "num=" + num +
                ", quantVezes=" + quantVezes +
                '}';
    }
}
