package GerenciarCaixas;

public interface IPilha {
    void empilhar(Caixa caixa) throws Exception;
    Caixa desempilhar() throws Exception;
    boolean estaCheia();
    boolean estaVazia();
}
