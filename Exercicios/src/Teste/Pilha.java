package Teste;

import java.util.Arrays;

public class Pilha implements PilhaInterface{
    private Caixa[] list;
    private int posicao;
    private int pesquisaPosicao = -1;
    private String nome;

    public Pilha() {
    }

    public Pilha(String nome) {
        this.nome = nome;
    }

    public void inicializar(int qtdElementos) {
        try {
            if (list.length > 0) {
                System.out.println("Pilha já iniciada!");
            } else {
                this.list = new Caixa[qtdElementos];
                posicao = 0;
            }
        } catch (Exception e) {
            this.list = new Caixa[qtdElementos];
            posicao = 0;
        }


    }

    public void empilhar(Caixa dado) {
        if (!estaCheia()) {
            this.list[posicao] = dado;
            this.posicao++;
        } else {
            System.out.println("Pilha está cheia!");
        }
    }

    public Caixa desempilhar() {
        Caixa aux;
        if (!estaVazia()) {
            this.posicao--;
            aux = list[posicao];
            return aux;
        } else {
            return null;
        }

    }

    public Boolean estaVazia() {
        if (this.posicao == 0) {
            return true;
        } else {
            return false;
        }

    }

    public Boolean estaCheia() {
        if (posicao < this.list.length) {
            return false;
        } else {
            return true;
        }
    }

    public int pesquisar(Caixa dados) {
        Caixa aux = desempilhar();
        if (aux != dados && !estaVazia()) {
            pesquisar(dados);
        } else if (aux == dados) {
            this.pesquisaPosicao = posicao;
        }
        this.empilhar(aux);
        return pesquisaPosicao;
    }

    public void deletar(Caixa dados) {
        Caixa aux = desempilhar();
        boolean achou = false;
        if (aux != dados && !estaVazia()) {
            deletar(dados);
        } else if (aux == dados) {
            achou = true;
        }
        if(!achou){
            this.empilhar(aux);
        }
    }

    public Caixa[] getList() {
        return list;
    }

    public void setPesquisaPosicao(int pesquisaPosicao) {
        this.pesquisaPosicao = pesquisaPosicao;
    }

    public String getNome() {
        return nome;
    }

    @Override
    public String toString() {
        return "Pilha{" +
                "list=" + Arrays.toString(list) +
                '}';
    }
}
