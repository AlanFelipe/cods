package Teste;

public interface PilhaInterface {
    void inicializar(int qtdElementos);
    void empilhar(Caixa dado);
    Caixa desempilhar();
    Boolean estaVazia();
    Boolean estaCheia();
}
