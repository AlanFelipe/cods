package GerenciarCaixasGenerico;

public interface IFila<G>{
    void enfileirar(G fila) throws Exception;
    G desenfileirar() throws Exception;
    boolean estaCheia();
    boolean estaVazia();
}
