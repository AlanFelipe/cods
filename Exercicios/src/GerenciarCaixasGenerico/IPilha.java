package GerenciarCaixasGenerico;

public interface IPilha<G> {
    void empilhar(G caixa) throws Exception;
    G desempilhar() throws Exception;
    boolean estaCheia();
    boolean estaVazia();
}
