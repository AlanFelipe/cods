package GerenciarCaixasGenerico;

import java.util.Arrays;

public class FilaDePilhas<G> implements IFila<G>{
    public G[] fila;
    public int fimFila;

    public FilaDePilhas() {
        this.fila = (G[]) new Object[10];
        this.fimFila = 0;
    }

    public FilaDePilhas(G[] fila, int fimFila) {
        this.fila = fila;
        this.fimFila = fimFila;
    }

    public void enfileirar(G fila) throws Exception {
        if(this.fimFila < this.fila.length){
            this.fila[this.fimFila] = fila;
            this.fimFila++;
        } else {
            throw new Exception("## O Deposito está cheio ##");
        }
    }

    public G desenfileirar() throws Exception {
        if(estaVazia()){
            throw new Exception("## O deposito está vazio ##");
        } else {
            int inicio = 0;
            G pilhaRemovida = this.fila[inicio];
            for(int i = inicio; i < this.fimFila - 1; i++) {
                this.fila[i] = this.fila[i+1];
            }
            this.fimFila--;
            return pilhaRemovida;
        }
    }

    /*public void aumentaCapacidade(){
        if(this.fimFila == this.fila.length){
            Object[] filaNova = new Object[this.fila.length*2];
            for (int i = 0; i < fila.length; i++) {
                filaNova[i] = this.fila[i];
            }
            this.fila = filaNova;
        }
    }*/

    public G primeiroElemento() throws Exception{
        try {
            return this.fila[0];
        } catch (Exception e){
            throw new Exception("## O deposito está vazio ##");
        }
    }

    public boolean estaCheia() {
        return this.fimFila == this.fila.length && true;
    }

    public boolean estaVazia() {
        return this.fimFila == 0 && true;
    }

    public G[] getFila() {
        return fila;
    }

    public void setFila(G[] fila) {
        this.fila = fila;
    }

    public int getFimFila() {
        return fimFila;
    }

    public void setFimFila(int fimFila) {
        this.fimFila = fimFila;
    }

    @Override
    public String toString() {
        return "FilaPilhas [fila=" + Arrays.toString(fila) + "]";
    }
}
