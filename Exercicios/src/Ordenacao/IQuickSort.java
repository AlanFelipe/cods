package Ordenacao;

public interface IQuickSort {
    void pivoInicioCrescente(int[] vetor, int inicio, int fim);
    void pivoInicioDecrescente(int[] vetor, int inicio, int fim);
    void pivoMeioCrescente(int[] vetor, int inicio, int fim);
    void pivoMeioDecrescente(int[] vetor, int inicio, int fim);
    void pivoFimCrescente(int[] vetor, int inicio, int fim);
    void pivoFimDecrescente(int[] vetor, int inicio, int fim);
}
