package Ordenacao;

public interface IMergeSort {
    void mergeSortCrescente(int[] vetor, int ini, int fim);
    void mergeSortDecrescente(int[] vetor, int ini, int fim);
    void intercalarCrescente(int[] vetor, int ini, int meio, int fim);
    void intercalaeDecrescente(int[] vetor, int ini, int meio, int fim);
}
