package Ordenacao;

import java.util.Arrays;

public class QuickSort implements IQuickSort{

    public void pivoInicioCrescente(int[] vetor, int inicio, int fim) {
        int direta, esquerda, pivo, aux;
        direta = inicio;
        esquerda = fim;
        pivo = vetor[inicio]; //o pivo fica no inicio

        do {
            while (vetor[direta] < pivo) {
                direta++;
            }
            while (vetor[esquerda] > pivo) {
                esquerda--;
            }
            if (direta <= esquerda) {
                aux = vetor[direta];
                vetor[direta] = vetor[esquerda];
                vetor[esquerda] = aux;
                direta++;
                esquerda--;
            }
        } while (direta <= esquerda);

        if (inicio < esquerda) {
            pivoInicioCrescente(vetor, inicio, esquerda);
        }
        if (direta < fim) {
            pivoInicioCrescente(vetor, direta, fim);
        }
    }

    public void pivoInicioDecrescente(int[] vetor, int inicio, int fim) {
        int direta, esquerda, pivo, aux;
        direta = inicio;
        esquerda = fim;
        pivo = vetor[inicio]; // o pivo fica no inicio

        do {
            while (vetor[direta] > pivo) {
                direta++;
            }
            while (vetor[esquerda] < pivo) {
                esquerda--;
            }
            if (direta <= esquerda) {
                aux = vetor[direta];
                vetor[direta] = vetor[esquerda];
                vetor[esquerda] = aux;
                direta++;
                esquerda--;
            }
        } while (direta <= esquerda);

        if (inicio < esquerda) {
            pivoInicioDecrescente(vetor, inicio, esquerda);
        }
        if (direta < fim) {
            pivoInicioDecrescente(vetor, direta, fim);
        }
    }

    public void pivoMeioCrescente(int[] vetor, int inicio, int fim){
        int direta, esquerda, pivo, aux;
        direta = inicio;
        esquerda = fim;
        pivo = vetor[(inicio + fim) / 2]; // o pivo fica no meio

        do {
            while(vetor[direta] < pivo) {
                direta++;
            }
            while(vetor[esquerda] > pivo) {
                esquerda--;
            }
            if(direta <= esquerda) {
                aux = vetor[direta];
                vetor[direta] = vetor[esquerda];
                vetor[esquerda] = aux;
                direta++;
                esquerda--;
            }
        }while(direta <= esquerda);

        if(inicio < esquerda) {
            pivoMeioCrescente(vetor, inicio, esquerda);
        }
        if(direta < fim) {
            pivoMeioCrescente(vetor, direta, fim);
        }
    }

    public void pivoMeioDecrescente(int[] vetor, int inicio, int fim){
        int direta, esquerda, pivo, aux;
        direta = inicio;
        esquerda = fim;
        pivo = vetor[(fim + inicio) / 2]; // o pivo fica no meio

        do {
            while(vetor[direta] > pivo) {
                direta++;
            }
            while(vetor[esquerda] < pivo) {
                esquerda--;
            }
            if(direta <= esquerda) {
                aux = vetor[direta];
                vetor[direta] = vetor[esquerda];
                vetor[esquerda] = aux;
                direta++;
                esquerda--;
            }
        }while(direta <= esquerda);

        if(inicio < esquerda) {
            pivoMeioDecrescente(vetor, inicio, esquerda);
        }
        if(direta < fim) {
            pivoMeioDecrescente(vetor, direta, fim);
        }
    }

    public void pivoFimCrescente(int[] vetor, int inicio, int fim) {
        int direta, esquerda, pivo, aux;
        direta = inicio;
        esquerda = fim;
        pivo = vetor[fim]; // o pivo fica no fim

        do {
            while (vetor[direta] < pivo) {
                direta++;
            }
            while (vetor[esquerda] > pivo) {
                esquerda--;
            }
            if (direta <= esquerda) {
                aux = vetor[direta];
                vetor[direta] = vetor[esquerda];
                vetor[esquerda] = aux;
                direta++;
                esquerda--;
            }
        } while (direta <= esquerda);

        if (inicio < esquerda) {
            pivoFimCrescente(vetor, inicio, esquerda);
        }
        if (direta < fim) {
            pivoFimCrescente(vetor, direta, fim);
        }
    }

    public void pivoFimDecrescente(int[] vetor, int inicio, int fim) {
        int direta, esquerda, pivo, aux;
        direta = inicio;
        esquerda = fim;
        pivo = vetor[fim]; // o pivo fica no fim

        do {
            while (vetor[direta] > pivo) {
                direta++;
            }
            while (vetor[esquerda] < pivo) {
                esquerda--;
            }
            if (direta <= esquerda) {
                aux = vetor[direta];
                vetor[direta] = vetor[esquerda];
                vetor[esquerda] = aux;
                direta++;
                esquerda--;
            }
        } while (direta <= esquerda);

        if (inicio < esquerda) {
            pivoFimDecrescente(vetor, inicio, esquerda);
        }
        if (direta < fim) {
            pivoFimDecrescente(vetor, direta, fim);
        }
    }
}
