package Ordenacao;

public class MergeSort implements IMergeSort{

    public void mergeSortCrescente(int[] vetor, int ini, int fim){

        int meio = (ini + fim) / 2;
        if (ini < fim) {
            mergeSortCrescente(vetor, ini, meio);
            mergeSortCrescente(vetor, meio + 1, fim);
            intercalarCrescente(vetor,ini,meio,fim);
        }
    }

    public  void intercalarCrescente(int[] vetor, int ini, int meio, int fim){
        int[] aux = new int[fim - ini + 1];
        int a = ini;
        int b = meio + 1;
        int h = 0;
        while (a <= meio && b <= fim) {
            if (vetor[a] < vetor[b]) {
                aux[h++] = vetor[a++];
            } else {
                aux[h++] = vetor[b++];
            }
        }
        while (a <= meio) {
            aux[h++] = vetor[a++];
        }
        while (b <= fim) {
            aux[h++] = vetor[b++];
        }
        for (h = 0; h < aux.length; h++) {
            vetor[ini++] = aux[h];
        }
    }

    public void mergeSortDecrescente(int[] vetor, int ini, int fim){

        int meio = (ini + fim) / 2;
        if (ini < fim) {
            mergeSortDecrescente(vetor, ini, meio);
            mergeSortDecrescente(vetor, meio + 1, fim);
            intercalaeDecrescente(vetor,ini,meio,fim);
        }
    }

    public  void intercalaeDecrescente(int[] vetor, int ini, int meio, int fim){
        int[] aux = new int[fim - ini + 1];
        int a = ini;
        int b = meio + 1;
        int h = 0;
        while (a <= meio && b <= fim) {
            if (vetor[a] > vetor[b]) {
                aux[h++] = vetor[a++];
            } else {
                aux[h++] = vetor[b++];
            }
        }
        while (a <= meio) {
            aux[h++] = vetor[a++];
        }
        while (b <= fim) {
            aux[h++] = vetor[b++];
        }
        for (h = 0; h < aux.length; h++) {
            vetor[ini++] = aux[h];
        }
    }
}
