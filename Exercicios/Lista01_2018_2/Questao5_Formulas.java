package Lista01_2018_2;

public class Questao5_Formulas {
    private int salario;
    private int idade;
    private int quantFilhos;
    private int num;

    public Questao5_Formulas() {
    }

    public Questao5_Formulas(int salario, int idade, int quantFilhos, int num) {
        this.salario = salario;
        this.idade = idade;
        this.quantFilhos = quantFilhos;
        this.num = num;
    }

    public int formula1(){
        Questao4_Fatorial fat = new Questao4_Fatorial();

        int result;
        result = (this.salario + fat.calcularFatorial(this.idade)) / this.quantFilhos;
        return result;
    }

    public int formula2(){
        Questao4_Fatorial fat = new Questao4_Fatorial();

        int result;
        result = (2*fat.calcularFatorial(this.num)) / (fat.calcularFatorial((this.num - 1)));
        return result;
    }

    public int fib(int num) {
        if (num == 1) {
            return 0;
        } else {
            if(num == 2){
                return 1;
            } else {
                return fib(num-1) + fib(num-2);
            }
        }
    }

    public int getSalario() {
        return salario;
    }

    public void setSalario(int salario) {
        this.salario = salario;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public int getQuantFilhos() {
        return quantFilhos;
    }

    public void setQuantFilhos(int quantFilhos) {
        this.quantFilhos = quantFilhos;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    @Override
    public String toString() {
        return "Questao5_Formulas{" +
                "salario=" + salario +
                ", idade=" + idade +
                ", quantFilhos=" + quantFilhos +
                ", num=" + num +
                '}';
    }
}
