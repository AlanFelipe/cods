package Lista01_2018_2;

import java.util.Arrays;
import java.util.Calendar;

public class Questao3_GerenciaPessoas {
    private Questao3_GerenciaPessoas[] pessoas;
    private int quantPessoas;
    private int dia,mes,ano;
    private String nome;
    private String cpf;

    public Questao3_GerenciaPessoas() {
        this.pessoas = new Questao3_GerenciaPessoas[10];
        this.quantPessoas = 0;
    }

    public int idade(int ano) throws Exception{
        Calendar hoje = Calendar.getInstance();
        int idade;
        if(ano < 1000 && ano > hoje.get(Calendar.YEAR) ){
            throw new Exception("# ERRO: Ano invalido #");
        } else {
            idade = (hoje.get(Calendar.YEAR) - ano);
        }
        return idade;
    }

    public void cadastrarPessoa(Questao3_GerenciaPessoas p) throws Exception{
        if(this.quantPessoas < this.pessoas.length){
            this.pessoas[quantPessoas] = p;
            quantPessoas++;
        } else {
            throw new Exception("# ERRO: Não é possivel cadastrar #");
        }
    }

    public int validarCpf(String cpf){
        int posicao = -1;
        for (int i = 0; i < quantPessoas; i++) {
            if(cpf.equalsIgnoreCase(pessoas[i].getCpf())){
                posicao = i;
            }
        }
        return posicao;
    }

    public void listarPessoas(){
        System.out.println("---- PESSOAS ----");
        for (int i = 0; i < this.pessoas.length; i++) {
            System.out.println(pessoas[i] + " ");
        }
    }

    public Questao3_GerenciaPessoas[] getPessoas() {
        return pessoas;
    }

    public void setPessoas(Questao3_GerenciaPessoas[] pessoas) {
        this.pessoas = pessoas;
    }

    public int getQuantPessoas() {
        return quantPessoas;
    }

    public void setQuantPessoas(int quantPessoas) {
        this.quantPessoas = quantPessoas;
    }

    public int getDia() {
        return dia;
    }

    public void setDia(int dia) {
        this.dia = dia;
    }

    public int getMes() {
        return mes;
    }

    public void setMes(int mes) {
        this.mes = mes;
    }

    public int getAno() {
        return ano;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    @Override
    public String toString() {
        return "Questao3_GerenciaPessoas{" +
                "pessoas=" + Arrays.toString(pessoas) +
                ", dia=" + dia +
                ", mes=" + mes +
                ", ano=" + ano +
                ", nome='" + nome + '\'' +
                ", cpf='" + cpf + '\'' +
                '}';
    }
}
