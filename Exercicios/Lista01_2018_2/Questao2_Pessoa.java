package Lista01_2018_2;

public class Questao2_Pessoa {
    private String nome;
    private String cpf;
    private Questao2_Pessoa[] pessoa;
    private int quantPessoas;

    public Questao2_Pessoa() {
        this.pessoa = new Questao2_Pessoa[10];
        this.quantPessoas = 0;
    }

    public Questao2_Pessoa(String nome, String cpf, Questao2_Pessoa[] pessoa, int quantPessoas) {
        this.nome = nome;
        this.cpf = cpf;
        this.pessoa = pessoa;
        this.quantPessoas = quantPessoas;
    }

    public void cadastrarPessoa(Questao2_Pessoa p) throws Exception{
        if(this.quantPessoas < this.pessoa.length){
            this.pessoa[quantPessoas] = p;
            quantPessoas++;
        } else {
            throw new Exception("# ERRO: Não é possivel cadastrar #");
        }
    }

    public int validarCpf(String cpf){
        int posicao = -1;
        for (int i = 0; i < quantPessoas; i++) {
            if(cpf.equalsIgnoreCase(pessoa[i].getCpf())){
                posicao = i;
            }
        }
        return posicao;
    }

    public void removerPessoa(String cpf){
        for (int i = 0; i < quantPessoas; i++) {
            if(pessoa[i] != null && pessoa[i].getCpf().equalsIgnoreCase(cpf)) {
                quantPessoas--;
                pessoa[i] = pessoa[quantPessoas];
            }
        }
    }
    public void listarPessoas(){
        System.out.println("--- PESSOAS ---");
        for (int i = 0; i < quantPessoas; i++) {
            System.out.print(pessoa[i] + "\n");
        }
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Questao2_Pessoa[] getPessoa() {
        return pessoa;
    }

    public void setPessoa(Questao2_Pessoa[] pessoa) {
        this.pessoa = pessoa;
    }

    public int getQuantPessoas() {
        return quantPessoas;
    }

    public void setQuantPessoas(int quantPessoas) {
        this.quantPessoas = quantPessoas;
    }

    @Override
    public String toString() {
        return "Questao2_Pessoa{" +
                "nome='" + nome + '\'' +
                ", cpf='" + cpf + '\'' +
                '}';
    }
}
