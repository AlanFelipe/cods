package GerenciarCaixas;

import java.util.Scanner;

public class Principal {
    Scanner input = new Scanner(System.in);

    public static void main(String[] args) throws Exception {
        Scanner input = new Scanner(System.in);
        Principal iniciar = new Principal();
        iniciar.inicializar();
    }

    public void inicializar() throws Exception {
        int opc;

        do {
            System.out.println(" ");
            System.out.println("[1] Inserir uma nova caixa");
            System.out.println("[2] Cosumir uma caixa");
            System.out.println("[3] Descartar Lote");
            //System.out.println("[4] Listar caixas armazenadas");
            System.out.println("[0] Sair");
            System.out.print("Opção: ");
            opc = input.nextInt();

            switch(opc) {

                case 1:
                    inserirCaixa();
                    break;
                case 2:
                    consumirCaixa();
                    break;
                case 3:
                    descartarLote();
                    break;
                /*case 4:
                    listarCaixas();
                    break; */
                default:
                    opc = 0;
                    break;
            }
        }while(opc != 0);
    }


    PilhaDeCaixas pilhaCaixas = new PilhaDeCaixas();
    FilaDePilhas filaPilhas = new FilaDePilhas();
    Caixa caixa;
    int codigoCaixa = 1;

    public void inserirCaixa() throws Exception {

        caixa = new Caixa();

        try {
            if(pilhaCaixas.estaCheia()){
                filaPilhas.enfileirar(pilhaCaixas);
                listarCaixas();
                pilhaCaixas = new PilhaDeCaixas();
                if (filaPilhas.getFimFila() == (filaPilhas.getFila().length * 80 / 100)){
                    System.out.println("## ALERTA: O Deposito está 80% cheio! ##");
                }

            } else {
                caixa.setCodigo(codigoCaixa);
                codigoCaixa++;
                pilhaCaixas.empilhar(caixa);
                listarCaixas();
            }
        } catch (Exception erro) {
            System.out.println(erro.getMessage());
        }
    }

    public void listarCaixas() throws Exception{
        try{
            if(pilhaCaixas.estaVazia() && filaPilhas.estaVazia()){
                throw new Exception("## O lote está vazio ##");
            } else {
                System.out.println("--- CAIXAS ARMARZENADAS ---");
                for(int i = 0; i < pilhaCaixas.getTopo(); i++) {
                    if (pilhaCaixas.getCaixas()[i] instanceof Caixa) {
                        System.out.println(pilhaCaixas.getCaixas()[i]);
                    }
                }

                System.out.println(" ");

                System.out.println("--- DEPOSITO ---");
                for(int i = 0; i < filaPilhas.getFimFila(); i ++) {
                    if (filaPilhas.getFila()[i] instanceof PilhaDeCaixas) {
                        System.out.println(filaPilhas.getFila()[i]);
                    }
                }
            }
        } catch (Exception erro){
            System.out.println(erro.getMessage());
        }
    }

    public void consumirCaixa() throws Exception {
        try {
            if(!pilhaCaixas.estaVazia()){
                System.out.println("--- Consumindo " + pilhaCaixas.desempilhar() + " ---");
                listarCaixas();
            }
            if(pilhaCaixas.estaVazia()){
                if(filaPilhas.primeiroElemento().estaVazia()){
                    filaPilhas.desenfileirar();
                } else {
                    System.out.println("--- Consumindo " + filaPilhas.primeiroElemento().desempilhar() + " ---");
                    listarCaixas();
                }
            }
        } catch (Exception erro) {
            System.out.println(erro.getMessage());
        }
    }

    public void descartarLote() throws Exception{
        try {
            filaPilhas.desenfileirar();
            listarCaixas();
        } catch (Exception erro){
            System.out.println(erro.getMessage());
        }
    }
}
