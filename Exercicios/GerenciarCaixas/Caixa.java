package GerenciarCaixas;

public class Caixa {
    private int codigo;

    public Caixa(){
    }

    public Caixa(int codigo) {
        this.codigo = codigo;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    @Override
    public String toString() {
        return "Caixa [codigo=" + codigo + "]";
    }
}
