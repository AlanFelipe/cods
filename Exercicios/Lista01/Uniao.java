public class Uniao {
    //Atributos
    int[] vetorA;
    int[] vetorB;
    int[] vetorUniao;
    int quantVetorA;
    int quantVetorB;
    int quantVetorUniao;

    //Construtores
    public Uniao(int quantA, int quantB){
        vetorA = new int[quantA];
        vetorB = new int[quantB];
        vetorUniao = new int[vetorA.length + vetorB.length];
    }

    public Uniao(int[] vetorA, int[] vetorB, int[] vetorUniao, int quantVetorA, int quantVetorB, int quantVetorUniao) {
        this.vetorA = vetorA;
        this.vetorB = vetorB;
        this.vetorUniao = vetorUniao;
        this.quantVetorA = quantVetorA;
        this.quantVetorB = quantVetorB;
        this.quantVetorUniao = quantVetorUniao;
    }

    //Metodos get e set

    public int[] getVetorA() {
        return vetorA;
    }

    public void setVetorA(int[] vetorA) {
        this.vetorA = vetorA;
    }

    public int[] getVetorB() {
        return vetorB;
    }

    public void setVetorB(int[] vetorB) {
        this.vetorB = vetorB;
    }

    public int[] getVetorUniao() {
        return vetorUniao;
    }

    public void setVetorUniao(int[] vetorUniao) {
        this.vetorUniao = vetorUniao;
    }

    public int getQuantVetorA() {
        return quantVetorA;
    }

    public void setQuantVetorA(int quantVetorA) {
        this.quantVetorA = quantVetorA;
    }

    public int getQuantVetorB() {
        return quantVetorB;
    }

    public void setQuantVetorB(int quantVetorB) {
        this.quantVetorB = quantVetorB;
    }

    public int getQuantVetorUniao() {
        return quantVetorUniao;
    }

    public void setQuantVetorUniao(int quantVetorUniao) {
        this.quantVetorUniao = quantVetorUniao;
    }

    //Metodos

    public boolean cadastrarVetorA(int num){
        if(quantVetorA < vetorA.length){
            vetorA[quantVetorA] = num;
            quantVetorA++;
            return true;
        } else {
            return false;
        }
    }

    public boolean cadastrarVetorB(int num){
        if(quantVetorB < vetorB.length){
            vetorB[quantVetorB] = num;
            quantVetorB++;
            return true;
        } else {
            return false;
        }
    }

    public void uniaoVetores(){
       quantVetorUniao = vetorA.length;
        for (int i = 0; i < vetorA.length; i++) {
            vetorUniao[i] = vetorA[i];
            vetorUniao[quantVetorUniao] = vetorB[i];
            quantVetorUniao++;
        }
    }

    public void listarVetor(){
        for (int i = 0; i < vetorUniao.length; i++) {
            System.out.print(vetorUniao[i] + " ");
        }
    }
}
