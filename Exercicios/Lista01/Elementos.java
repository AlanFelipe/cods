import java.util.Scanner;

public class Elementos {
    private int[] num;
    private int quantElementos;

    public Elementos() {
        this.num = new int[10];
    }

    public int[] getNum() {
        return num;
    }

    public void setNum(int[] num) {
        this.num = num;
    }

    public int getQuantElementos() {
        return quantElementos;
    }

    public void setQuantElementos(int quantElementos) {
        this.quantElementos = quantElementos;
    }

    public boolean cadastarElemento(int elemento){
        if(quantElementos < num.length){
            this.num[quantElementos] = elemento;
            quantElementos++;
            return true;
        } else {
            return false;
        }
    }

    public void listarElementos(){
        System.out.println("-- ELEMENTOS --");
        for (int i = 0; i < quantElementos; i++){
            System.out.println(num[i] + " ");
        }
    }

    @Override
    public String toString() {
        return "Elementos{" +
                "num=" + this.num +
                '}';
    }
}

