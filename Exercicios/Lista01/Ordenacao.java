import java.util.Arrays;

public class Ordenacao {
    //Atributos
    private int [] elementos;
    private int quantElementos;
    private int menor;

    //Construtores
    public Ordenacao(){
        elementos = new int[10];
    }

    public Ordenacao(int[] elementos, int menor, int quantElementos) {
        this.elementos = elementos;
        this.menor = menor;
        this.quantElementos = quantElementos;
    }

    //Metodos get e set
    public int[] getElementos() {
        return elementos;
    }

    public void setElementos(int[] elementos) {
        this.elementos = elementos;
    }

    public int getMenor() {
        return menor;
    }

    public void setMenor(int menor) {
        this.menor = menor;
    }
    //Metodos
    public boolean cadastrarElementos(int num){
        if(quantElementos < elementos.length){
            elementos[quantElementos] = num;
            quantElementos++;
            return true;
        } else {
            return false;
        }
    }
    public void ordenarSelectionCrescente(){
        int aux;
        for (int i = 0; i < quantElementos - 1; i++) {
            this.menor = i;
            for (int j = i+1 ; j < quantElementos; j++) {
                if(getElementos()[j] < getElementos()[menor]){
                    menor = j;
                }
            }
            aux = getElementos()[menor];
            getElementos()[menor] = getElementos()[i];
            getElementos()[i] = aux;
        }
    }

    public void ordenarSelectionDecrescente(){
        int aux;
        for (int i = 0; i < quantElementos - 1; i++) {
            this.menor = i;
            for (int j = i+1 ; j < quantElementos; j++) {
                if(getElementos()[j] > getElementos()[menor]){
                    menor = j;
                }
            }
            aux = getElementos()[menor];
            getElementos()[menor] = getElementos()[i];
            getElementos()[i] = aux;
        }
    }


    public void ordenarBubbleCrescente(){
        int aux;
        for (int i = 0; i < quantElementos; i++) {
            for (int j = i+1; j < quantElementos; j++) {
                if(elementos[j] < elementos[i]){
                    aux = elementos[i];
                    elementos[i] = elementos[j];
                    elementos[j] = aux;
                }
            }
        }
    }

    public void ordenarBubbleDecrescente(){
        int aux;
        for (int i = 0; i < quantElementos; i++) {
            for (int j = i+1; j < quantElementos; j++) {
                if(elementos[j] > elementos[i]) {
                    aux = elementos[i];
                    elementos[i] = elementos[j];
                    elementos[j] = aux;
                }
            }
        }
    }

    public void ordenarParImpar(){
        int aux;
        for (int i = 0; i < quantElementos - 1; i++) {
            menor = i;
            for (int j = i+1; j < quantElementos; j++) {
                if (elementos[j] % 2 != 0){
                    menor = j;
                    aux = getElementos()[menor];
                    getElementos()[menor] = getElementos()[i];
                    getElementos()[i] = aux;
                } else {
                    aux = getElementos()[menor];
                    getElementos()[menor] = getElementos()[i];
                    getElementos()[i] = aux;
                }
            }
        }
    }

    public void listarElementos(){
        for (int i = 0; i < quantElementos; i++) {
            System.out.print(elementos[i] + " ");
        }
    }

    @Override
    public String toString() {
        return "Ordenacao{" +
                "elementos=" + this.elementos +
                '}';
    }
}
