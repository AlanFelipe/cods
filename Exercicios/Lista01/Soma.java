public class Soma {
    //Atributos
    private int numA;
    private int numB;
    private int soma;

    //Construtores
    public Soma(){

    }

    public Soma(int numA, int numB, int soma) {
        this.numA = numA;
        this.numB = numB;
        this.soma = soma;
    }

    //Metodos get e set

    public int getNumA() {
        return numA;
    }

    public void setNumA(int numA) {
        this.numA = numA;
    }

    public int getNumB() {
        return numB;
    }

    public void setNumB(int numB) {
        this.numB = numB;
    }

    public int getSoma() {
        return soma;
    }

    public void setSoma(int soma) {
        this.soma = soma;
    }

    //Metodos
    public int soma(){
        int mult = getNumA() * getNumB();

        do{
            soma += numA;
        }while(soma != mult);
        return soma;
    }
}
