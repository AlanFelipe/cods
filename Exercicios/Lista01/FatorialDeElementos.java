public class FatorialDeElementos {
    //Atributos
    private int[] elementos;
    private int quantElementos;

    //Construtores
    public FatorialDeElementos(){
        elementos = new int[10];
    }

    //Metodos get e set

    public int[] getElementos() {
        return elementos;
    }

    public void setElementos(int[] elementos) {
        this.elementos = elementos;
    }

    //Metodos
    public boolean cadastrarElemento(int elemento){
        if(quantElementos < elementos.length){
            elementos[quantElementos] = elemento;
            quantElementos++;
            return true;
        } else {
            return false;
        }
    }

    public int calcularFatorial(int num){
        Fatorial fat = new Fatorial();
        return fat.calcularFatorial(num);
    }

    public double calcularMedia(){
        double media = 0;
        double soma = 0;
        for (int i = 0; i < quantElementos; i++) {
            soma += calcularFatorial(getElementos()[i]);
        }
        media = soma/quantElementos;
        return media;
    }

    public int verificarFatorial(){
        int quant = 0;
        for (int i = 0; i < quantElementos; i++) {
            if (calcularFatorial(getElementos()[i]) < calcularMedia()) {
                quant++;
            }
        }
        return quant;
    }

    public int verificarImpar(){
        boolean impar;
        boolean par = false;
        int quant = 0;

        for (int i = 0; i < quantElementos; i++) {
            if ((getElementos()[i] % 2 != 0) && (calcularFatorial(getElementos()[i]) % 2 == 0) && (calcularFatorial(getElementos()[i]) > calcularMedia())) {
                quant++;
            }
        }
        return quant;
    }
}
