public class Fatorial {

    public Fatorial(){

    }

    public int calcularFatorial(int num){
        int fat = 1;
        for (int i = 1; i <= num ; i++) {
            fat *= i;
        }
        return fat;
    }
}
