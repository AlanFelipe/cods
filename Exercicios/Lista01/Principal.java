import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;

import java.util.Scanner;

public class Principal {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int opc;


        do{
            System.out.println("====== RESPONDER QUESTÕES ======");
            System.out.println("--- Informe a questão ---");
            System.out.println("[1] Questão 01 - Elementos");
            System.out.println("[2] Questão 02 - Cadastrar Pessoas");
            System.out.println("[3] ");
            System.out.println("[4] Questão 04 - Fatorial");
            System.out.println("[5] Questão 05 - Formulas");
            System.out.println("[6] Questão 06 - Fatorial de Elementos");
            System.out.println("[7] Questão 07 - Soma");
            System.out.println("[8] Questão 08 - Ordenação");
            System.out.println("[9] Questão 09 - União");
            System.out.println("[0] Sair");
            System.out.print("OPÇÃO: ");
            opc = input.nextInt();

            switch (opc){
                case 1:
                    System.out.println("----- QUESTÃO 01 -----");
                    Elementos elemento = new Elementos();
                    for (int i = 0; i < 10; i++) {
                        System.out.print("Informe o " + (i+1) + "º numero: ");
                        int num = input.nextInt();
                        elemento.cadastarElemento(num);
                    }
                    elemento.listarElementos();
                    System.out.println("Deseja respoder mais uma questão? [S/N]");
                    String resp1 = input.next();
                    if(resp1.equalsIgnoreCase("s")){
                        break;
                    } else {
                        System.out.println("SAINDO...");
                        opc = 0;
                    }
                    break;
                case 2:
                    System.out.println("----- QUESTÃO 02 -----");
                    int opc1;
                    Pessoa pessoa = new Pessoa();
                    do{
                        System.out.println("[1] CADASTRAR PESSOA");
                        System.out.println("[2] LISTAR PESSOAS");
                        System.out.println("[3] REMOVER PESSOA");
                        System.out.println("[0] SAIR");
                        System.out.println("OPCÃO: ");
                        opc1 = input.nextInt();

                        switch (opc1){
                            case 1:
                                Pessoa pessoa1 = new Pessoa();
                                System.out.print("Informe o nome: ");
                                pessoa1.setNome(input.next());
                                System.out.print("Informe o cpf: ");
                                pessoa1.setCpf(input.next());
                                if (pessoa.validarCpf(pessoa1.getCpf()) == -1){
                                    pessoa.cadastrarPessoa(pessoa1);
                                } else {
                                    System.out.println("ERRO: CPF JA CADASTRADO");
                                }
                                break;
                            case 2:
                                pessoa.listarPessoas();
                                break;
                            case 3:
                                System.out.print("Informe o cpf: ");
                                String cpf = input.next();
                                pessoa.removerPessoa(cpf);
                                break;
                                default:
                                    break;
                        }
                    }while(opc1 != 0);
                    System.out.println("Deseja respoder mais uma questão? [S/N]");
                    String resp2 = input.next();
                    if(resp2.equalsIgnoreCase("s")){
                        break;
                    } else {
                        System.out.println("SAINDO...");
                        opc = 0;
                    }
                    break;
                case 3:
                    System.out.println("Deseja respoder mais uma questão? [S/N]");
                    String resp3 = input.next();
                    if(resp3.equalsIgnoreCase("s")){
                        break;
                    } else {
                        System.out.println("SAINDO...");
                        opc = 0;
                    }
                    break;
                case 4:
                    System.out.println("----- QUESTÃO 04 -----");
                    Fatorial fatorial = new Fatorial();
                    System.out.print("Informe um numero para calcular o fatorial: ");
                    int num = input.nextInt();
                    System.out.println(num + "! = " + fatorial.calcularFatorial(num) + "\n");
                    System.out.println("Deseja respoder mais uma questão? [S/N]");
                    String resp4 = input.next();
                    if(resp4.equalsIgnoreCase("s")){
                        break;
                    } else {
                        System.out.println("SAINDO...");
                        opc = 0;
                    }
                    break;
                case 5:
                    System.out.println("----- QUESTÃO 05 -----");
                    int opc5;

                    do{
                        System.out.println("[1] ALTERNATIVA A");
                        System.out.println("[2] ALTERNATIVA B");
                        System.out.println("[3] ALTERNATIVA C");
                        System.out.println("[0] SAIR");
                        System.out.print("OPÇÃO: ");
                        opc5 = input.nextInt();

                        switch (opc5){

                            case 1:
                                Formulas form = new Formulas();
                                System.out.print("Informe o salario atual: ");
                                form.setSalarioAtual(input.nextDouble());
                                System.out.print("Informe a idade: ");
                                form.setIdade(input.nextInt());
                                System.out.print("Informe a quantidade de filhos: ");
                                form.setQtdFilhos(input.nextInt());
                                form.calcularFormulaA();
                                System.out.print("Resultado da Formula: " + form.calcularFormulaA());
                                break;
                            case 2:
                                Formulas form2 = new Formulas();
                                System.out.print("Informe um numero: ");
                                int num2 = input.nextInt();

                                System.out.println("Resultado da Formula: " + form2.calcularFormulaB(num2));
                                break;
                            case 3:
                                Formulas form3 = new Formulas();
                                System.out.print("Informe a quantidade de numeros da serie Fibonnaci: ");
                                int num3 = input.nextInt();
                                for (int i = 1; i <= num3; i++) {
                                    System.out.println(form3.fib(i) + " ");
                                }
                                break;
                                default:
                                    break;
                        }
                    }while (opc5 != 0);

                    System.out.println("Deseja respoder mais uma questão? [S/N]");
                    String resp5 = input.next();
                    if(resp5.equalsIgnoreCase("s")){
                        break;
                    } else {
                        System.out.println("SAINDO...");
                        opc = 0;
                    }
                    break;
                case 6:
                    System.out.println("----- QUESTÃO 06 -----");
                    int opc6;
                    FatorialDeElementos fatEle = new FatorialDeElementos();
                    for (int i = 0; i < 10; i++) {
                        System.out.print("Informe o " + (i+1) + "º numero: ");
                        int num4 = input.nextInt();
                        fatEle.cadastrarElemento(num4);
                    }

                    do{
                        System.out.println("[1] ALTERNATIVA A");
                        System.out.println("[2] ALTERNATIVA B");
                        System.out.println("[3] ALTERNATIVA C");
                        System.out.println("[0] SAIR");
                        System.out.print("OPÇÃO: ");
                        opc6 = input.nextInt();

                        switch (opc6){

                            case 1:
                                for (int i = 0; i < 10; i++) {
                                    System.out.println(fatEle.getElementos()[i] + "! = " + fatEle.calcularFatorial(fatEle.getElementos()[i]));
                                }
                                break;
                            case 2:
                                System.out.println("Media: " + fatEle.calcularMedia());
                                System.out.println("Numero de vezes que o fatorial é menor: " + fatEle.verificarFatorial());
                                break;
                            case 3:
                                    System.out.println("Resposta: " + fatEle.verificarImpar());
                                break;
                            default:
                                break;
                        }
                    }while (opc6 != 0);

                    System.out.println("Deseja respoder mais uma questão? [S/N]");
                    String resp6 = input.next();
                    if(resp6.equalsIgnoreCase("s")){
                        break;
                    } else {
                        System.out.println("SAINDO...");
                        opc = 0;
                    }
                    break;
                case 7:
                    System.out.println("----- QUESTÃO 07 -----");
                    Soma soma = new Soma();
                    System.out.print("Informe o numero para A: ");
                    soma.setNumA(input.nextInt());
                    System.out.print("Informe o numero para B: ");
                    soma.setNumB(input.nextInt());
                    System.out.println("Multiplicação = " + soma.soma());

                    System.out.println("Deseja respoder mais uma questão? [S/N]");
                    String resp7 = input.next();
                    if(resp7.equalsIgnoreCase("s")){
                        break;
                    } else {
                        System.out.println("SAINDO...");
                        opc = 0;
                    }
                    break;
                case 8:
                    System.out.println("----- QUESTÃO 08 -----");
                    Ordenacao ordem = new Ordenacao();
                    for (int i = 0; i < 10; i++) {
                        System.out.print("Informe o " + (i+1) + "º numero: ");
                        int num8 = input.nextInt();
                        ordem.cadastrarElementos(num8);
                    }

                    int opc8;
                    do{
                        System.out.println("");
                        System.out.println("[1] ORDENAÇÃO CRESCENTE");
                        System.out.println("[2] ORDENAÇÃO DECRESENTE");
                        System.out.println("[3] ORDENÇÃO IMPAR/PAR");
                        System.out.println("[0] SAIR");
                        System.out.print("OPÇÃO: ");
                        opc8 = input.nextInt();

                        switch (opc8){
                            case 1:
                                int opc8a;
                                do{
                                    System.out.println("");
                                    System.out.println("[1] METODO SELECTION SORT");
                                    System.out.println("[2] METODO BUBBLE SORT");
                                    System.out.println("[3] METODO MERGESORT");
                                    System.out.println("[4] METODO QUICKSORT");
                                    System.out.println("[0] VOLTAR");
                                    System.out.print("OPÇÃO: ");
                                    opc8a = input.nextInt();

                                    switch (opc8a){
                                        case 1:
                                            ordem.ordenarSelectionCrescente();
                                            ordem.listarElementos();
                                            break;
                                        case 2:
                                            ordem.ordenarBubbleCrescente();
                                            ordem.listarElementos();
                                            break;
                                        case 3:
                                            break;
                                        case 4:
                                            break;
                                            default:
                                                break;
                                    }
                                }while (opc8a != 0);
                                break;
                            case 2:
                                int opc8b;
                                do{
                                    System.out.println("");
                                    System.out.println("[1] METODO SELECTION SORT");
                                    System.out.println("[2] METODO BUBBLE SORT");
                                    System.out.println("[3] METODO MERGESORT");
                                    System.out.println("[4] METODO QUICKSORT");
                                    System.out.println("[0] VOLTAR");
                                    System.out.print("OPÇÃO: ");
                                    opc8b = input.nextInt();

                                    switch (opc8b){
                                        case 1:
                                            ordem.ordenarSelectionDecrescente();
                                            ordem.listarElementos();
                                            break;
                                        case 2:
                                            ordem.ordenarBubbleDecrescente();
                                            ordem.listarElementos();
                                            break;
                                        case 3:
                                            break;
                                        case 4:
                                            break;
                                        default:
                                            break;
                                    }
                                }while (opc8b != 0);
                                break;
                            case 3:
                                ordem.ordenarParImpar();
                                ordem.listarElementos();
                                break;
                                default:
                                    break;
                        }
                    }while (opc8 != 0);

                    System.out.println("Deseja respoder mais uma questão? [S/N]");
                    String resp8 = input.next();
                    if(resp8.equalsIgnoreCase("s")){
                        break;
                    } else {
                        System.out.println("SAINDO...");
                        opc = 0;
                    }
                    break;
                case 9:
                    System.out.println("----- QUESTÃO 08 -----");
                    System.out.print("Informe a quantidade de elementos do vetor A: ");
                    int quantVetA = input.nextInt();
                    System.out.print("Informe a quantidade de elementos do vetor B: ");
                    int quantVetB = input.nextInt();

                    Uniao numeros = new Uniao(quantVetA, quantVetB);
                    System.out.println("--- VETOR A ---");
                    for (int i = 0; i < quantVetA; i++) {
                        System.out.print("Informe o " + (i+1) + "º numero: ");
                        int num9 = input.nextInt();
                        numeros.cadastrarVetorA(num9);
                    }

                    System.out.println("--- VETOR B ---");
                    for (int i = 0; i < quantVetB; i++) {
                        System.out.print("Informe o " + (i+1) + "º numero: ");
                        int num9a = input.nextInt();
                        numeros.cadastrarVetorB(num9a);
                    }

                    int opc9;
                    do {
                        System.out.println("");
                        System.out.println("[1] UNIÃO DOS VETORES");
                        System.out.println("[2] ORDENAÇÃO");
                        System.out.println("[0] SAIR");
                        System.out.print("OPCÃO: ");
                        opc9 = input.nextInt();

                        switch (opc9){
                            case 1:
                                numeros.uniaoVetores();
                                numeros.listarVetor();
                                break;
                            case 2:
                                break;
                                default:
                                    break;
                        }
                    }while (opc9 != 0);

                    System.out.println("Deseja respoder mais uma questão? [S/N]");
                    String resp9 = input.next();
                    if(resp9.equalsIgnoreCase("s")){
                        break;
                    } else {
                        System.out.println("SAINDO...");
                        opc = 0;
                    }
                    break;
                default:
                    opc = 0;
                    System.out.println("SAINDO...");
                    break;
            }
        } while(opc != 0);
    }
}
