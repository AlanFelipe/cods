import java.util.Arrays;

public class Pessoa {
    //Atributos
    private String nome;
    private String cpf;
    private Pessoa [] pessoas;
    private int quantPessoas;

    //Construtores
    public Pessoa(){
        this.pessoas = new Pessoa[10];
    }
    public Pessoa(String nome, String cpf, Pessoa[] pessoas, int quantPessoas) {
        this.nome = nome;
        this.cpf = cpf;
        this.pessoas = pessoas;
        this.quantPessoas = quantPessoas;
    }

    //Metodos Get e Set
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Pessoa[] getPessoas() {
        return pessoas;
    }

    public void setPessoas(Pessoa[] pessoas) {
        this.pessoas = pessoas;
    }

    public int getQuantPessoas() {
        return quantPessoas;
    }

    public void setQuantPessoas(int quantPessoas) {
        this.quantPessoas = quantPessoas;
    }

    //Metodos
    public boolean cadastrarPessoa(Pessoa pessoa){
        if(quantPessoas < pessoas.length){
            pessoas[quantPessoas] = pessoa;
            quantPessoas++;
            return true;
        } else {
            return false;
        }
    }
    
    public int validarCpf(String cpf){
        int posicao = -1;
        for (int i = 0; i < quantPessoas; i++) {
            if(cpf.equalsIgnoreCase(pessoas[i].getCpf())){
                posicao = i;
            }
        }
        return posicao;
    }

    public void removerPessoa(String cpf){
        System.out.println("-- REMOVER PESSOA --");
        for (int i = 0; i < quantPessoas; i++) {
            if(pessoas[i] != null && pessoas[i].getCpf() == cpf){
                quantPessoas--;
                pessoas[i] = pessoas[quantPessoas];
            }
        }
    }

    public void listarPessoas(){
        System.out.println("-- PESSOAS --");
        for (int i = 0; i < quantPessoas; i++) {
            System.out.println(pessoas[i] + " \n");
        }
    }

    @Override
    public String toString() {
        return "Pessoa{" +
                "nome='" + nome + '\'' +
                ", cpf='" + cpf + '\'' +
                '}';
    }
}
