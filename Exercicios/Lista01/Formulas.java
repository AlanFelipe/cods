public class Formulas {
    //Atributos
    private double salarioAtual;
    private int idade;
    private int qtdFilhos;

    //Construtores
    public Formulas(){

    }

    public Formulas(double salarioAtual, int idade, int qtdFilhos) {
        this.salarioAtual = salarioAtual;
        this.idade = idade;
        this.qtdFilhos = qtdFilhos;
    }

    //Metodos get e set

    public double getSalarioAtual() {
        return salarioAtual;
    }

    public void setSalarioAtual(double salarioAtual) {
        this.salarioAtual = salarioAtual;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public int getQtdFilhos() {
        return qtdFilhos;
    }

    public void setQtdFilhos(int qtdFilhos) {
        this.qtdFilhos = qtdFilhos;
    }

    //Metodos
    public double calcularFormulaA(){
        Fatorial fat = new Fatorial();
        return ((this.salarioAtual + (fat.calcularFatorial(this.idade))) / this.qtdFilhos);
    }

    public double calcularFormulaB(int num){
        Fatorial fat = new Fatorial();
        return (2 * (fat.calcularFatorial(num)) / fat.calcularFatorial((num -1)));
    }

    public int fib(int num) {
        if (num == 1) {
            return 0;
        } else {
            if(num == 2){
                return 1;
            } else {
                return fib(num-1) + fib(num-2);
            }
        }
    }
}
