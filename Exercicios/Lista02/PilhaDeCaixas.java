package GerenciarCaixas;

import java.util.Arrays;

public class PilhaDeCaixas implements IPilha{
    private Caixa[] caixas;
    private int topo;

    public PilhaDeCaixas(){
        this.caixas = new Caixa[5];
        this.topo = 0;
    }


    public void empilhar(Caixa caixa) throws Exception {
        if(this.topo < this.caixas.length){
            this.caixas[this.topo] = caixa;
            this.topo++;
        } else {
            throw new Exception("## O lote está cheio ##");
        }
    }

    public Caixa desempilhar() throws Exception {
        if(!estaVazia()){
            Caixa Novacaixa = this.caixas[this.topo - 1];
            topo--;
            return Novacaixa;
        } else {
            throw new Exception("## O lote está vazio ##");
        }
    }

    public boolean estaCheia() {
        return this.topo == this.caixas.length && true;
    }

    public boolean estaVazia() {
        return this.topo == 0 && true;
    }

    public Caixa[] getCaixas() {
        return caixas;
    }

    public void setCaixas(Caixa[] caixas) {
        this.caixas = caixas;
    }

    public int getTopo() {
        return topo;
    }

    public void setTopo(int topo) {
        this.topo = topo;
    }


    @Override
    public String toString() {
        return "PilhaDeCaixas [caixas=" + Arrays.toString(caixas) + "]";
    }
}
