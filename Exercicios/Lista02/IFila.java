package GerenciarCaixas;

public interface IFila {
    void enfileirar(PilhaDeCaixas fila) throws Exception;
    PilhaDeCaixas desenfileirar() throws Exception;
    boolean estaCheia();
    boolean estaVazia();
}
