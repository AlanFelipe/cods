package Lista0102_2018_2;

import Lista01_2018_2.Principal;

import java.io.*;
import java.util.Scanner;

public class Diretorios {
    Scanner input = new Scanner(System.in);

    public static void main(String[] args) {
        Diretorios d = new Diretorios();
        d.cadastrarDiretorios();
        d.listarPorMesAno();
    }

    public void cadastrarDiretorios() {
        String[] lista = new String[10];
        for (int i = 0; i < 3; i++) {
            System.out.print("Informe o nome da pasta: ");
            String data = input.nextLine();
            File diretorios = new File("D:\\Codigos IntelliJ\\Exercicios\\src\\Lista0102_2018_2", data);
            diretorios.mkdir();
            File foto = new File(diretorios, "DSC_" + (i + 1) + ".jpg");
            try {
                foto.createNewFile();
                salvarArquivo(data + " - " + foto.getName() + "\n");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    public void listarPorMesAno() {
        System.out.print("Informe o (dia-mes-ano): ");
        String data = input.nextLine();

        File fotos = new File("D:\\Codigos IntelliJ\\Exercicios\\src\\Lista0102_2018_2", data);
        String[] list = fotos.list();
        for (String arquivos : list) {
            System.out.println(arquivos);
        }
    }

    public void salvarArquivo(String texto) {
        File arquivo = new File("D:\\Codigos IntelliJ\\Exercicios\\src\\Lista0102_2018_2\\Resultado.txt");
        try {
            FileWriter txt = new FileWriter(arquivo,true);
            txt.write(texto);
            txt.flush();
            txt.close();


        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
