public interface IPilha {
    void empilhar(Object dado) throws Exception;
    Object desempilhar() throws Exception;
    boolean estahVazia();
    boolean estahCheia();
}
