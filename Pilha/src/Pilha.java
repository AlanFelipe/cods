import java.util.Arrays;

public class Pilha implements IPilha {
    private Object[] dados;
    private int topo;

    public Pilha(int capacidade){
        this.dados  = new Object[capacidade];
        this.topo = 0;
    }

    public Pilha(Object[] dados, int topo) {
        this.dados = dados;
        this.topo = topo;
    }

    @Override
    public void empilhar(Object dado) throws Exception{
        if(this.topo < dados.length) {
            this.dados[this.topo] = dado;
            this.topo++;
        } else {
            throw new Exception("## A PILHA ESTA CHEIA ##");
        }
    }

    @Override
    public Object desempilhar() throws Exception{
        if(!estahVazia()){
            Object dadoRemovido = this.dados[topo-1];
            topo--;
            return dadoRemovido;
        } else {
            throw new Exception("## A PILHA ESTA VAZIA ##");
        }
    }

    @Override
    public boolean estahVazia() {
        if(this.topo == 0){
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean estahCheia() {
        if(this.topo == this.dados.length){
            return true;
        } else {
            return false;
        }
    }

    /* Verificar Elemento do Topo
        public Object topo() throws Exception {
            if(!this.estahVazia()){
                return this.dados[this.topo-1];
            } else {
                throw new Exception("## A PILHA ESTA VAZIA ##");
            }
        }
    */

    public Object[] getDados() {
        return dados;
    }

    public void setDados(Object[] dados) {
        this.dados = dados;
    }

    public int getTopo() {
        return this.topo;
    }

    public void setTopo(int topo) {
        this.topo = topo;
    }

    @Override
    public String toString() {
        return "Pilha{" +
                "dados=" + Arrays.toString(dados) +
                '}';
    }
}
