package CanalDoCodigo;

import java.util.Arrays;

public class MergeSort {
    public static void mergeSort(int[] vetor, int[] aux, int ini, int fim){
        if(ini < fim){
            int meio = (ini + fim) / 2;
            mergeSort(vetor, aux, ini, meio);
            mergeSort(vetor, aux, meio+1, fim);
            intercalar(vetor, aux, ini, meio, fim);
        }
    }

    public static void intercalar(int[] vetor, int[] aux, int ini, int meio, int fim){
        for (int i = ini; i <= fim; i++)
            aux[i] = vetor[i];

        int j = ini;
        int k = meio + 1;

        for (int l = ini; l <= fim ; l++) {
            if(j > meio) {
                vetor[l] = aux[k++];
            } else if(k > fim){
                vetor[l] = aux[j++];
            } else if(aux[j] < aux[k]){
                vetor[l] = aux[j++];
            } else {
                vetor[l] = aux[k++];
            }
        }

    }

    public static void main(String[] args) {
        int[] vet = {4,6,7,3,5,1,2,8};
        int[] aux = new int[vet.length];

        mergeSort(vet, aux, 0, vet.length-1);

        System.out.println(Arrays.toString(vet));
    }
}
