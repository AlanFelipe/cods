package Questao01;

import java.util.Arrays;
import java.util.Scanner;

public class Principal {
    Scanner input = new Scanner(System.in);

    public static void main(String[] args) throws Exception {
        Principal inicio = new Principal();
        inicio.inicializar();
    }


    public void inicializar() throws Exception {

        int opc;

        do{
            System.out.println(" ");
            System.out.println("[1] Empilhar elementos na pilha A");
            System.out.println("[2] Empilhar elementos na pilha B");
            System.out.println("[3] Desempilhar elemento da pilha A");
            System.out.println("[4] Desempilhar elemento da pilha B");
            System.out.println("[5] Verificar se sao iguais");
            System.out.println("[0] Sair");
            System.out.print("Opcao: ");
            opc = input.nextInt();

            switch (opc){
                case 1:
                    try {
                        inserirPilhaA();
                    } catch (Exception e){
                        System.out.println(e.getMessage());
                    }
                    break;
                case 2:
                    try {
                        inserirPilhaB();
                    } catch (Exception e){
                        System.out.println(e.getMessage());
                    }
                    break;
                case 3:
                    try {
                        System.out.print("--- Desemplinhando elemento " + pilhaA.desempilhar() + " ---");
                    } catch (Exception e){
                        System.out.println(e.getMessage());
                    }
                    break;
                case 4:
                    try {
                        System.out.print("--- Desemplinhando elemento " + pilhaB.desempilhar() + " ---");
                    } catch (Exception e){
                        System.out.println(e.getMessage());
                    }
                    break;
                case 5:
                    System.out.println(Iguais(pilhaA , pilhaB));
                    break;
                default:
                    opc = 0;
                    System.out.println("Saindo...");
                    break;
            }
        }while(opc != 0);

    }

    Pilha pilhaA = new Pilha(10);
    Pilha pilhaB = new Pilha(10);
    Object elementoA = new Object();
    Object elementoB = new Object();

    public void inserirPilhaA() throws Exception {

        try {
            System.out.print("Informe o nome do elemento da pilha A: ");
            elementoA = input.next();
            pilhaA.empilhar(elementoA);
            listarPilhas();
        } catch (Exception e){
            System.out.println(e.getMessage());
        }
    }


    public void inserirPilhaB() throws Exception {

        try {
            System.out.print("Informe o nome do elemento da pilha B: ");
            elementoB = input.next();
            pilhaB.empilhar(elementoB);
            listarPilhas();
        } catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

    public void listarPilhas(){
        System.out.println("--- PILHA A ---");
        System.out.println(pilhaA);
        System.out.println("--- PILHA B ---");
        System.out.println(pilhaB);
    }

    public static boolean Iguais(IPilha pilhaA, IPilha pilhaB) {
        if (pilhaA.getTopo() == pilhaB.getTopo()) {
            return true;
        } else {
            return false;
        }
    }
}
