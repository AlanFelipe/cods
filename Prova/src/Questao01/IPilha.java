package Questao01;

public interface IPilha{
    void empilhar(Object o) throws Exception;
    Object desempilhar() throws Exception;
    boolean estahVazia();
    int getTopo();
}
