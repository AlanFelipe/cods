package Questao03;

public abstract class QuestaoProva {
    public enum Tipo{
        MaiorQZeroPar,
        MaiorQZeroImpar,
        MenorIgualZeroPar,
        MenorIgualZeroImpar
    }

    public abstract void adicionar(int valor);
    public abstract int obterProximo(Tipo tipo);
    public abstract void excluirTodos(Tipo tipo);
    int capacidadeMaxPorTipo = 0;

    public QuestaoProva (int capacidadeMaxPorTipo){
        this.capacidadeMaxPorTipo = capacidadeMaxPorTipo;
    }

}
