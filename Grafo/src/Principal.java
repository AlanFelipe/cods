
public class Principal {
    public static void main(String[] args) {
        Grafo<String> map = new Grafo<String>();

        map.addNo("A");
        map.addNo("B");
        map.addNo("C");
        map.addNo("D");
        map.addNo("E");
        map.addNo("F");
        map.addNo("G");
        map.addNo("H");
        map.addNo("I");
        map.addNo("J");


        //A
        map.addAresta("A", "B", 1, 1, 1);
        map.addAresta("A", "F", 1, 3, 2);
        map.addAresta("A", "H", 1, 2, 1);
        //B
        map.addAresta("B", "A", 1, 1, 1);
        map.addAresta("B", "I", 1, 3, 3);
        map.addAresta("B", "C", 1, 2, 3);
        //C
        map.addAresta("C", "B", 1, 2, 3);
        map.addAresta("C", "E", 2, 2, 2);
        //D
        map.addAresta("D", "C", 1, 1, 1);
        map.addAresta("D", "E", 1, 1, 1);
        //E
        map.addAresta("E", "D", 2, 2, 2);
        map.addAresta("E", "C", 2, 2, 2);
        map.addAresta("E", "I", 1, 1, 2);
        //F
        map.addAresta("F", "G", 2, 2, 1);
        map.addAresta("F", "D", 2, 3, 1);
        map.addAresta("F", "H", 1, 1, 1);
        //G
        map.addAresta("G", "B", 3, 2, 1);
        //H
        map.addAresta("H", "F", 1, 1, 1);
        map.addAresta("H", "J", 2, 1, 1);
        //I
        map.addAresta("I", "B", 1, 3, 3);
        map.addAresta("I", "E", 1, 1, 2);
        //J
        map.addAresta("J", "I", 1, 1, 1);
        map.addAresta("J", "H", 2, 2, 1);

        No<String> origem,destino;
        origem = map.acharNo("A");
        destino = map.acharNo("B");
        map.melhorCaminho(origem,destino);


        System.out.println("\n" + "-- Média Caminho --");
        System.out.println(map.mediaCaminho());
    }
}
