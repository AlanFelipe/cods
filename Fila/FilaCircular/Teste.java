package FilaCircular;

public class Teste {
    public static void main(String[] args) {

        CircularQueue circularQueue = new CircularQueue(8);
        circularQueue.enqueue(15);
        circularQueue.enqueue(16);
        circularQueue.enqueue(17);
        circularQueue.enqueue(18);
        circularQueue.enqueue(19);
        circularQueue.enqueue(20);
        circularQueue.enqueue(21);
        circularQueue.enqueue(22);

        System.out.println("Fila cheia" + circularQueue);

        System.out.print("Elemento removido da fila "+circularQueue.dequeue());
        //inserindo outro para testar
        circularQueue.enqueue(23);
        System.out.println("Nova fila");
        System.out.println(circularQueue);
        //Removendo da fila
        System.out.println(circularQueue.dequeue() + " ");
        System.out.println(circularQueue.dequeue() + " ");
        System.out.println(circularQueue.dequeue() + " ");
        try{
            circularQueue.enqueue(24);
            circularQueue.enqueue(25);
            circularQueue.enqueue(26);
            circularQueue.enqueue(27);
        }catch (QueueFullException e){
            System.out.println(e.getMessage());
        }

        System.out.println(circularQueue);

    }
}
