package FilaCircular;

import java.util.Arrays;

public class CircularQueue {
    private int currentSize; //Quantidade de elemento
    private long[] circularQueueElements;
    private int maxSize; //Tamanho da Fila

    private int rear;//Final da fila
    private int front; //Frente da Fula

    public CircularQueue(int maxSize) {
        this.maxSize = maxSize;
        circularQueueElements = new long[this.maxSize];
        currentSize = 0;
        front = 0;
        rear = 0;
    }

    /**
     * Enfileirando elementos.
     */
    public void enqueue (long value) throws QueueFullException {
        if (isFull()) {
            throw new QueueFullException("Fila está cheia, o elemento não pode ser adicionado");
        }
        else {
            circularQueueElements[this.rear] = value;
            rear = (rear + 1) % circularQueueElements.length;
            currentSize++;
        }
    }

    /**
     * Desenfileirando Elementos.
     */
    public long dequeue() throws QueueEmptyException {
        long deQueuedElement;
        if (isEmpty()) {
            throw new QueueEmptyException("Fila está vazia, não possui elemento para retirar");
        }
        else {
            deQueuedElement = circularQueueElements[this.front];
            this.front = (this.front + 1) % circularQueueElements.length;
            currentSize--;
        }
        return deQueuedElement;
    }

    /**
     * Está cheia = true.
     */
    public boolean isFull() {
        return (currentSize == circularQueueElements.length);
    }

    /**
     * Está vazia = true.
     */
    public boolean isEmpty() {
        return (currentSize == 0);
    }

    @Override
    public String toString() {
        return "CircularQueue [" + Arrays.toString(this.circularQueueElements) + "]";
    }
}

class QueueFullException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public QueueFullException() {
        super();
    }

    public QueueFullException(String message) {
        super(message);
    }

}

class QueueEmptyException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public QueueEmptyException() {
        super();
    }

    public QueueEmptyException(String message) {
        super(message);
    }

}