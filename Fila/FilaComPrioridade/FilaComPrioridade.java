package FilaComPrioridade;

import Fila.Fila;

public class FilaComPrioridade extends Fila{

    public FilaComPrioridade(int capacidade) {
        super(capacidade);
    }

    public FilaComPrioridade(Object[] dados, int fimFila) {
        super(dados, fimFila);
    }

    @Override
    public void enfileirar(Object dado) throws Exception {
        Comparable chave = (Comparable)dado;

        int i;
        for (i = 0; i < this.getFimFila(); i++) {
            if(chave.compareTo(this.getDados()[i]) < 0){
                break;
            }
        }
        adiciona(i, chave);
    }

    public boolean adiciona(int posicao, Object dado) throws Exception{
        if(!(posicao >= 0 && posicao < getFimFila())){ //quando a posição não poder ser acessada
            throw new Exception("Posição Invalida"); //lanca o erro
        }

        //mover todos elementos
        for (int i = getFimFila()-1; i >= posicao; i--) {
            getDados()[i+1] = getDados();
        }
        getDados()[posicao] = dado;
        getDados();

        return false;
    }
}
