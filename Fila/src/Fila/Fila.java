package Fila;

import java.util.Arrays;

public class Fila implements IFila {
    private Object[] dados;
    private int fimFila;

    public Fila(int capacidade){
        this.dados = new Object[capacidade];
        this.fimFila = 0;
    }

    public Fila(Object[] dados, int fimFila) {
        this.dados = dados;
        this.fimFila = fimFila;
    }

    @Override
    public void enfileirar(Object dado) throws Exception {
        if(this.fimFila < this.dados.length){
            this.dados[fimFila] = dado;
            fimFila++;
        } else {
            throw new Exception("## A FILA ESTA CHEIA ##");
        }
    }

    @Override
    public Object desenfileirar() throws Exception {
        if(!estahVazia()){
            int inicioFila = 0;
            Object dadoRemovido = this.dados[inicioFila];
            for (int i = 0; i < this.fimFila - 1; i++) {
                this.dados[i] = this.dados[i+1];
            }
            this.fimFila--;
            return dadoRemovido;
        } else {
            throw new Exception("## A FILA ESTA VAZIA ##");
        }
    }

    @Override
    public boolean estahVazia() {
        if(this.fimFila == 0){
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean estahCheia() {
        if(this.fimFila == this.dados.length){
            return true;
        } else {
            return false;
        }
    }

    /* Verificar o primeiro da fila
        public Object primeiroDado() throws Exception{
            try {
                return this.dados[0];
            } catch (Exception e){
                throw new Exception("## A FILA ESTA VAZIA ##");
            }
        }
    */

    public Object[] getDados() {
        return dados;
    }

    public void setDados(Object[] dados) {
        this.dados = dados;
    }

    public int getFimFila() {
        return fimFila;
    }

    public void setFimFila(int fimFila) {
        this.fimFila = fimFila;
    }

    @Override
    public String toString() {
        return "Fila.Fila{" +
                "dados=" + Arrays.toString(dados) +
                '}';
    }
}
