package FilaComPrioridade;

public class Teste {
    public static void main(String[] args) throws Exception {

        FilaComPrioridade fila = new FilaComPrioridade(10);

        fila.enfileirar(new Paciente("A", 2));
        fila.enfileirar(new Paciente("B", 1));
        fila.enfileirar(new Paciente("C", 3));

        System.out.println(fila);
    }
}
