package FilaComPrioridade;

public class Paciente implements Comparable{
    private String nome;
    private int prioridade;

    public Paciente(){

    }
    public Paciente(String nome, int prioridade) {
        this.nome = nome;
        this.prioridade = prioridade;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getPrioridade() {
        return prioridade;
    }

    public void setPrioridade(int prioridade) {
        this.prioridade = prioridade;
    }

    @Override
    public int compareTo(Object o) {
        if(this.prioridade > ((Paciente)o).getPrioridade()){
            return 1;
        } else if(this.prioridade < ((Paciente)o).getPrioridade()){
            return -1;
        } else {
            return 0;
        }
    }

    @Override
    public String toString() {
        return "Paciente{" +
                "nome='" + nome + '\'' +
                ", prioridade=" + prioridade +
                '}';
    }
}
