package Fila;

public interface IFila {
    void enfileirar(Object dado) throws Exception;
    Object desenfileirar() throws Exception;
    boolean estahVazia();
    boolean estahCheia();
}
