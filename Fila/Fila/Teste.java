package Fila;

import java.util.Scanner;

public class Teste {
    public static void main(String[] args) throws Exception {
        Scanner input = new Scanner(System.in);

        Fila fila = new Fila(10);
        int opc;

        do{
            System.out.println(" ");
            System.out.println("[1] Enfileirsar elementos");
            System.out.println("[2] Desenfileirar elementos");
            System.out.println("[3] Verificar se ta cheia");
            System.out.println("[4] Verificar se ta vazia");
            System.out.println("[5] Listar");
            System.out.println("[0] Sair");
            System.out.print("Opcao: ");
            opc = input.nextInt();

            switch (opc){
                case 1:
                    try {
                        System.out.print("Informe o elemento: ");
                        int elemento = input.nextInt();
                        fila.enfileirar(elemento);
                    } catch (Exception e){
                        System.out.println(e.getMessage());
                    }
                    break;
                case 2:
                    try {
                        System.out.println("Removendo elemento " + fila.desenfileirar());
                    } catch  (Exception e){
                        System.out.println(e.getMessage());
                    }
                    break;
                case 3:
                    System.out.println(fila.estahCheia());
                    break;
                case 4:
                    System.out.println(fila.estahVazia());
                    break;
                case 5:
                    for (int i = 0; i < fila.getFimFila(); i++) {
                        System.out.println(fila.getDados()[i] + " ");
                    }
                    break;
                default:
                    opc = 0;
                    System.out.println("Saindo...");
                    break;
            }
        }while(opc != 0);
    }
}
