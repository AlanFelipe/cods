package Arvore.ArvoreAVL;

import Arvore.ArvoreBinaria.ArvoreBinaria;
import Arvore.No;

public class ArvoreAVL<T extends Comparable> extends ArvoreBinaria<T> {

    @Override
    protected void inserir(No<T> noAtual, T dado) throws Exception {
        //veririfa se a raiz esta vazia
        //se a raiz estiver vazia, instacia o primeiro dado na raiz
        if(estahVazia()){
            setRaiz(new No<T>(dado));
            System.out.println(dado + " inserido na raiz");
        } else {
            //se a raiz não estiver vazia, é criado um novoNo
            //se o valor do dado for MENOR que o valor do noAtual verifica se o lado ESQUERDO do noAtual esta vazio
            //se o lado ESQUERDO do noAtual estiver vazio é estanciado o novoNo a ESQUERDA do noAtual
            //se o lado ESQUERDO não estiver vazio é chamado o metodo de inserir com o valor do novoNo como noAtual
            No<T> novoNo = new No<T>(dado);
            if(dado.compareTo(noAtual.getInfo()) < 0){
                if(noAtual.getEsq() == null){
                    noAtual.setEsq(novoNo);
                    novoNo.setPai(noAtual);
                    //balancear a arvore
                    balancear(novoNo);
                    System.out.println(dado + " foi inserido a esquerda de " + noAtual.getInfo());
                } else {
                    inserir(noAtual.getEsq(), dado);
                }
                //se o valor do dado for MAIOR que o valor do noAtual verifica se o lado DIREITO do noAtual esta vazio
                // se o lado DIREITO do noAtual estiver vazio é estanciado o novoNo a DIREITA do noAtual
                // se o lado DIREITO não estiver vazio é chamado o metodo de inserir com o valor do novoNo como noAtual
            } else if(dado.compareTo(noAtual.getInfo()) > 0){
                if(noAtual.getDir() == null){
                    noAtual.setDir(novoNo);
                    novoNo.setPai(noAtual);
                    //balancear a arvore
                    balancear(novoNo);
                    System.out.println(dado + " foi inserido a direita de " + noAtual.getInfo());
                } else {
                    inserir(noAtual.getDir(), dado);
                }
            }
        }
    }

    @Override
    public void remover(T dado) throws Exception {
        if(!estahVazia()){
            if(dado.compareTo(getRaiz().getInfo()) == 0 &&
                    getRaiz().getEsq()  == null &&
                    getRaiz().getDir()  == null){
                setRaiz(null);
            } else {
                remover(getRaiz(), dado);
                balancear(getRaiz());
            }
        } else {
            throw new Exception("A arvore está vazia");
        }
    }

    private void balancear(No<T> atual){

        int balanceamento = verificarBalanceamento(atual);

        if (balanceamento == -2) {

            if (altura(atual.getEsq().getEsq()) >= altura(atual.getEsq().getDir())) {
                atual = rotacionarDireita(atual);

            } else {
                atual = rotacaoDuplaDireita(atual);
            }

        } else if (balanceamento == 2) {

            if (altura(atual.getDir().getDir()) >= altura(atual.getDir().getEsq())) {
                atual = rotacionarEsquerda(atual);

            } else {
                atual = rotacaoDuplaEsquerda(atual);
            }
        }

        if (atual.getPai() != null) {
            balancear(atual.getPai());
        } else {
            setRaiz(atual);
        }
    }

    private No<T> rotacionarEsquerda(No<T> inicial){
        No<T> direita = inicial.getDir();
        direita.setPai(inicial.getPai());

        inicial.setDir(direita.getEsq());

        if (inicial.getDir() != null) {
            inicial.getDir().setPai(inicial);
        }

        direita.setEsq(inicial);
        inicial.setPai(direita);

        if (direita.getPai() != null) {

            if (direita.getPai().getDir() == inicial) {
                direita.getPai().setDir(direita);

            } else if (direita.getPai().getEsq() == inicial) {
                direita.getPai().setEsq(direita);
            }
        }
        return direita;
    }

    private No<T> rotacionarDireita(No<T> inicial){
        No<T> esquerda = inicial.getEsq();
        esquerda.setPai(inicial.getPai());

        inicial.setEsq(esquerda.getDir());

        if (inicial.getEsq() != null) {
            inicial.getEsq().setPai(inicial);
        }

        esquerda.setDir(inicial);
        inicial.setPai(esquerda);

        if (esquerda.getPai() != null) {

            if (esquerda.getPai().getDir() == inicial) {
                esquerda.getPai().setDir(esquerda);

            } else if (esquerda.getPai().getEsq() == inicial) {
                esquerda.getPai().setEsq(esquerda);
            }
        }
        return esquerda;
    }

    public No<T> rotacaoDuplaDireita(No<T> inicial){
        inicial.setEsq(rotacionarEsquerda(inicial.getEsq()));
        return rotacionarDireita(inicial);
    }

    public No<T> rotacaoDuplaEsquerda(No<T> inicial){
        inicial.setDir(rotacionarDireita(inicial.getDir()));
        return rotacionarEsquerda(inicial);
    }

    private int verificarBalanceamento(No<T> atual){
        return altura(atual.getDir()) - altura(atual.getEsq());
    }

    @Override
    public String toString() {
        return "ARVORE AVL";
    }
}

