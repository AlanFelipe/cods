package Arvore;

public class No<T extends Comparable> {
    private T info;
    private No<T> dir;
    private No<T> esq;
    private No<T> pai;

    public No() {
    }

    public No(T info){
        this.info = info;
        this.dir = null;
        this.esq = null;
        this.pai = null;
    }

    public T getInfo() {
        return info;
    }

    public void setInfo(T info) {
        this.info = info;
    }

    public No<T> getDir() {
        return dir;
    }

    public void setDir(No<T> dir) {
        this.dir = dir;
    }

    public No<T> getEsq() {
        return esq;
    }

    public void setEsq(No<T> esq) {
        this.esq = esq;
    }

    public No<T> getPai() {
        return pai;
    }

    public void setPai(No<T> pai) {
        this.pai = pai;
    }
}
