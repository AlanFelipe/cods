package Arvore;

import Arvore.ArvoreAVL.ArvoreAVL;
import Arvore.ArvoreBinaria.ArvoreBinaria;

import java.util.Scanner;


public class Principal {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        Principal p = new Principal();
        ArvoreBinaria<Float> arvoreBinaria = new ArvoreBinaria<Float>();
        ArvoreAVL<Float> arvoreAVL = new ArvoreAVL<Float>();

        int opc;
        System.out.println("[1] Arvore Binaria");
        System.out.println("[2] Arvore AVL");
        System.out.print("Opção: ");
        opc = input.nextInt();

        switch (opc){
            case 1:
                p.menu(arvoreBinaria);
                break;
            case 2:
                p.menu(arvoreAVL);
                break;
        }
    }

    public void menu(ArvoreBinaria<Float> arvore){
        Scanner input = new Scanner(System.in);

        int opc;
        do{
            System.out.println(" ");
            System.out.println("=========== MENU " + arvore.toString() + " ============");
            System.out.println("[1] INSERIR ELEMENTOS");
            System.out.println("[2] REMOVER ELEMENTOS");
            System.out.println("[3] BUSCAR ELEMENTOS");
            System.out.println("[4] VERIFICAR ALTURA");
            System.out.println("[5] VERIFICAR SE ESTÁ CHEIA");
            System.out.println("[9] LISTAR");
            System.out.println("[0] SAIR");
            System.out.print("OPÇÃO: ");
            opc = input.nextInt();

            switch (opc){
                case 1:
                    System.out.println("--- INSERIR ELEMENTOS ---");
                    System.out.print("Informe um numero: ");
                    float num1 = input.nextFloat();
                    try {
                        arvore.inserir(num1);
                    } catch (Exception e){
                        System.out.println(e.getMessage());
                    }
                    break;
                case 2:
                    System.out.println("--- REMOVER ELEMENTOS ---");
                    System.out.print("Informe um numero: ");
                    float num2 = input.nextFloat();
                    try{
                        arvore.remover(num2);
                        System.out.println("Elemento removido com sucesso..");
                    } catch (Exception e){
                        System.out.println(e.getMessage());
                    }
                    break;
                case 3:
                    System.out.println("--- BUSCAR ELEMENTOS ---");
                    System.out.print("Informe um numero: ");
                    float num3 = input.nextFloat();
                    try{
                        if (arvore.buscar(num3) != null){
                            System.out.println("Elemento encontrado: " + arvore.buscar(num3));
                        } else {
                            throw new Exception("Elemento não encontrado");
                        }
                    } catch (Exception e){
                        System.out.println(e.getMessage());
                    }
                    break;
                case 4:
                    System.out.println("--- ALTURA DA ARVORE---");
                    System.out.println("Altura: " + arvore.altura());
                    break;
                case 5:
                    try {
                        System.out.println("Esta cheia? " + arvore.estahCheia());
                    } catch (Exception e){
                        System.out.println(e.getMessage());
                    }
                    break;
                case 9:
                    System.out.println("--- LISTAR ARVORE ---");
                    try {
                        if(!arvore.estahVazia()){
                            System.out.println(" -- Crescente -- " );
                            arvore.imprimirCrescente(new PrintArvore());
                            System.out.println("\n" + "-- Decrescente --");
                            arvore.imprimirDecrescente(new PrintArvore());
                            System.out.println("\n" + " -- Pré Ordem -- " );
                            arvore.imprimirPreOrdem(new PrintArvore());
                            System.out.println("\n" + "-- Pós Ordem --");
                            arvore.imprimirPosOrdem(new PrintArvore());

                        } else {
                            throw new Exception("A arvore está vazia");
                        }
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                    break;
                    default:
                        opc = 0;
                        System.out.println("SAINDO ...");
                        break;
            }
        }while (opc != 0);
    }
}
