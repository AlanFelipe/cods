package Arvore;

public interface IPrint<T extends Comparable> {
    void print(T dados);
}
