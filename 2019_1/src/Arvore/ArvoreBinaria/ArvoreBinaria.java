package Arvore.ArvoreBinaria;

import Arvore.IPrint;
import Arvore.No;

public class ArvoreBinaria<T extends Comparable> {

    //raiz da arvore
    private No<T> raiz;

    //construtor
    public ArvoreBinaria() {
        this.raiz = null;

    }
    //====================================================================== INSERIR ELEMENTOS ===============================================================================
    public void inserir(T dado) throws Exception{
        //chama o metodo de inserir passando a raiz como noAtual
        inserir(raiz,dado);
    }

    protected void inserir(No<T> noAtual, T dado) throws Exception{
        //verifica se a raiz esta vazia
        //se a raiz estiver vazia, instacia o primeiro dado na raiz
        if(buscar(dado) != dado){
            if(estahVazia()){
                this.raiz = new No<T>(dado);
                System.out.println(dado + " inserido na raiz");
            } else {
                //se a raiz não estiver vazia, é criado um novoNo
                //se o valor do dado for MENOR que o valor do noAtual verifica se o lado ESQUERDO do noAtual esta vazio
                //se o lado ESQUERDO do noAtual estiver vazio é estanciado o novoNo a ESQUERDA do noAtual
                //se o lado ESQUERDO não estiver vazio é chamado o metodo de inserir com o valor do novoNo como noAtual
                No<T> novoNo = new No<T>(dado);
                if(dado.compareTo(noAtual.getInfo()) < 0){
                    if(noAtual.getEsq() == null){
                        noAtual.setEsq(novoNo);
                        novoNo.setPai(noAtual);
                        System.out.println(dado + " foi inserido a esquerda de " + noAtual.getInfo());
                    } else {
                        inserir(noAtual.getEsq(), dado);
                    }
                    //se o valor do dado for MAIOR que o valor do noAtual verifica se o lado DIREITO do noAtual esta vazio
                    // se o lado DIREITO do noAtual estiver vazio é estanciado o novoNo a DIREITA do noAtual
                    // se o lado DIREITO não estiver vazio é chamado o metodo de inserir com o valor do novoNo como noAtual
                } else if(dado.compareTo(noAtual.getInfo()) > 0){
                    if(noAtual.getDir() == null){
                        noAtual.setDir(novoNo);
                        novoNo.setPai(noAtual);
                        System.out.println(dado + " foi inserido a direita de " + noAtual.getInfo());
                    } else {
                        inserir(noAtual.getDir(), dado);
                    }
                }
            }
        } else {
            throw new Exception("Elemento ja cadastrado");
        }
    }

    //==================================================================== REMOVER ELEMENTOS =================================================================================
    public void remover(T dado) throws Exception {
        if(!estahVazia()){
            if(dado.compareTo(this.raiz.getInfo()) == 0
                    && this.raiz.getEsq() == null
                    && this.raiz.getDir() == null){
                this.raiz = null;
            } else {
                remover(this.raiz, dado);
            }
        } else {
            throw new Exception("A arvore está vazia");
        }
    }

    protected No<T> remover(No<T> atual, T dado) throws Exception {

        //Verifica se exite o elemento na arvore
        if (buscar(dado) != null) {
            //se o elemento existir verifica se o elemento é menor que o valor atual
            //se for menor remove pela esquerda
            if (dado.compareTo(atual.getInfo()) < 0) {
                atual.setEsq(remover(atual.getEsq(), dado));
                //se o elemento for maior remove pela direita
            } else if (dado.compareTo(atual.getInfo()) > 0) {
                atual.setDir(remover(atual.getDir(), dado));
                //se o elemento a ser removido tiver 2 filhos
            } else if (atual.getEsq() != null && atual.getDir() != null) {
                atual.setInfo(noMaisDireita(atual.getEsq()).getInfo());
                atual.setEsq(removerAnteriorEsq(atual.getEsq()));
            } else {
                //se o elemento tiver so um filho e não for raiz
                if (atual.getPai() != null){
                    if (atual.getEsq() != null) {
                        atual = atual.getEsq();
                    } else {
                        atual = atual.getDir();
                    }
                } else {
                    //se o elemento tiver so um filho e for a raiz
                    if (atual.getEsq() != null) {
                        atual.setInfo(atual.getEsq().getInfo());
                        atual.setEsq(removerAnteriorEsq(atual.getEsq()));
                    } else {
                        atual.setInfo(atual.getDir().getInfo());
                        atual.setDir(removerAnteriorDir(atual.getDir()));
                    }
                }
            }
        } else {
            throw new Exception("Elemento não encontrado");
        }
        return atual;
    }

    //======================================================================= BUSCAR ELEMENTO ==================================================================================
    public T buscar(T dado) throws Exception{
        return buscar(this.raiz, dado);
    }

    private T buscar(No<T> atual, T dado) throws Exception{
        if(!estahVazia()){
            if(atual != null){
                if (atual.getInfo().compareTo(dado) == 0){
                    return atual.getInfo();
                }
                if (atual.getInfo().compareTo(dado) >= 0){
                    return buscar(atual.getEsq(), dado);
                } else {
                    return buscar(atual.getDir(), dado);
                }
            } else {
                return null;
            }
        }
        return null;
    }

    //======================================================================= IMPRIMIR ARVORE ==================================================================================
    public void imprimirCrescente(IPrint print) throws Exception{
        imprimirLRD(this.raiz, print);
    }
    public void imprimirDecrescente(IPrint print) throws Exception{
        imprimirDRL(this.raiz, print);
    }

    public void imprimirPreOrdem(IPrint print) throws Exception{
        imprimirRLD(this.raiz, print);
    }

    public void imprimirPosOrdem(IPrint print) throws Exception{
        imprimirRDL(this.raiz, print);
    }

    private void imprimirLRD(No<T> atual, IPrint print) throws Exception{
        if(atual != null){
            imprimirLRD(atual.getEsq(), print);
            print.print(atual.getInfo());
            imprimirLRD(atual.getDir(),print);
        }
    }

    private void imprimirDRL(No<T> atual, IPrint print) throws Exception{
        if(atual != null){
            imprimirDRL(atual.getDir(),print);
            print.print(atual.getInfo());
            imprimirDRL(atual.getEsq(), print);
        }
    }

    private void imprimirRLD(No<T> atual, IPrint print) throws Exception{
        if(atual != null){
            print.print(atual.getInfo());
            imprimirDRL(atual.getEsq(), print);
            imprimirDRL(atual.getDir(),print);
        }
    }

    private void imprimirRDL(No<T> atual, IPrint print) throws Exception{
        if(atual != null){
            print.print(atual.getInfo());
            imprimirDRL(atual.getDir(),print);
            imprimirDRL(atual.getEsq(), print);
        }
    }

    //========================================================================= VERIFICAR ALTURA ===============================================================================
    public int altura(){
        return altura(this.raiz);
    }

    protected int altura(No<T> atual){
        if(atual != null){
           int contEsq = altura(atual.getEsq());
           int contDir = altura(atual.getDir());

           if(contEsq > contDir){
               return 1 + contEsq;
           } else {
               return 1 + contDir;
           }
       }
       return 0;
    }

    //=================================================================== VERIFICAR QUANTIDADE DE NÓS ==========================================================================
     private int quantNos(No<T> atual){
        if(atual != null){
            int contEsq = quantNos(atual.getEsq());
            int contDir = quantNos(atual.getDir());
            return 1 + (contDir + contEsq);
        }
        return 0;
    }

    //===================================================================== MÉTODOS AUXILIARES =================================================================================

    private No<T> noMaisEsquerda(No<T> atual){
        if(atual.getEsq() == null){
            return atual;
        }
        return noMaisEsquerda(atual.getEsq());
    }

    private No<T> noMaisDireita(No<T> atual){
        if(atual.getDir() == null){
            return atual;
        }
        return noMaisDireita(atual.getDir());
    }

    private No<T> removerAnteriorEsq(No<T> atual) {
        if (atual != null) {
            if (atual.getDir() != null) {
                atual.setDir(removerAnteriorEsq(atual.getDir()));
                return atual;
            } else {
                return atual.getEsq();
            }
        }
        return null;
    }

    private No<T> removerAnteriorDir(No<T> atual) {
        if (atual != null) {
            if (atual.getEsq() != null) {
                atual.setEsq(removerAnteriorDir(atual.getEsq()));
                return atual;
            } else {
                return atual.getDir();
            }
        }
        return null;
    }

    public boolean estahVazia(){
        return this.raiz == null;
    }

    public boolean estahCheia(){
        if(!estahVazia()){
            if ((Math.pow(2,altura()) - 1) == quantNos(this.raiz)){
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    //========================================================================== GETS E SETS ===================================================================================
    public No<T> getRaiz() {
        return raiz;
    }

    public void setRaiz(No<T> raiz) {
        this.raiz = raiz;
    }

    @Override
    public String toString() {
        return "ARVORE BINARIA";
    }
}
