package Generics.Lista.ListaDupEncadeada;

public class No<T extends Comparable> {
    private T info;
    private No<T> prox;
    private No<T> ant;

    public No(T info) {
        this.info = info;
        this.prox = null;
        this.ant = null;
    }

    public T getInfo() {
        return info;
    }

    public void setInfo(T info) {
        this.info = info;
    }

    public No<T> getProx() {
        return prox;
    }

    public void setProx(No<T> prox) {
        this.prox = prox;
    }

    public No<T> getAnt() {
        return ant;
    }

    public void setAnt(No<T> ant) {
        this.ant = ant;
    }
}
