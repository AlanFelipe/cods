package Generics.Lista;

public interface ILista<T extends Comparable> {
    void inicializar();
    void incluir(T elemento) throws Exception;
    void incluirInicio(T elemento) throws Exception;
    void incluir(T elemento, int posicao) throws Exception;
    T obterDaPosicao(int posicao)  throws Exception ;
    int obter(T item)  throws Exception ;
    void remover(int posicao) throws Exception ;
    void limpar() throws Exception ;
    int getTamanho()  throws Exception ;
    boolean contem(T item) throws Exception;
    boolean verificarInicializacao()  throws Exception;
    boolean estahCheia() throws Exception;
    boolean estahVazia() throws Exception;
}
