package Generics.Pilha;

public interface IPilha<T> {
    void push(T dado) throws Exception;
    T pop() throws Exception;
    T topo() throws Exception;
    boolean estahVazia();
}
