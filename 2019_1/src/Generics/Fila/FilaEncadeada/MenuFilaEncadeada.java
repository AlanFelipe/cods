package Generics.Fila.FilaEncadeada;

import Generics.Fila.IFila;

import java.util.Scanner;

public class MenuFilaEncadeada {
    public void menu(){
        Scanner input = new Scanner(System.in);

        IFila<Integer> fila = new FilaEncadeada();
        int opc;

        do {
            System.out.println(" ");
            System.out.println("----- FILA ENCADEADA -----");
            System.out.println("[1] Enfileirar elementos");
            System.out.println("[2] Desenfileirar elementos");
            System.out.println("[3] Verificar tamanho");
            System.out.println("[4] Listar");
            System.out.println("[0] Sair");
            System.out.print("Opcao: ");
            opc = input.nextInt();

            switch (opc){
                case 1:
                    try {
                        System.out.print("Informe o elemento: ");
                        int elemento = input.nextInt();
                        fila.queue(elemento);
                    } catch (Exception e){
                        System.out.println(e.getMessage());
                    }
                    break;
                case 2:
                    try {
                        System.out.println("Removendo elemento " + fila.deQueue());
                    } catch  (Exception e){
                        System.out.println(e.getMessage());
                    }
                    break;
                case 3:
                    try {
                        System.out.println("Tamanho: " + fila.getTamanho());
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                    break;
                case 4:
                    try {
                        if(fila.getTamanho() != 0){
                            System.out.println("");
                            System.out.print(fila);
                            System.out.println("");
                        } else {
                            System.out.println("## A FILA ESTÁ VAZIA ##");
                        }
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                    break;
                default:
                    opc = 0;
                    System.out.println("Saindo...");
                    break;
            }
        }while(opc != 0);
    }
}
