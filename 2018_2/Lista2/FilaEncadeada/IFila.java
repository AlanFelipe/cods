package Lista.FilaEncadeada;

public interface IFila {
    void queue(Object dado) throws Exception;
    Object deQueue() throws Exception;
    boolean estahVazia();
    int tamanho();
}
