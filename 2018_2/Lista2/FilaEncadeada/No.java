package Lista.FilaEncadeada;

public class No {
    private Object info;
    private No prox;

    public No() {
    }

    public No(Object info, No prox) {
        this.info = info;
        this.prox = prox;
    }

    public Object getInfo() {
        return info;
    }

    public void setInfo(Object info) {
        this.info = info;
    }

    public No getProx() {
        return prox;
    }

    public void setProx(No prox) {
        this.prox = prox;
    }

    @Override
    public String toString() {
        String listar;
        listar = "[" + this.info +"]";
        if(this.prox != null){
            listar += this.prox;
        }
        return listar;
    }
}
