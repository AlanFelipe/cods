package Lista.FilaEncadeada;

public class Fila implements IFila{
    private No inicio;
    private No fim;
    private int tamanho;

    public Fila() {
        this.inicio = null;
        this.fim = null;
        this.tamanho = 0;
    }

    public Fila(No inicio, No fim, int tamanho) {
        this.inicio = inicio;
        this.fim = fim;
        this.tamanho = tamanho;
    }

    @Override
    public void queue(Object dado) throws Exception {
        if(this.inicio == null){
            No novo = new No();
            novo.setInfo(dado);
            novo.setProx(null);
            this.inicio = novo;
            this.fim = this.inicio;
        } else {
            No novo = new No();
            novo.setInfo(dado);
            novo.setProx(null);
            this.fim.setProx(novo);
            this.fim = novo;
        }
        this.tamanho++;
    }

    @Override
    public Object deQueue() throws Exception {
        No aux;
        if (!estahVazia()) {
            aux = this.inicio;
            this.inicio = this.inicio.getProx();
            this.tamanho--;
        } else {
            throw new Exception("## A FILA ESTÁ VAZIA ##");
        }
        return aux.getInfo();
    }

    @Override
    public boolean estahVazia() {
        if(this.tamanho == 0){
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int tamanho() {
        return this.tamanho;
    }

    public No getInicio() {
        return inicio;
    }

    public void setInicio(No inicio) {
        this.inicio = inicio;
    }

    public No getFim() {
        return fim;
    }

    public void setFim(No fim) {
        this.fim = fim;
    }

    public int getTamanho() {
        return tamanho;
    }

    public void setTamanho(int tamanho) {
        this.tamanho = tamanho;
    }

    public String toString() {
        return this.inicio.toString();
    }
}
