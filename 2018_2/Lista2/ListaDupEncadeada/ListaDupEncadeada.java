package Lista.ListaDupEncadeada;

public class ListaDupEncadeada implements ILista {

    private No inicio;
    private No fim;
    private int tamanho;
    private boolean inicializar;

    public ListaDupEncadeada() {
        inicializar();
    }

    public ListaDupEncadeada(No inicio, No fim, int tamanho, boolean inicializar) {
        this.inicio = inicio;
        this.fim = fim;
        this.tamanho = tamanho;
        this.inicializar = inicializar;
    }

    @Override
    public void inicializar() {
        this.inicio = inicio;
        this.fim = fim;
        this.tamanho = 0;
        this.inicializar = inicializar;
    }

    @Override
    public void incluir(Object elemento) throws Exception {
        if(this.inicio == null){
            No novo = new No();
            novo.setInfo(elemento);
            novo.setProx(null);
            novo.setAnt(null);
            this.inicio = novo;
            this.fim = this.inicio;
        } else {
            No novo = new No();
            novo.setInfo(elemento);
            novo.setProx(null);
            novo.setAnt(this.fim);
            this.fim.setProx(novo);
            this.fim = novo;
        }
        this.tamanho++;
    }

    @Override
    public void incluirInicio(Object elemento) throws Exception {
        if(this.inicio == null){
            No novo = new No();
            novo.setInfo(elemento);
            novo.setProx(null);
            novo.setAnt(null);
            this.inicio = novo;
            this.fim = this.inicio;
        } else {
            No novo = new No();
            novo.setInfo(elemento);
            novo.setAnt(null);
            novo.setProx(this.inicio);
            this.inicio.setAnt(novo);
            this.inicio = novo;
        }
        this.tamanho++;
    }

    @Override
    public void incluir(Object elemento, int posicao) throws Exception {
        No novo = new No();
        novo.setInfo(elemento);

        if (posicao == 0) {
            incluirInicio(elemento);
        } else if (posicao == (this.tamanho)) {
            incluir(elemento);
        } else if (posicao > this.tamanho) {
            incluir(elemento);
            throw new Exception("## ERRO: POSIÇÃO INVALIDA, FOI ADCIONADO NO FINAL DA LISTA ##");
        } else {
            No anterior = obterNo(posicao-1);
            No proximo = anterior.getProx();
            anterior.setProx(novo);
            novo.setProx(proximo);
            proximo.setAnt(novo);
            novo.setAnt(anterior);
            tamanho++;
        }
    }

    @Override
    public Object obterDaPosicao(int posicao) throws Exception {
        if(!estahVazia()){
            if(posicao < this.tamanho){
                No aux = this.inicio;
                for (int i = 1; i <= posicao; i++) {
                    aux = aux.getProx();
                }
                return aux.getInfo();
            } else {
                throw new Exception("## ERRO: POSIÇÃO INVALIDA ##");
            }
        } else {
            throw new Exception("## A LISTA ESTÁ VAZIA ##");
        }
    }

    @Override
    public int obter(Object item) throws Exception {
        No aux = this.inicio;
        for (int i = 0; i < this.tamanho; i++) {
            if(aux.getInfo().equals(item)){
                return i;
            }
            aux = aux.getProx();
        }
        throw new Exception("## ERRO: NÃO EXISTE ESTE ELEMENTO NA LISTA ##");
    }

    private No obterNo(int posicao) throws Exception {

        int i;
        if (posicao >= 0 || posicao < tamanho) {

            No temporario = this.inicio;

            for (i = 0; i < posicao; i++) {
                temporario = temporario.getProx();
            }

            return temporario;

        } else {
            throw new Exception ("## ERRO: POSIÇÃO INVALIDA ##");
        }
    }

    @Override
    public void remover(int posicao) throws Exception {
        if(!estahVazia()){
            if (posicao == 0) {
                this.removeInicio();
            } else if (posicao == this.tamanho - 1) {
                this.removeFim();
            } else if (posicao >= this.tamanho) {
                throw new Exception("## ERRO: POSIÇÃO INVALIDA ##");
            } else {
                No noRemovido = obterNo(posicao);
                No anterior = noRemovido.getAnt();
                anterior.setProx(noRemovido.getProx());
                noRemovido.getProx().setAnt(anterior);
                this.tamanho--;
            }
        } else {
            throw new Exception("## A LISTA ESTÁ VAZIA ##");
        }
    }

    public Object removeInicio() throws Exception {
        No aux;
        if (!estahVazia()) {
            if(this.tamanho == 1){
                aux = this.inicio;
                this.inicio = null;
                this.fim = null;
                this.tamanho--;
            } else {
                aux = this.inicio;
                this.inicio = this.inicio.getProx();
                this.inicio.setAnt(null);
                this.tamanho--;
            }
        } else {
            throw new Exception("## A LISTA ESTÁ VAZIA ##");
        }
        return aux;
    }

    public void removeFim() throws Exception {
        if (!estahVazia()) {
            if (this.tamanho == 1) {
                this.inicio = null;
                this.fim = null;
                this.tamanho--;
            } else {
                No anterior = this.fim.getAnt();
                anterior.setProx(null);
                this.fim = anterior;
                this.tamanho--;
            }
        } else {
            throw new Exception("## A LISTA ESTÁ VAZIA ##");
        }
    }

    @Override
    public void limpar() throws Exception {
        if(this.tamanho != 0){
            inicializar();
            System.out.println("REMOVENDO TODOS OS DADOS...");
        } else {
            throw new Exception("## A LISTA ESTÁ VAZIA ##");
        }
    }

    @Override
    public int getTamanho() throws Exception {
        return this.tamanho;
    }

    @Override
    public boolean contem(Object item) throws Exception {
        No aux = this.inicio;
        for (int i = 0; i < this.tamanho; i++) {
            if(aux.getInfo().equals(item)){
                return true;
            }
            aux = aux.getProx();
        }
        throw new Exception("## ERRO: NÃO CONTÉM ESTE ELEMENTO NA LISTA ##");
    }

    @Override
    public boolean verificarInicializacao() throws Exception {
        if (this.inicializar) {
            return true;
        } else {
            throw new Exception("## ERRO: A LISTA NÃO FOI INICIALIZADA ##");
        }
    }

    @Override
    public boolean estahCheia() throws Exception {
        return false;
    }

    public boolean estahVazia() throws Exception {
        if(this.tamanho == 0){
            return true;
        } else {
            return false;
        }
    }

    public No getInicio() {
        return inicio;
    }

    public void setInicio(No inicio) {
        this.inicio = inicio;
    }

    public No getFim() {
        return fim;
    }

    public void setFim(No fim) {
        this.fim = fim;
    }

    public void setTamanho(int tamanho) {
        this.tamanho = tamanho;
    }

    public boolean isInicializar() {
        return inicializar;
    }

    public void setInicializar(boolean inicializar) {
        this.inicializar = inicializar;
    }

    @Override
    public String toString() {
        return this.inicio.toString();
    }
}
