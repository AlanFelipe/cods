package Lista.ListaEncadeada;

public class ListaEncadeada implements ILista {

    private No inicio;
    private No fim;
    private int tamanho;
    private boolean inicializar;

    public ListaEncadeada() {
        inicializar();
    }

    public ListaEncadeada(No inicio, No fim, int tamanho, boolean inicializar) {
        this.inicio = inicio;
        this.fim = fim;
        this.tamanho = 0;
        this.inicializar = inicializar;
    }

    @Override
    public void inicializar() {
        this.inicio = null;
        this.fim = null;
        this.tamanho = 0;
        this.inicializar = true;
    }

    @Override
    public void incluir(Object elemento) throws Exception {
        if(this.inicio == null){
            No novo = new No();
            novo.setInfo(elemento);
            novo.setProx(null);
            this.inicio = novo;
            this.fim = this.inicio;
        } else {
            No novo = new No();
            novo.setInfo(elemento);
            novo.setProx(null);
            this.fim.setProx(novo);
            this.fim = novo;
        }
        this.tamanho++;
    }

    public void incluirOrdenado(Comparable elemento) throws Exception {
        No novo = new No();
        No atual = this.inicio;
        No anterior = null;
        novo.setInfo(elemento);

        if(atual == null){
            novo.setProx(null);
            this.inicio = novo;
            this.fim = inicio;
        } else {
            while (atual != null && elemento.compareTo(atual.getInfo()) > 0){
                anterior = atual;
                atual = atual.getProx();
            }
            novo.setProx(atual);
            if(anterior == null){
                this.inicio = novo;
            } else {
                anterior.setProx(novo);
            }
        }
        this.tamanho++;
    }

    @Override
    public void incluirInicio(Object elemento) throws Exception {
        if(this.inicio == null){
            No novo = new No();
            novo.setInfo(elemento);
            novo.setProx(null);
            this.inicio = novo;
            this.fim = this.inicio;
        } else {
            No novo = new No();
            novo.setInfo(elemento);
            novo.setProx(this.inicio);
            this.inicio = novo;
        }
        this.tamanho++;
    }

    @Override
    public void incluir(Object elemento, int posicao) throws Exception {
        No novo = new No();
        novo.setInfo(elemento);
        No anterior = null;
        No aux = this.inicio;
        int cont = 0;

        if (posicao == 0) {
            incluirInicio(elemento);
        } else if (posicao == (this.tamanho)) {
            incluir(elemento);
        } else if (posicao > this.tamanho) {
            incluir(elemento);
            throw new Exception("## ERRO: POSIÇÃO INVALIDA, FOI ADCIONADO NO FINAL DA LISTA ##");
        } else {
            while ((aux != null) && (cont != posicao)){
                anterior = aux;
                aux = aux.getProx();
                cont++;
            }
            anterior.setProx(novo);
            novo.setProx(aux);
            this.tamanho++;
        }
    }

    @Override
    public Object obterDaPosicao(int posicao) throws Exception {
        if(!estahVazia()){
            if(posicao < this.tamanho){
                No aux = this.inicio;
                for (int i = 1; i <= posicao; i++) {
                    aux = aux.getProx();
                }
                return aux.getInfo();
            } else {
                throw new Exception("## ERRO: POSIÇÃO INVALIDA ##");
            }
        } else {
            throw new Exception("## A LISTA ESTÁ VAZIA ##");
        }
    }

    @Override
    public int obter(Object item) throws Exception {
        No aux = this.inicio;
        for (int i = 0; i < this.tamanho; i++) {
            if(aux.getInfo().equals(item)){
                return i;
            }
            aux = aux.getProx();
        }
        throw new Exception("## ERRO: NÃO EXISTE ESTE ELEMENTO NA LISTA ##");
    }

    @Override
    public void remover(int posicao) throws Exception {
        if(!estahVazia()){
            No aux = this.inicio;
            No anterior = null;

            if (posicao == 0) {
                this.removeInicio();
            } else if (posicao == this.tamanho - 1) {
                this.removeFim();
            } else if (posicao >= this.tamanho) {
                throw new Exception("## ERRO: POSIÇÃO INVALIDA ##");
            } else {
                for (int i = 1; i <= posicao; i++) {
                    anterior = aux;
                    aux = aux.getProx();
                }
                anterior.setProx(aux.getProx());
                this.tamanho--;
            }
        } else {
            throw new Exception("## A LISTA ESTÁ VAZIA ##");
        }
    }

    public Object removeInicio() throws Exception {
        No aux;
        if (!estahVazia()) {
            aux = this.inicio;
            this.inicio = this.inicio.getProx();
            this.tamanho--;
        } else {
            throw new Exception("## A LISTA ESTÁ VAZIA ##");
        }
        return aux;
    }

    public void removeFim() throws Exception {
        if (!estahVazia()) {
            if (this.fim == this.inicio) {
                this.inicio = null;
                this.fim = null;
                this.tamanho--;
            } else {
                No aux, aux2;
                aux = this.inicio;
                aux2 = aux;
                while (aux.getProx() != null) {
                    aux2 = aux;
                    aux = aux.getProx();
                }
                aux2.setProx(null);
                this.fim = aux2;
                this.fim = aux2;
                this.tamanho--;
            }
        } else {
            throw new Exception("## A LISTA ESTÁ VAZIA ##");
        }
    }

    @Override
    public void limpar() throws Exception {
        if(this.tamanho != 0){
            inicializar();
            System.out.println("REMOVENDO TODOS OS DADOS...");
        } else {
            throw new Exception("## A LISTA ESTÁ VAZIA ##");
        }
    }

    @Override
    public int getTamanho() throws Exception {
        return this.tamanho;
    }

    @Override
    public boolean contem(Object item) throws Exception {
        No aux = this.inicio;
        for (int i = 0; i < this.tamanho; i++) {
            if(aux.getInfo().equals(item)){
                return true;
            }
            aux = aux.getProx();
        }
        throw new Exception("## ERRO: NÃO CONTÉM ESTE ELEMENTO NA LISTA ##");
    }

    @Override
    public boolean verificarInicializacao() throws Exception {
        if (this.inicializar) {
            return true;
        } else {
            throw new Exception("## ERRO: A LISTA NÃO FOI INICIALIZADA ##");
        }
    }

    @Override
    public boolean estahCheia() throws Exception {
        return false;
    }

    public boolean estahVazia() throws Exception {
        if(this.tamanho == 0){
            return true;
        } else {
            return false;
        }
    }

    public No getInicio() {
        return inicio;
    }

    public void setInicio(No inicio) {
        this.inicio = inicio;
    }

    public No getFim() {
        return fim;
    }

    public void setFim(No fim) {
        this.fim = fim;
    }

    public void setTamanho(int tamanho) {
        this.tamanho = tamanho;
    }

    public boolean isInicializar() {
        return inicializar;
    }

    public void setInicializar(boolean inicializar) {
        this.inicializar = inicializar;
    }

    @Override
    public String toString() {
        return this.inicio.toString();
    }
}
