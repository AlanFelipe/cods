package Lista.ListaCircular;

import Lista.ListaEncadeada.ListaEncadeada;

import java.util.Scanner;

public class Principal {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);

        ListaCircular lista = new ListaCircular();

        int opc;

        do {
            System.out.println(" ");
            System.out.println("[1] Adcionar elementos");
            System.out.println("[2] Adcionar elementos no inicio");
            System.out.println("[3] Adcionar elementos na posição");
            System.out.println("[4] Obter elemento da posição");
            System.out.println("[5] Obter elemento");
            System.out.println("[6] Remover elemento");
            System.out.println("[7] Verificar tamanho da lista");
            System.out.println("[8] Verificar se contem o elemento");
            System.out.println("[9] Listar elementos");
            System.out.println("[10] Limpar lista");
            System.out.println("[0] Sair");
            System.out.print("Opcao: ");
            opc = input.nextInt();

            switch (opc){
                case 1:
                    System.out.print("Informe o nome: ");
                    String elemento = input.next();
                    try {
                        lista.incluir(elemento);
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                    break;
                case 2:
                    System.out.print("Informe o nome: ");
                    Object elemento1 = input.next();
                    try {
                        lista.incluirInicio(elemento1);
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                    break;
                case 3:
                    System.out.print("Informe o nome: ");
                    Object elemento2 = input.next();
                    System.out.print("Informe a posição: ");
                    int pos = input.nextInt();
                    try {
                        lista.incluir(elemento2,pos);
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                    break;
                case 4:
                    System.out.print("Informe a posição: ");
                    int pos1 = input.nextInt();
                    try {
                        System.out.println("Elemento: " + lista.obterDaPosicao(pos1));
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                    break;
                case 5:
                    System.out.print("Informe o elemento: ");
                    Object elem = input.next();
                    try {
                        System.out.print("Encontra-se na posição: " + lista.obter(elem));
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                    break;
                case 6:
                    try {
                        System.out.print("Informe a posição: ");
                        int posicao = input.nextInt();
                        lista.remover(posicao);
                        System.out.println("Removendo elemento...");
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                    break;
                case 7:
                    try {
                        System.out.println("Tamaho da lista: " + lista.getTamanho());
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                    break;
                case 8:
                    System.out.print("Informe o elemento: ");
                    Object ele = input.next();
                    try {
                        System.out.println("Contém este elemento na lista? -> " + lista.contem(ele) + " <-");
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                    break;
                case 9:
                    try {
                        if(lista.getTamanho() != 0){
                            System.out.println("");
                            lista.listar();
                            System.out.println("");
                        } else {
                            System.out.println("## A LISTA ESTÁ VAZIA ##");
                        }
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                    break;
                case 10:
                    try {
                        lista.limpar();
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                    break;
                default:
                    opc = 0;
                    System.out.println("Saindo...");
                    break;
            }
        }while(opc != 0);
    }
}
