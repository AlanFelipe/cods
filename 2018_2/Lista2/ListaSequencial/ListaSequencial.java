package Lista.ListaSequencial;

public class ListaSequencial implements ILista {
    Object[] lista;
    private int iniLista;
    private int fimLista;
    private boolean inicializar;


    public ListaSequencial() {
    }

    public ListaSequencial(int capacidade) {
        inicializar(capacidade);
    }

    public ListaSequencial(Object[] lista, int iniLista, int fimLista, int capacidade) {
        this.lista = lista;
        this.iniLista = iniLista;
        this.fimLista = fimLista;
    }

    @Override //inicializar lista
    public void inicializar(int quantidadeMaxima) {
        this.lista = new Object[quantidadeMaxima];
        this.iniLista = 0;
        this.fimLista = 0;
        this.inicializar = true;
    }

    @Override //inserir elementos no fim da lista
    public void incluir(Object elemento) throws Exception {
        verificarInicializacao();
        if (this.fimLista < this.lista.length) {
            this.lista[fimLista] = elemento;
            this.fimLista++;
        } else {
            throw new Exception("## ERRO: A LISTA ESTÁ CHEIA ##");
        }
    }

    //inserir elementos de maneira ordenada Crescente
    public void incluirOrdenado(Comparable elemento) throws Exception {
        verificarInicializacao();
        if (this.fimLista < this.lista.length) {
            int i = fimLista;
            while ((i > 0) && (elemento.compareTo(lista[i - 1]) < 0)){
                lista[i] = lista[i - 1];
                i--;
            }
            this.lista[i] = elemento;
            this.fimLista++;
        }
        else {
            throw new Exception("## ERRO: A LISTA ESTÁ CHEIA ##");
        }
    }

    @Override //inserir elementos sempre no inicio da lista
    public void incluirInicio(Object elemento) throws Exception {
        verificarInicializacao();
        if(this.fimLista < this.lista.length){
            deslocaDireita(this.iniLista);
            this.lista[iniLista] = elemento;
            this.fimLista++;
        } else {
            throw new Exception("## ERRO: A LISTA ESTÁ CHEIA ##");
        }
    }

    @Override //inserir elementos em determinada posição da lista
    public void incluir(Object elemento, int posicao) throws Exception {
        verificarInicializacao();
        if(this.fimLista < this.lista.length) {
            if(posicao >= fimLista){
                incluir(elemento);
            } else {
                deslocaDireita(posicao);
                this.lista[posicao] = elemento;
                this.fimLista++;
            }
        } else {
            throw new Exception("## ERRO: A LISTA ESTÁ CHEIA ##");
        }
    }

    @Override //saber qual o elemento de determinada posição
    public Object obterDaPosicao(int posicao) throws Exception {
        verificarInicializacao();
        if(posicao < this.fimLista){
            return this.lista[posicao];
        } else {
            throw new Exception("## ERRO: NÃO EXISTE DADOS DESSA POSIÇÃO ##");
        }
    }

    @Override //saber a posição de determinado elemento
    public int obter(Object item) throws Exception {
        verificarInicializacao();
        for (int i = 0; i < this.fimLista; i++) {
            if(this.lista[i].equals(item)){
                return i;
            }
        }
        throw new Exception("## ERRO: NÃO EXISTE ESTE ELEMENTO NA LISTA ##");
    }

    //realizar a busca binaria de determinado elemento
    public Object buscaBinaria(Object elemento) throws Exception{
        verificarInicializacao();
        int ini = this.iniLista;
        int fim = this.fimLista - 1;
        int meio;

        while (ini <= fim){
            meio = (ini + fim) / 2;
            if(this.lista[meio].equals(elemento)){
                return meio;
            }
            if((Integer) elemento < (Integer) this.lista[meio]){
                fim = meio - 1;
            } else {
                ini = meio + 1;
            }
        }
        throw new Exception("## ERRO: NÃO EXISTE ESTE ELEMENTO NA LISTA ##");
    }


    @Override //remover elemento de determinada posição
    public void remover(int posicao) throws Exception {
        verificarInicializacao();
        if(!estahVazia()){
            if(posicao < this.fimLista){
                deslocarEsquerda(posicao);
                this.fimLista--;
            } else {
                throw new Exception("## ERRO: NÃO EXISTE DADOS DESSA POSIÇÃO ##");
            }
        } else {
            throw new Exception("## A LISTA ESTA VAZIA ##");
        }
    }

    //remover determinado elemento de maneira ordenada
    public void removerEmOrdem(Object elemento) throws Exception {
        verificarInicializacao();
        if(!estahVazia()){
            Object posicao = buscaBinaria(elemento);
            if((Integer) posicao >= this.iniLista ){
                deslocarEsquerda((Integer)posicao);
                this.fimLista--;
            } else {
                throw new Exception("## ERRO: NÃO EXISTE ESTE ELEMENTO NA LISTA ##");
            }
        } else {
            throw new Exception("## A LISTA ESTA VAZIA ##");
        }
    }

    @Override //apagar todos os elementos da lista
    public void limpar() throws Exception {
        verificarInicializacao();
        if(!estahVazia()){
            while (!estahVazia()){
                deslocaDireita(this.iniLista);
                this.fimLista--;
            }
        } else {
            throw new Exception("## A LISTA ESTA VAZIA ##");
        }
    }

    @Override //retorna a quantidade de elementos da lista (tamanho)
    public int getTamanho() throws Exception {
        verificarInicializacao();
        if(!estahVazia()){
            return this.fimLista;
        } else {
            throw new Exception("## A LISTA ESTA VAZIA ##");
        }
    }

    @Override //verificar se determinado elemento está na lista
    public boolean contem(Object item) throws Exception {
        verificarInicializacao();
        for (int i = 0; i < this.fimLista; i++) {
            if(this.lista[i].equals(item)){
                return true;
            }
        }
        throw new Exception("## NÃO CONTÉM ESTE ELEMENTO NA LISTA ##");
    }

    @Override //verificar se a lista foi inicializada
    public boolean verificarInicializacao() throws Exception {
        if(this.inicializar){
            return true;
        } else {
            throw new Exception("## ERRO: A LISTA NÃO FOI INICIALIZADA ##");
        }
    }

    @Override //verificar se a lista está cheia
    public boolean estahCheia() throws Exception {
        if(this.fimLista == this.lista.length){
            return true;
        } else {
            throw new Exception("## ERRO: A LISTA ESTÁ CHEIA ##");
        }
    }

    //verificar se a lista está vazia
    public boolean estahVazia() throws Exception{
        if(this.fimLista == 0){
            return true;
        } else {
            throw new Exception("## A LISTA ESTA VAZIA ##");
        }
    }

    //deslocar elementos da lista para a direita
    public void deslocaDireita(int pos){
        for (int i = this.fimLista - 1; i >= pos; i--) {
            this.lista[i + 1] = this.lista[i];
        }
    }

    //deslocar elementos da lista para a esquerda
    public void deslocarEsquerda(int pos){
        for (int i = pos; i < this.fimLista - 1; i++) {
            this.lista[i] = this.lista[i+1];
        }
    }

    public Object[] getLista() {
        return lista;
    }

    public void setLista(Object[] lista) {
        this.lista = lista;
    }

    public int getIniLista() {
        return iniLista;
    }

    public void setIniLista(int iniLista) {
        this.iniLista = iniLista;
    }

    public int getFimLista() {
        return fimLista;
    }

    public void setFimLista(int fimLista) {
        this.fimLista = fimLista;
    }

    public boolean isInicializar() {
        return inicializar;
    }

    public void setInicializar(boolean inicializar) {
        this.inicializar = inicializar;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < this.fimLista; i++) {
            result.append("[" + this.lista[i] + "]");
        }
        return result.toString();
    }
}
