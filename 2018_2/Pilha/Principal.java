package Pilha;

import java.util.Scanner;

public class Principal {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        Pilha pilha = new Pilha(10);
        int opc;

        do {
            System.out.println(" ");
            System.out.println("[1] Empilhar elementos");
            System.out.println("[2] Desempilhar elementos");
            System.out.println("[3] Verificar topo");
            System.out.println("[4] Listar");
            System.out.println("[0] Sair");
            System.out.print("Opcao: ");
            opc = input.nextInt();

            switch (opc){
                case 1:
                    try {
                        System.out.print("Informe o elemento: ");
                        int elemento = input.nextInt();
                        pilha.push(elemento);
                    } catch (Exception e){
                        System.out.println(e.getMessage());
                    }
                    break;
                case 2:
                    try {
                        System.out.println("Removendo elemento " + pilha.pop());
                    } catch  (Exception e){
                        System.out.println(e.getMessage());
                    }
                    break;
                case 3:
                    try {
                        System.out.println("Topo: " + pilha.topo());
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                    break;
                case 4:
                    System.out.print(pilha);
                    break;
                default:
                    opc = 0;
                    System.out.println("Saindo...");
                    break;
            }
        }while(opc != 0);
    }
}

