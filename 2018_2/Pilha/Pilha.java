package Pilha;

import java.util.Arrays;

public class Pilha implements IPilha{
    private Object[] dados;
    private int topo;

    public Pilha() {
    }

    public Pilha(int capacidade) {
        this.dados = new Object[capacidade];
        this.topo = 0;
    }

    public Pilha(Object[] dados, int topo) {
        this.dados = dados;
        this.topo = topo;
    }

    @Override
    public void push(Object dado) throws Exception {
        aumentaCapacidade();
        if(this.topo < this.dados.length){
            this.dados[this.topo] = dado;
            this.topo++;
        } else {
            throw new Exception("## ERRO: A PILHA ESTA CHEIA ##");
        }
    }

    @Override
    public Object pop() throws Exception {
        if(!estahVazia()){
            Object dadoRemovido = this.dados[this.topo-1];
            this.dados[topo-1] = null;
            this.topo--;
            return dadoRemovido;
        } else {
            throw new Exception("## ERRO: A PILHA ESTA VAZIA ##");
        }
    }

    @Override
    public Object topo() throws Exception {
        if(!this.estahVazia()){
            return this.dados[this.topo-1];
        } else {
            throw new Exception("## ERRO: A PILHA ESTA VAZIA ##");
        }
    }

    @Override
    public boolean estahVazia() {
        if(this.topo == 0){
            return true;
        } else {
            return false;
        }
    }

    private void aumentaCapacidade(){
        int percentual = ((this.dados.length * 80) / 100);
        if(this.topo == percentual){
            Object[] dadosNovos = new Object[this.dados.length * 2];
            for (int i = 0; i < this.dados.length; i++) {
                dadosNovos[i] = this.dados[i];
            }
            this.dados = dadosNovos;
        }
    }

    public Object[] getDados() {
        return dados;
    }

    public void setDados(Object[] dados) {
        this.dados = dados;
    }

    public int getTopo() {
        return topo;
    }

    public void setTopo(int topo) {
        this.topo = topo;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < this.dados.length; i++) {
            result.append("[" + this.dados[i] + "]");
        }
        return result.toString();
    }
}
