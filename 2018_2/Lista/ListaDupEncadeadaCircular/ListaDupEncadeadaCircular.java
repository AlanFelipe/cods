package Lista.ListaDupEncadeadaCircular;

import Lista.ListaDupEncadeada.ListaDupEncadeada;
import Lista.ListaDupEncadeada.No;

public class ListaDupEncadeadaCircular extends ListaDupEncadeada {
    @Override
    public void incluir(Object elemento) throws Exception {
        No novo = new No(elemento);
        if (getInicio() == null) {
            setFim(novo);
            setInicio(novo);
            novo.setAnt(novo);
            novo.setProx(novo);
        } else {
            novo.setAnt(getFim());
            novo.setProx(getInicio());
            getFim().setProx(novo);
            getInicio().setAnt(novo);
            setFim(novo);
        }
        setTamanho(getTamanho() + 1);
    }

    @Override
    public void incluirInicio(Object elemento) throws Exception {
        No novo = new No(elemento);
        if (getInicio() == null) {
            setFim(novo);
            setInicio(novo);
            novo.setProx(novo);
            novo.setAnt(novo);
        } else {
            novo.setProx(getInicio());
            novo.setAnt(getFim());
            getFim().setProx(novo);
            getInicio().setAnt(novo);
            setInicio(novo);
        }
        setTamanho(getTamanho() + 1);
    }

    @Override
    public Object removeInicio() throws Exception {
        No temp = getInicio();
        if (!estahVazia()) {
            if(getTamanho() == 1) {
                setInicio(null);
                setFim(null);
                setTamanho(getTamanho() - 1);
            } else {
                setInicio(getInicio().getProx());
                getInicio().setAnt(getFim());
                getFim().setProx(getInicio());
                setTamanho(getTamanho() - 1);
            }
        } else {
            throw new Exception("## A LISTA ESTÁ VAZIA ##");
        }
        return temp.getInfo();
    }

    @Override
    public Object removeFim() throws Exception {
        No temp = getFim();
        if (!estahVazia()) {
            if (getTamanho() == 1) {
                setInicio(null);
                setFim(null);
                setTamanho(getTamanho() - 1);
            } else {
                No anterior = getFim().getAnt();
                anterior.setProx(getInicio());
                setFim(anterior);
                setTamanho(getTamanho() - 1);
            }
        } else {
            throw new Exception("## A LISTA ESTÁ VAZIA ##");
        }
        return getFim().getInfo();
    }

    @Override
    public String toString(){
        try {
            if(getTamanho() != 0){
                StringBuilder s = new StringBuilder();

                No aux = getInicio();

                if (aux == getInicio()) {
                    s.append("[" + aux.getInfo() + "]" + "\n");
                    aux = aux.getProx();
                }

                while (aux != getFim()) {
                    s.append("[" + aux.getInfo() + "]" + "\n");
                    aux = aux.getProx();
                }

                if(aux == getFim() && aux != getInicio()){
                    s.append("[" + aux.getInfo() + "]" + "\n");
                }

                return s.toString();
            } else {
                return "## A LISTA ESTÁ VAZIA ##";
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return null;
    }
}
