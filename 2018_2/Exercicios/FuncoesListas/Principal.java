package Exercicios.FuncoesListas;

import java.util.Scanner;

public class Principal {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        Funcoes lista1 = new Funcoes();
        Funcoes lista2 = new Funcoes();
        int opc;

        do {
            System.out.println("[1] INSERIR ELEMENTOS");
            System.out.println("[2] REMOVER ELEMENTOS");
            System.out.println("[3] UNIÃO LISTAS");
            System.out.println("[4] SEPARA LISTA");
            System.out.println("[5] VERIFICAR SE SAO IGUAIS");
            System.out.println("[6] LISTAR");
            System.out.println("[0] SAIR");
            System.out.print("OPÇÃO: ");
            opc = input.nextInt();

            switch (opc){
                case 1:
                    int opc2;
                    do {
                        System.out.println("[1] LISTA A");
                        System.out.println("[2] LISTA B");
                        System.out.println("[0] VOLTAR");
                        System.out.print("OPÇÃO: ");
                        opc2 = input.nextInt();

                        switch (opc2){
                            case 1:
                                System.out.print("Infome o nome: ");
                                Object elem = input.next();
                                try {
                                    lista1.incluir(elem);
                                    System.out.println("");
                                    System.out.println(lista1.toString());
                                } catch (Exception e) {
                                    System.out.println(e.getMessage());
                                }
                                break;
                            case 2:
                                System.out.print("Infome o nome: ");
                                Object elem2 = input.next();
                                try {
                                    lista2.incluir(elem2);
                                    System.out.println("");
                                    System.out.println(lista2.toString());
                                } catch (Exception e) {
                                    System.out.println(e.getMessage());
                                }
                                break;
                                default:
                                    opc2 = 0;
                                    break;
                        }
                    }while (opc2 != 0);
                    break;
                case 2:
                    int opc3;
                    do {
                        System.out.println("[1] LISTA A");
                        System.out.println("[2] LISTA B");
                        System.out.println("[0] VOLTAR");
                        System.out.print("OPÇÃO: ");
                        opc3 = input.nextInt();

                        switch (opc3){
                            case 1:
                                System.out.print("Infome a posição: ");
                                int elem = input.nextInt();
                                try {
                                    lista1.remover(elem);
                                    System.out.println("");
                                    System.out.println(lista1.toString());
                                } catch (Exception e) {
                                    System.out.println(e.getMessage());
                                }
                                break;
                            case 2:
                                System.out.print("Infome a posição: ");
                                int elem2 = input.nextInt();
                                try {
                                    lista2.remover(elem2);
                                    System.out.println("");
                                    System.out.println(lista2.toString());
                                } catch (Exception e) {
                                    System.out.println(e.getMessage());
                                }
                                break;
                                default:
                                    opc3 = 0;
                                    break;
                        }
                    }while (opc3 != 0);
                    break;
                case 3:
                    try {
                        System.out.println(lista1.uniao(lista1,lista2));
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                    break;
                case 4:
                    System.out.print("Informe o numero: ");
                    Object num = input.next();
                    try {
                        lista2.incluir(lista1.separar(num));
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                case 5:
                    try {
                        System.out.println(lista1.iguais(lista1,lista2));
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                    break;
                case 6:
                    System.out.println("-- LISTA A --");
                    System.out.println(lista1.toString());
                    System.out.println("-- LISTA B --");
                    System.out.println(lista2.toString());
                    break;
                default:
                        opc = 0;
                        System.out.println("SAINDO...");
                        break;
            }
        }while (opc != 0);
    }
}
