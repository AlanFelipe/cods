package Exercicios.prova2018_1.Q3;

import java.util.Scanner;

public class Principal extends QuestaoProva {
    final int tamanhoFila = 10;
    Fila f1 = new Fila(tamanhoFila);
    Fila f2 = new Fila(tamanhoFila);
    Fila f3 = new Fila(tamanhoFila);
    Fila f4 = new Fila(tamanhoFila);

    public Principal(int capacidadeMaxPorTipo) {
        super(capacidadeMaxPorTipo);
    }

    public Principal() {
    }

    @Override
    public void adicionar(int valor) {
        if ((valor % 2 == 0) && (valor >= 0)) {
            try {
                f1.queque(valor);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }

        if ((valor % 2 != 0) && (valor >= 0)) {
            try {
                f2.queque(valor);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }

        if ((valor % 2 == 0) && (valor < 0)) {
            try {
                f3.queque(valor);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }

        if ((valor % 2 != 0) && (valor < 0)) {
            try {
                f4.queque(valor);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }

    @Override
    public Object obterProximo(Tipo tipo) {
        Object valor = 0;

        switch (tipo){
            case MaiorQZeroPar:
                try {
                    valor = f1.deQueque();
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
                break;
            case MaiorQZeroImpar:
                try {
                    valor = f2.deQueque();
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
                break;
            case MenorQZeroPar:
                try {
                    valor = f3.deQueque();
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
                break;
            case MenorQZeroImpar:
                try {
                    valor = f4.deQueque();
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
                break;
        }
        return valor;
    }

    @Override
    public void excluirTodos(Tipo tipo) {
        switch (tipo){
            case MaiorQZeroPar:
                f1 = new Fila(tamanhoFila);
                System.out.println("-- OS VALORES MAIORES QUE ZERO PAR FORAM REVOMIDOS -- ");
                break;
            case MaiorQZeroImpar:
                f2 = new Fila(tamanhoFila);
                System.out.println("-- OS VALORES MAIORES QUE ZERO IMPAR FORAM REVOMIDOS -- ");
                break;
            case MenorQZeroPar:
                f3 = new Fila(tamanhoFila);
                System.out.println("-- OS VALORES MENORES QUE ZERO PAR FORAM REVOMIDOS -- ");
                break;
            case MenorQZeroImpar:
                f4 = new Fila(tamanhoFila);
                System.out.println("-- OS VALORES MENORES QUE ZERO IMPAR FORAM REVOMIDOS -- ");
                break;
        }
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        Principal p = new Principal();
        Tipo maiorQZeroPar = Tipo.MaiorQZeroPar;
        Tipo maiorQZeroImpar = Tipo.MenorQZeroImpar;
        Tipo menorQZeroPar = Tipo.MenorQZeroPar;
        Tipo menorQZeroImpar = Tipo.MenorQZeroImpar;

        int opc = 0;
        do {
            System.out.println(" ");
            System.out.println("================= MENU ==================");
            System.out.println("[1] ADICIONAR VALOR");
            System.out.println("[2] OBTER PROXIMO MAIOR QUE ZERO PAR");
            System.out.println("[3] OBTER PROXIMO MAIOR QUE ZERO IMPAR");
            System.out.println("[4] OBTER PROXIMO MENOR QUE ZERO PAR");
            System.out.println("[5] OBTER PROXIMO MENOR QUE ZERO IMPAR");
            System.out.println("[6] EXCLUIR TODOS MAIOR QUE ZERO PAR");
            System.out.println("[7] EXCLUIR TODOS MAIOR QUE ZERO IMPAR");
            System.out.println("[8] EXCLUIR TODOS MENOR QUE ZERO PAR");
            System.out.println("[9] EXCLUIR TODOS MENOR QUE ZERO IMPAR");
            System.out.println("[0] SAIR");
            System.out.println("OPÇÃO: ");
            opc = input.nextInt();

            switch (opc){
                case 1:
                    System.out.print("Informe o valor: ");
                    int num = input.nextInt();
                    p.adicionar(num);
                    System.out.println(" --- FILA MAIOR QUE ZERO PAR --");
                    System.out.println(p.f1);
                    System.out.println(" --- FILA MAIOR QUE ZERO IMPAR --");
                    System.out.println(p.f2);
                    System.out.println(" --- FILA MENOR QUE ZERO PAR --");
                    System.out.println(p.f3);
                    System.out.println(" --- FILA MENOR QUE ZERO IMPAR --");
                    System.out.println(p.f4);
                    break;
                case 2:
                    System.out.println("VALOR: " + p.obterProximo(maiorQZeroPar));
                    break;
                case 3:
                    System.out.println("VALOR: " + p.obterProximo(maiorQZeroImpar));
                    break;
                case 4:
                    System.out.println("VALOR: " + p.obterProximo(menorQZeroPar));
                    break;
                case 5:
                    System.out.println("VALOR: " + p.obterProximo(menorQZeroImpar));
                    break;
                case 6:
                    p.excluirTodos(maiorQZeroPar);
                    break;
                case 7:
                    p.excluirTodos(maiorQZeroImpar);
                    break;
                case 8:
                    p.excluirTodos(menorQZeroPar);
                    break;
                case 9:
                    p.excluirTodos(menorQZeroImpar);
                    break;
                    default:
                        opc = 0;
                        System.out.println(" ");
                        System.out.println("Saindo...");
                        break;
            }
        }while (opc != 0);
    }
}
