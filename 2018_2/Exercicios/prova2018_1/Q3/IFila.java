package Exercicios.prova2018_1.Q3;

public interface IFila {
    void queque(Object dado) throws Exception;
    Object deQueque() throws Exception;
    boolean estahVazia();
    int tamanho();
}
