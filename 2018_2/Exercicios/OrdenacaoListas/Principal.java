package Exercicios.OrdenacaoListas;

import Lista.ListaEncadeada.ListaEncadeada;

import java.util.Arrays;
import java.util.Scanner;

public class Principal {

    public static void main(String[] args) {
            Scanner input = new Scanner(System.in);

        ListaEncadeada lista = new ListaEncadeada();
            //CRIANDO VETOR
            int[] vetor = new int[10];

            //RECEBENDO NUMEROS PRO VETOR
            for (int i = 0; i < vetor.length; i++) {
                System.out.print("Informe o " + (i + 1) + "º numero: ");
                vetor[i] = input.nextInt();
            }

            int opc;
            do {
                System.out.println(" ");
                System.out.println("===== ORDENAÇÃO =====");
                System.out.println("[1] MERGE SORT");
                System.out.println("[2] QUICK SORT");
                System.out.println("[0] SAIR");
                System.out.print("OPCÃO: ");
                opc = input.nextInt();

                switch (opc) {
                    case 1: //CASO ESCOLHA MERGE SORT
                        int opc1;
                        do {
                            System.out.println(" ");
                            System.out.println("[1] ORDEM CRESCENTE");
                            System.out.println("[2] ORDEM DECRESCENTE");
                            System.out.println("[0] VOLTAR");
                            System.out.print("OPÇÃO: ");
                            opc1 = input.nextInt();

                            switch (opc1) {
                                case 1: //ORDENAR EM FORMA CRESCENTE
                                    MergeSort msC = new MergeSort();
                                    msC.mergeSortCrescente(vetor, 0, vetor.length - 1);
                                    System.out.println(Arrays.toString(vetor));
                                    break;
                                case 2: //ORDENAR EM FORMA DECRESCENTE
                                    MergeSort msD = new MergeSort();
                                    msD.mergeSortDecrescente(vetor, 0, vetor.length - 1);
                                    System.out.println(Arrays.toString(vetor));
                                    break;
                                default:
                                    opc1 = 0;
                                    break;
                            }
                        } while (opc1 != 0);
                        break; //FIM DO MERGE SORT
                    case 2: //CASO ESCOLHA QUICK SORT
                        int opc2;

                        do {
                            System.out.println(" ");
                            System.out.println("[1] PIVO NO INICIO");
                            System.out.println("[2] PIVO MEDIA");
                            System.out.println("[3] PIVO NO FIM");
                            System.out.println("[0] VOLTAR");
                            System.out.print("OPÇÃO: ");
                            opc2 = input.nextInt();

                            switch (opc2) {
                                case 1: // PIVO NO INICIO
                                    int opc2a;
                                    do {
                                        System.out.println(" ");
                                        System.out.println("[1] ORDEM CRESCENTE");
                                        System.out.println("[2] ORDEM DECRESCENTE");
                                        System.out.println("[0] VOLTAR");
                                        System.out.print("OPÇÃO: ");
                                        opc2a = input.nextInt();

                                        switch (opc2a) {
                                            case 1: // PIVO NO INICIO EM ORDEM CRESCENTE
                                                QuickSort qsIniCres = new QuickSort();
                                                qsIniCres.pivoInicioCrescente(vetor, 0, vetor.length - 1);
                                                System.out.println(Arrays.toString(vetor));
                                                break;
                                            case 2: // PIVO NO INICIO EM ORDEM DECRESCENTE
                                                QuickSort qsIniDecre = new QuickSort();
                                                qsIniDecre.pivoInicioDecrescente(vetor, 0, vetor.length - 1);
                                                System.out.println(Arrays.toString(vetor));
                                                break;
                                            default:
                                                opc2a = 0;
                                                break;
                                        }
                                    } while (opc2a != 0);
                                    break;// FIM DO PIVO NO INICIO
                                case 2: // PIVO MEDIA
                                    int opc2b;
                                    do {
                                        System.out.println(" ");
                                        System.out.println("[1] ORDEM CRESCENTE");
                                        System.out.println("[2] ORDEM DECRESCENTE");
                                        System.out.println("[0] VOLTAR");
                                        System.out.print("OPÇÃO: ");
                                        opc2b = input.nextInt();

                                        switch (opc2b) {
                                            case 1: // PIVO MEDIA EM ORDEM CRESCENTE
                                                QuickSort qsMedCresc = new QuickSort();
                                                qsMedCresc.pivoMeioCrescente(vetor, 0, vetor.length - 1);
                                                System.out.println(Arrays.toString(vetor));
                                                break;
                                            case 2: // PIVO MEDIA EM ORDEM DECRESCENTE
                                                QuickSort qsMedDecre = new QuickSort();
                                                qsMedDecre.pivoMeioDecrescente(vetor, 0, vetor.length - 1);
                                                System.out.println(Arrays.toString(vetor));
                                                break;
                                            default:
                                                opc2b = 0;
                                                break;
                                        }
                                    } while (opc2b != 0);
                                    break;//FIM DO PIVO MEDIA
                                case 3: // PIVO NO FIM
                                    int opc2c;
                                    do {
                                        System.out.println(" ");
                                        System.out.println("[1] ORDEM CRESCENTE");
                                        System.out.println("[2] ORDEM DECRESCENTE");
                                        System.out.println("[0] VOLTAR");
                                        System.out.print("OPÇÃO: ");
                                        opc2c = input.nextInt();

                                        switch (opc2c) {
                                            case 1: // PIVO NO FIM EM ORDEM CRESCENTE
                                                QuickSort qsFimCres = new QuickSort();
                                                qsFimCres.pivoFimCrescente(vetor, 0, vetor.length - 1);
                                                System.out.println(Arrays.toString(vetor));
                                                break;
                                            case 2: // PIVO NO FIM EM ORDEM DECRESCENTE
                                                QuickSort qsFimDecre = new QuickSort();
                                                qsFimDecre.pivoFimDecrescente(vetor, 0, vetor.length - 1);
                                                System.out.println(Arrays.toString(vetor));
                                                break;
                                            default:
                                                opc2c = 0;
                                                break;
                                        }
                                    } while (opc2c != 0);
                                    break;//FIM DO PIVO NO FIM
                                default:
                                    opc2 = 0;
                                    break;
                            }
                        } while (opc2 != 0);
                        break;// FIM DO QUICK SORT
                    default:
                        opc = 0;
                        System.out.println(" ");
                        System.out.println("SAINDO...");
                        break;
                }
            } while (opc != 0);
        }

}
