package Lista.PilhaEncadeada;

import Lista.ListaEncadeada.ListaEncadeada;

public class PilhaEncadeada extends ListaEncadeada implements IPilha{

    @Override
    public void push(Object dado) throws Exception {
        super.incluir(dado);
    }

    @Override
    public Object pop() throws Exception {
        return super.removeFim();
    }

    @Override
    public Object topo() throws Exception {
        if(getFim() != null){
            return super.getFim().getInfo();
        } else {
            throw new Exception("## A PILHA ESTÁ VAZIA ##");
        }
    }
}
