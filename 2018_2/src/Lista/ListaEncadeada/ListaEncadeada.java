package Lista.ListaEncadeada;

import Lista.ILista;

public class ListaEncadeada implements ILista{
    private No inicio;
    private No fim;
    private int tamanho;
    private boolean inicializar;

    public ListaEncadeada() {
        inicializar();
    }

    @Override
    public void inicializar() {
        this.inicio = null;
        this.fim = null;
        this.tamanho = 0;
        this.inicializar = true;
    }


    //incluir elementos no final
    public void incluir(Object elemento) throws Exception {
        No novo = new No(elemento);
        if(this.inicio == null){
            this.inicio = novo;
            this.fim = novo;
        } else {
            this.fim.setProx(novo);
            this.fim = novo;
        }
        this.tamanho++;
    }

    //incluir elementos ordenado
    public void incluirOrdenado(Comparable elemento){
        No novo = new No(elemento);
        No atual = this.inicio;
        No anterior = null;

        if(atual == null){
            this.inicio = novo;
            this.fim = novo;
        } else {
            while (atual != null && elemento.compareTo(atual.getInfo()) > 0){
                anterior = atual;
                atual = atual.getProx();
            }
            novo.setProx(atual);
            if(anterior == null){
                this.inicio = novo;
            } else {
                anterior.setProx(novo);
            }
        }
        this.tamanho++;
    }

    //incluir elementos no inicio
    public void incluirInicio(Object elemento) throws Exception {
        No novo = new No(elemento);
        if(this.inicio == null){
            this.inicio = novo;
            this.fim = novo;
        } else {
            novo.setProx(this.inicio);
            this.inicio = novo;
        }
        this.tamanho++;
    }

    //incluir elementos em determinada posicao
    public void incluir(Object elemento, int posicao) throws Exception {
        No novo = new No(elemento);
        No anterior = null;
        No aux = this.inicio;
        int cont = 0;

        if (posicao == 0) {
            incluirInicio(elemento);
        } else if (posicao == (this.tamanho)) {
            incluir(elemento);
        } else if (posicao > this.tamanho) {
            incluir(elemento);
            throw new Exception("## ERRO: POSIÇÃO INVALIDA, FOI ADCIONADO NO FINAL DA LISTA ##");
        } else {
            while ((aux != null) && (cont != posicao)){
                anterior = aux;
                aux = aux.getProx();
                cont++;
            }
            anterior.setProx(novo);
            novo.setProx(aux);
            this.tamanho++;
        }
    }

    @Override //Obter elementos de determinada posicao
    public Object obterDaPosicao(int posicao) throws Exception {
        if(!estahVazia()){
            if(posicao < this.tamanho){
                No aux = this.inicio;
                for (int i = 0; i < posicao; i++) {
                    aux = aux.getProx();
                }
                return aux.getInfo();
            } else {
                throw new Exception("## ERRO: POSIÇÃO INVALIDA ##");
            }
        } else {
            throw new Exception("## A LISTA ESTÁ VAZIA ##");
        }
    }

    @Override // Obter posicao do elemento
    public int obter(Object item) throws Exception {
        No aux = this.inicio;
        for (int i = 0; i < this.tamanho; i++) {
            if(aux.getInfo().equals(item)){
                return i;
            }
            aux = aux.getProx();
        }
        throw new Exception("## ERRO: NÃO EXISTE ESTE ELEMENTO NA LISTA ##");
    }

    private No obterNo(int posicao) throws Exception {

        int i;
        if (posicao >= 0 || posicao < tamanho) {

            No temporario = this.inicio;

            for (i = 0; i < posicao; i++) {
                temporario = temporario.getProx();
            }

            return temporario;

        } else {
            throw new Exception ("## ERRO: POSIÇÃO INVALIDA ##");
        }
    }

    // remover elemento de determinada posicao
    public void remover(int posicao) throws Exception {
        if(!estahVazia()){
            No aux = this.inicio;
            No anterior = null;

            if (posicao == 0) {
                this.removeInicio();
            } else if (posicao == this.tamanho - 1) {
                this.removeFim();
            } else if (posicao >= this.tamanho) {
                throw new Exception("## ERRO: POSIÇÃO INVALIDA ##");
            } else {
                for (int i = 0; i < posicao; i++) {
                    anterior = aux;
                    aux = aux.getProx();
                }
                anterior.setProx(aux.getProx());
                this.tamanho--;
            }
        } else {
            throw new Exception("## A LISTA ESTÁ VAZIA ##");
        }
    }

    //Remover apenas do inicio
    protected Object removeInicio() throws Exception {
        No aux;
        if (!estahVazia()) {
            aux = this.inicio;
            this.inicio = this.inicio.getProx();
            this.tamanho--;
        } else {
            throw new Exception("## A LISTA ESTÁ VAZIA ##");
        }
        return aux.getInfo();
    }

    //Remover apenas no fim
    protected Object removeFim() throws Exception {
        No temp = this.fim;
        if (!estahVazia()) {
            if (this.fim == this.inicio) {
                this.inicio = null;
                this.fim = null;
                this.tamanho--;
            } else {
                No aux, aux2;
                aux = this.inicio;
                aux2 = aux;
                while (aux.getProx() != null) {
                    aux2 = aux;
                    aux = aux.getProx();
                }
                aux2.setProx(null);
                this.fim = aux2;
                this.tamanho--;
            }
        } else {
            throw new Exception("## A LISTA ESTÁ VAZIA ##");
        }
        return temp.getInfo();
    }

    @Override //remover toda a lista
    public void limpar() throws Exception {
        if(this.tamanho != 0){
            inicializar();
            System.out.println("REMOVENDO TODOS OS DADOS...");
        } else {
            throw new Exception("## A LISTA ESTÁ VAZIA ##");
        }
    }

    @Override //obter tamanho da lista
    public int getTamanho() throws Exception {
        return this.tamanho;
    }

    @Override //verificar se contem o elemento
    public boolean contem(Object item) throws Exception {
        No aux = this.inicio;
        for (int i = 0; i < this.tamanho; i++) {
            if(aux.getInfo().equals(item)){
                return true;
            }
            aux = aux.getProx();
        }
        throw new Exception("## ERRO: NÃO CONTÉM ESTE ELEMENTO NA LISTA ##");
    }

    @Override // verificar se foi inicializado
    public boolean verificarInicializacao() throws Exception {
        if (this.inicializar) {
            return true;
        } else {
            throw new Exception("## ERRO: A LISTA NÃO FOI INICIALIZADA ##");
        }
    }

    @Override // verificar se esta cheio
    public boolean estahCheia() throws Exception {
        return false;
    }

    //verificar se esta vazio
    public boolean estahVazia() throws Exception {
        if(this.tamanho == 0){
            return true;
        } else {
            return false;
        }
    }

    public void inverterElementos(No lista, No ant){
        if(lista.getProx() != null){
            inverterElementos(lista.getProx(), lista);
        }
        lista.setProx(ant);
    }

    public void inverter(Object lista){
        inverterElementos(((ListaEncadeada) lista).getInicio(), null);
        No aux = ((ListaEncadeada) lista).getInicio();
        ((ListaEncadeada) lista).setInicio(((ListaEncadeada) lista).getFim());
        ((ListaEncadeada) lista).setFim(aux);
    }

    public No getInicio() {
        return inicio;
    }

    public void setInicio(No inicio) {
        this.inicio = inicio;
    }

    public No getFim() {
        return fim;
    }

    public void setFim(No fim) {
        this.fim = fim;
    }

    public void setTamanho(int tamanho) {
        this.tamanho = tamanho;
    }

    public boolean isInicializar() {
        return inicializar;
    }

    public void setInicializar(boolean inicializar) {
        this.inicializar = inicializar;
    }

    @Override
    public String toString() {
        if(this.tamanho != 0){
            StringBuilder s = new StringBuilder();
            No aux = this.inicio;

            while (aux != null) {
                s.append("[" + aux.getInfo() + "]");
                aux = aux.getProx();
            }

            return s.toString();
        } else {
            return "## A LISTA ESTÁ VAZIA ##";
        }
    }
}
