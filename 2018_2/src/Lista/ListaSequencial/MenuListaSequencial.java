package Lista.ListaSequencial;

import Lista.ILista;

import java.util.Scanner;

public class MenuListaSequencial {
    public void menu(){
        Scanner input = new Scanner(System.in);

        final int TAMANHO = 10;
        ILista lista = new ListaSequencial(10);
        int opc = 0;


        do {
            System.out.println(" ");
            System.out.println("----- LISTA SEQUENCIAL -----");
            System.out.println("[1] Adcionar elementos");
            System.out.println("[2] Adcionar elementos no inicio");
            System.out.println("[3] Adcionar elementos na posição");
            System.out.println("[4] Obter elemento da posição");
            System.out.println("[5] Obter elemento");
            System.out.println("[6] Remover elemento");
            System.out.println("[7] Verificar tamanho da lista");
            System.out.println("[8] Verificar se contem o elemento");
            System.out.println("[9] Listar elementos");
            System.out.println("[10] Limpar lista");
            System.out.println("[0] Sair");
            System.out.print("Opcao: ");
            opc = input.nextInt();

            switch (opc) {
                case 1:
                    try {
                        System.out.print("Informe o elemento: ");
                        Object elemento = input.nextInt();
                        lista.incluir(elemento);
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                    break;
                case 2:
                    try {
                        System.out.print("Informe o elemento: ");
                        Object elemento = input.nextInt();
                        lista.incluirInicio(elemento);
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                    break;
                case 3:
                    System.out.print("Informe o elemento: ");
                    Object elemento = input.nextInt();
                    System.out.print("Informe a posição para inserir: ");
                    int pos = input.nextInt();
                    try {
                        lista.incluir(elemento, pos);
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                    break;
                case 4:
                    System.out.print("Informe a posição: ");
                    int pos2 = input.nextInt();
                    try {
                        System.out.println("Elemento: " + lista.obterDaPosicao(pos2));
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                    break;
                case 5:
                    System.out.print("Informe o elemento: ");
                    Object elem = input.nextInt();
                    try {
                        System.out.print("Encontra-se na posição: " + lista.obter(elem));
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                    break;
                case 6:
                    System.out.print("Informe a posição: ");
                    int pos3 = input.nextInt();
                    try {
                        System.out.println("Removendo elemento " + lista.obterDaPosicao(pos3));
                        lista.remover(pos3);
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                    break;
                case 7:
                    try {
                        System.out.println("Tamaho da lista: " + lista.getTamanho());
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                    break;
                case 8:
                    System.out.print("Informe o elemento: ");
                    Object ele = input.nextInt();
                    try {
                        System.out.println("Contém este elemento na lista? -> " + lista.contem(ele) + " <-");
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                    break;
                case 9:
                    System.out.println("");
                    System.out.print(lista + "\n");
                    break;
                case 10:
                    try {
                        lista.limpar();
                        System.out.println("LIMPANDO TODA A LISTA...");
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                    break;
                default:
                    opc = 0;
                    System.out.println("Saindo...");
                    break;
            }
        }while(opc != 0);
    }
}
