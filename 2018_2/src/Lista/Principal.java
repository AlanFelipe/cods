package Lista;

import Lista.FilaEncadeada.MenuFilaEncadeada;
import Lista.ListaDupEncadeadaCircular.MenuListaDupEncadeadaCircular;
import Lista.ListaSequencial.MenuListaSequencial;
import Lista.ListaEncadeada.MenuListaEncadeada;
import Lista.ListaDupEncadeada.MenuListaDupEncadeada;
import Lista.ListaEncadeadaCircular.MenuListaEncadeadaCircular;
import Lista.PilhaEncadeada.MenuPilhaEncadeada;

import java.util.Scanner;

public class Principal {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int opc;

        do{
            System.out.println("[1] LISTA SEQUENCIAL");
            System.out.println("[2] LISTA ENCADEADA");
            System.out.println("[3] LISTA DUPLAMENTE ENCADEADA");
            System.out.println("[4] LISTA ENCADEADA CIRCULAR");
            System.out.println("[5] LISTA DUPLAMENTE ENCADEADA CIRCULAR");
            System.out.println("[6] FILA ENCADEADA");
            System.out.println("[7] PILHA ENCADEADA");
            System.out.println("[0] SAIR");
            System.out.print("OPÇÃO: ");
            opc = input.nextInt();

            switch (opc){
                case 1:
                    //MENU LISTA SEQUENCIAL
                    MenuListaSequencial listaSequencial = new MenuListaSequencial();
                    listaSequencial.menu();
                    break;
                case 2:
                    //MENU LISTA ENCADEADA
                    MenuListaEncadeada listaEncadeada = new MenuListaEncadeada();
                    listaEncadeada.menu();
                    break;
                case 3:
                    //MENU LISTA DUPLAMENTE ENCADEADA
                    MenuListaDupEncadeada listaDupEncadeada = new MenuListaDupEncadeada();
                    listaDupEncadeada.menu();
                    break;
                case 4:
                    //MENU LISTA ENCADEADA CIRCULAR
                    MenuListaEncadeadaCircular listaEncadeadaCircular = new MenuListaEncadeadaCircular();
                    listaEncadeadaCircular.menu();
                    break;
                case 5:
                    //MENU LISTA DUPLAMENTE ENCADEADA CIRCULAR
                    MenuListaDupEncadeadaCircular listaDupEncadeadaCircular = new MenuListaDupEncadeadaCircular();
                    listaDupEncadeadaCircular.menu();
                    break;
                case 6:
                    //MENU FILA ENCADEADA
                    MenuFilaEncadeada filaEncadeada = new MenuFilaEncadeada();
                    filaEncadeada.menu();
                    break;
                case 7:
                    //MENU PILHA ENCADEADA
                    MenuPilhaEncadeada pilhaEncadeada = new MenuPilhaEncadeada();
                    pilhaEncadeada.menu();
                    break;
                default:
                    opc = 0;
                    System.out.println("SAINDO...");
                    break;
            }
        }while (opc != 0);
    }
}
