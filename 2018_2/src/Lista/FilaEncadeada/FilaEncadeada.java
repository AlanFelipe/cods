package Lista.FilaEncadeada;

import Lista.ListaEncadeada.ListaEncadeada;

public class FilaEncadeada extends ListaEncadeada implements IFila{

    public void queue(Object dado) throws Exception {
        super.incluir(dado);
    }

    public Object deQueue() throws Exception {
        return super.removeInicio();
    }

    public int tamanho() throws Exception{
        return getTamanho();
    }
}
