package Exercicios.FuncoesListas;

import Lista.ListaEncadeada.ListaEncadeada;
import Lista.ListaEncadeada.No;

public class Funcoes extends ListaEncadeada {
    public Object uniao (ListaEncadeada lista1, ListaEncadeada lista2) throws Exception {
        if(!lista1.estahVazia() || !lista2.estahVazia()){
            No aux = lista1.getInicio();
            No aux2 = null;
            if(aux == null){
                return lista2;
            }
            while (aux != null){
                aux2 = aux;
                aux = aux.getProx();
            }
            aux2.setProx(lista2.getInicio());
            return lista1;
        } else {
            throw new Exception("## AS LISTAS ESTAO VAZIAS ##");
        }
    }

    public boolean iguais(ListaEncadeada lista1, ListaEncadeada lista2) throws Exception {
        int cont = 0;
        No aux = lista1.getInicio();
        No aux2 = lista2.getInicio();
        if(lista1.estahVazia() && lista2.estahVazia()){
            return true;
        } else {
            if(!lista1.estahVazia() && !lista2.estahVazia()){
                if(lista1.getTamanho() == lista2.getTamanho()){

                    while (aux != null && aux2 != null){
                        if(aux.getInfo().equals(aux2.getInfo())){
                            cont++;
                        }
                        aux = aux.getProx();
                        aux2 = aux2.getProx();
                    }
                }
                if (cont == lista2.getTamanho()){
                    return true;
                } else {
                    return false;
                }
            }
            return false;
        }
    }

    public No separar(Object elemento) {
        No aux;
        No aux2;
        for (aux = getInicio();  aux != null ; aux = aux.getProx()) {
            if(aux.getInfo().equals(elemento)){
                aux2 = aux.getProx();
                aux.setProx(null);
             //////////   return aux2;
            }
        }
        return null;
    }



    /*public boolean iguaisRecursivo(ListaEncadeada lista1, ListaEncadeada lista2){
        No aux = lista1.getInicio();
        No aux2 = lista2.getInicio();
        if(aux == null && aux2 == null){
            return true;
        } else  if(aux == null || aux2 == null){
            return false;
        } else {
            iguaisRecursivo(aux.getProx(), aux2.getProx());
            return true;
        }
    }*/

    public Object unirOrdenado(ListaEncadeada lista1, ListaEncadeada lista2) throws Exception {
        ListaEncadeada lista = new ListaEncadeada();
        No list1 = lista1.getInicio();
        No list2 = lista2.getInicio();

        while (lista1.getTamanho() != 0 && lista2.getTamanho() != 0){
            Integer il1, il2;
            il1 = (Integer) list1.getInfo();
            il2 = (Integer) list2.getInfo();

            if(il1 < il2){
                lista.incluir(lista1);
                lista1.remover(0);
            } else {
                lista.incluir(lista2);
                lista2.remover(0);
            }
        }
        while (lista1.getTamanho() != 0){
            lista.incluir(lista1);
            lista1.remover(0);
        }
        while (lista1.getTamanho() != 0){
            lista.incluir(lista2);
            lista2.remover(0);
        }
        lista1 = lista;
        return lista1;
    }
}
