package Exercicios.FilasOrdenadas;

public class Principal{
    Fila f1 = new Fila(5);
    Fila f2 = new Fila(6);

    public static void main(String[] args) throws Exception {
        Principal p = new Principal();
        p.menu();
    }

    public void menu() throws Exception {
        adicionarElementos();
        System.out.println(juntar(f1,f2));
    }

    public void adicionarElementos()  {
        try {
            f1.filaPrioridade(2);
            f1.filaPrioridade(0);
            f1.filaPrioridade(9);
            f1.filaPrioridade(1);
            f1.filaPrioridade(7);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        try {
            f2.filaPrioridade(6);
            f2.filaPrioridade(4);
            f2.filaPrioridade(8);
            f2.filaPrioridade(5);
            f2.filaPrioridade(3);
            f2.filaPrioridade(10);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

    public Object juntar(Fila f1, Fila f2) throws Exception {

        Fila filaOrdenada = new Fila(f1.getDados().length + f2.getDados().length);

        while (f1.tamanho() != 0 && f2.tamanho() != 0) {
            int i = 0;
            Integer if1,if2;
            if1 = (Integer) f1.primeiroElemento();
            if2 = (Integer) f2.primeiroElemento();

            if (if1 < if2) {
                filaOrdenada.queue(f1.deQueue());
            } else {
                filaOrdenada.queue(f2.deQueue());
            }
        }
        while (f1.tamanho() != 0) {
            filaOrdenada.queue(f1.deQueue());
        }
        while (f2.tamanho() != 0) {
            filaOrdenada.queue(f2.deQueue());
        }

        return filaOrdenada;
    }

    /*Fila f1 = new Fila(5);
    Fila f2 = new Fila(6);

    public static void main(String[] args) throws Exception {
        Principal p = new Principal();
        p.menu();
    }

    public void menu() throws Exception {
        adicionarElementos();
        System.out.println(juntar(f1,f2));
    }

    public void adicionarElementos()  {
        try {
            f1.queue(1);
            f1.queue(4);
            f1.queue(6);
            f1.queue(8);
            f1.queue(10);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        try {
            f2.queue(1);
            f2.queue(3);
            f2.queue(5);
            f2.queue(7);
            f2.queue(9);
            f2.queue(11);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }


    public Object juntar(Fila f1, Fila f2) throws Exception {

        Fila filaOrdenada = new Fila(f1.getDados().length + f2.getDados().length);

        while (f1.tamanho() != 0 && f2.tamanho() != 0) {
           Integer if1,if2;
           if1 = (Integer) f1.primeiroElemento();
           if2 = (Integer) f2.primeiroElemento();


            if (if1 < if2) {
                filaOrdenada.queue(f1.deQueue());
            } else {
                filaOrdenada.queue(f2.deQueue());
            }
        }
        while (f1.tamanho() != 0) {
            filaOrdenada.queue(f1.deQueue());
        }
        while (f2.tamanho() != 0) {
            filaOrdenada.queue(f2.deQueue());
        }

        return filaOrdenada;
    }
    */
}