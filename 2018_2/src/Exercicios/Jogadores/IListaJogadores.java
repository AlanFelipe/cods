package Exercicios.Jogadores;

import Lista.ILista;

public interface IListaJogadores extends ILista{
    void removerJogador(String nome) throws Exception;
    Pessoa getProximoJogador() throws Exception;
    Pessoa jogadorAtual() throws Exception;
    Pessoa[] listarProximos(int quant) throws Exception;
    int quantPessoasAFrente(String nome) throws Exception;
    //String listar() throws Exception;
}
