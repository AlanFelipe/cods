package Exercicios.Jogadores;

public class Pessoa {
    String nome;
    int quantJogadas;

    public Pessoa() {
    }

    public Pessoa(String nome, int quantJogadas) {
        this.nome = nome;
        this.quantJogadas = quantJogadas;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getQuantJogadas() {
        return quantJogadas;
    }

    public void setQuantJogadas(int quantJogadas) {
        this.quantJogadas = quantJogadas;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj.equals(this.nome)){
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString() {
        return "Nick: " + this.nome + " - Quantidade de vezes jogadas: " + this.quantJogadas;
    }
}
