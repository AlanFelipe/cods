package Exercicios.Jogadores;

import Lista.ListaEncadeada.No;
import Lista.ListaEncadeadaCircular.ListaEncadeadaCircular;

public class ListaEncadeadaJogadores extends ListaEncadeadaCircular implements IListaJogadores{

    /*@Override
    public void inserirJogador(Object elemento) throws Exception {
        incluir(elemento);
    }*/

    @Override
    public void removerJogador(String nome) throws Exception {
        int posicao = obter(nome);
        remover(posicao);
    }

    public Pessoa jogadorAtual() throws Exception {
        Pessoa pessoa = (Pessoa) getFim().getInfo();
        if(pessoa.getQuantJogadas() >= 1){
            return pessoa;
        } else {
            throw new Exception("NENHUM JOGADOR NO MOMENTO");
        }
    }

    @Override
    public Pessoa getProximoJogador() throws Exception {
        Pessoa pessoa;
        if(!estahVazia()) {
            pessoa = (Pessoa) getInicio().getInfo();
            setInicio(getInicio().getProx());
            setFim(getFim().getProx());
            pessoa.setQuantJogadas(pessoa.getQuantJogadas() + 1);
            /*while (getInicio() != null){
                pessoa = (Pessoa) getInicio().getInfo();


            }*/
            return pessoa;
        } else {
            throw new Exception("## NENHUM JOGADOR FOI CADASTRADO ##");
        }
    }

    @Override
    public int quantPessoasAFrente(String nome) throws Exception {
        int quant = 0;
        if(!estahVazia()){
            int posicao = obter(nome);
            for (int i = 0; i < posicao; i++) {
                quant++;
            }
        }
        else {
            throw new Exception("## NENHUM JOGADOR FOI CADASTRADO ##");
        }
        return quant;
    }

    @Override
    public Pessoa[] listarProximos(int quant) throws Exception {
        if(!estahVazia()){
            Pessoa[] proximo = new Pessoa[quant];
            No aux = getInicio();
            for (int i = 0; i < quant; i++) {
                proximo[i] = (Pessoa)aux.getInfo();
                aux = aux.getProx();
            }
            return proximo;
        } else {
            throw new Exception("## NENHUM JOGADOR FOI CADASTRADO ##");
        }
    }

    /*@Override
    public String listar() throws Exception {
        return super.toString();
    }*/
}
