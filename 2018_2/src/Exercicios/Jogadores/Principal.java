package Exercicios.Jogadores;

import java.util.Scanner;

public class Principal {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        //IListaJogadores lista = new ListaEncadeadaJogadores();
        IListaJogadores lista = new ListaDupEncadeadaJogadores();

        int opc;

        do{
            System.out.println("");
            System.out.println("===== MENU =====");
            System.out.println("[1] ADICIONAR JOGADORES");
            System.out.println("[2] REMOVER JOGADORES");
            System.out.println("[3] JOGADOR ATUAL");
            System.out.println("[4] OBTER PROXIMO JOGADOR");
            System.out.println("[5] PESQUISAR QUANTIDADE DE JOGADORES A FRENTE");
            System.out.println("[6] LISTAR PROXIMOS 3 JOGADORES");
            System.out.println("[7] LISTAR TODOS JOGADORES ADCIONADOS");
            System.out.println("[0] SAIR");
            System.out.print("OPÇÃO: ");
            opc = input.nextInt();

            switch (opc){
                case 1:
                    Pessoa pessoa = new Pessoa();
                    System.out.println("");
                    System.out.println("----- ADICIONAR JOGADOR -----");
                    System.out.print("Informe o nick do jogador: ");
                    String nick = input.next();
                    pessoa.setNome(nick);
                    try {
                        lista.incluir(pessoa);
                        System.out.println("Jogador adicionado...");
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                    break;
                case 2:
                    System.out.println("----- REMOVER JOGADOR -----");
                    System.out.print("Informe o nick do jogador: ");
                    String nick2 = input.next();
                    try {
                        lista.removerJogador(nick2);
                        System.out.println("Removendo jogador...");
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                    break;
                case 3:
                    System.out.println("----- JOGADOR ATUAL -----");
                    try {
                        System.out.println(lista.jogadorAtual());
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                    break;
                case 4:
                    try {
                        System.out.println(lista.getProximoJogador().getNome() + " COMECOU A JOGAR! ");
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                    break;
                case 5:
                    System.out.println("----- QUANTIDADE DE JOGADORES A FRENTE -----");
                    System.out.print("Informe o seu nick: ");
                    String nick3 = input.next();
                    try {
                        System.out.println(lista.quantPessoasAFrente(nick3) + " JOGADORES NA FRENTE");
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                    break;
                case 6:
                    try {
                        System.out.println("----- PROXIMOS JOGADORES -----");
                        Pessoa[] proximos = lista.listarProximos(3);
                        for (int i = 0; i < proximos.length; i++) {
                            Pessoa p = proximos[i];
                            imprimirJogador(p);
                        }

                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                    break;
                case 7:
                    try {
                        System.out.println("----- LISTA DE JOGADORES -----");
                        //System.out.println(lista.toString);
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                    break;
                default:
                    opc = 0;
                    System.out.println("SAINDO ...");
                    break;
            }
        }while (opc != 0);
    }

    public static void imprimirJogador(Pessoa p){
        System.out.println("Nome: " + p.getNome() +
                " Qtd Jogos: " + p.getQuantJogadas());
    }

}
