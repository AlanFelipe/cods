package Exercicios.prova2018_1.Q3;

public abstract class QuestaoProva {
    public abstract void adicionar(int valor);
    public abstract Object obterProximo(Tipo tipo);
    public abstract void excluirTodos(Tipo tipo);
    int capacidadeMaxPorTipo = 0;
    public QuestaoProva(int capacidadeMaxPorTipo){
        this.capacidadeMaxPorTipo = capacidadeMaxPorTipo;
    }
    public QuestaoProva(){

    }
}
