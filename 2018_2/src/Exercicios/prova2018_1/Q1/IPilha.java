package Exercicios.prova2018_1.Q1;

public interface IPilha {
    void empilhar(Object o) throws Exception;
    Object desempilhar() throws Exception;
    boolean estahVazia();
}
