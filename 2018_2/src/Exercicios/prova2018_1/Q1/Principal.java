package Exercicios.prova2018_1.Q1;

import java.util.Scanner;

public class Principal {
    public static void main(String[] args) {
        Principal p = new Principal();
        p.menu();
    }


    final static int TAMANHO = 10;
    static int quantA = 0;
    static int quantB = 0;

    public void menu() {
        Scanner input = new Scanner(System.in);

        Pilha pilhaA = new Pilha(TAMANHO);
        Pilha pilhaB = new Pilha(TAMANHO);

        int opc;
        do {
            System.out.println("[1] Empilhar elemento");
            System.out.println("[2] Desempilhar elemento");
            System.out.println("[3] Listar Pilhas");
            System.out.println("[4] Verificar se são iguais");
            System.out.println("[0] Sair");
            System.out.print("OPÇÃO: ");
            opc = input.nextInt();

            switch (opc) {
                case 1:
                    int opc1;
                    do {
                        System.out.println(" ");
                        System.out.println("[1]Pilha A");
                        System.out.println("[2]Pilha B");
                        System.out.println("[0]Voltar");
                        System.out.print("OPÇÃO: ");
                        opc1 = input.nextInt();

                        switch (opc1) {
                            case 1:
                                System.out.print("[PILHA A] Informe o numero: ");
                                int numA = input.nextInt();
                                try {
                                    pilhaA.empilhar(numA);
                                    quantA++;
                                    listarElementos(pilhaA);
                                } catch (Exception e) {
                                    System.out.println(e.getMessage());
                                }
                                break;
                            case 2:
                                System.out.print("[PILHA B] Informe o numero: ");
                                int numB = input.nextInt();
                                try {
                                    pilhaB.empilhar(numB);
                                    quantB++;
                                    listarElementos(pilhaB);
                                } catch (Exception e) {
                                    System.out.println(e.getMessage());
                                }
                                break;
                            default:
                                opc1 = 0;
                                break;
                        }
                    } while (opc1 != 0);
                    break;
                case 2:
                    int opc2;
                    do {
                        System.out.println("[1]Pilha A");
                        System.out.println("[2]Pilha B");
                        System.out.println("[0]Voltar");
                        System.out.print("OPÇÃO: ");
                        opc2 = input.nextInt();

                        switch (opc2) {
                            case 1:
                                try {
                                    System.out.println("Desempilhando elemento " + pilhaA.desempilhar());
                                    quantA--;
                                    listarElementos(pilhaA);
                                } catch (Exception e) {
                                    System.out.println(e.getMessage());
                                }
                                break;
                            case 2:
                                try {
                                    System.out.println("Desempilhando elemento " + pilhaB.desempilhar());
                                    quantB--;
                                    listarElementos(pilhaB);
                                } catch (Exception e) {
                                    System.out.println(e.getMessage());
                                }
                                break;
                            default:
                                opc2 = 0;
                                break;
                        }
                    } while (opc2 != 0);
                    break;
                case 3:
                    System.out.println("-- PILHA A --");
                    listarElementos(pilhaA);
                    System.out.println("-- PILHA B --");
                    listarElementos(pilhaB);
                    break;
                case 4:
                    System.out.println(" ");
                    try {
                        System.out.println("PilhaA é igual a PilhaB ? -> " + iguais(pilhaA, pilhaB) + " <-");
                    } catch (Exception e) {
                        e.getMessage();
                    }
                    break;
                default:
                    opc = 0;
                    System.out.println("Saindo...");
            }
        } while (opc != 0);
    }

    public static void listarElementos(Pilha pilha) {
        System.out.print(pilha + "\n");
    }

    public static boolean iguais(IPilha pilhaA, IPilha pilhaB) throws Exception {
        int contAux = 0;
        Object valorA, valorB;
        Pilha pilhaAuxA = new Pilha(TAMANHO);
        Pilha pilhaAuxB = new Pilha(TAMANHO);
        if (pilhaA.estahVazia() && pilhaB.estahVazia()) {
            return true;
        } else {
            if (!pilhaA.estahVazia() && !pilhaB.estahVazia()) {
                if(quantA == quantB){
                    do {
                        valorA = pilhaA.desempilhar();
                        valorB = pilhaB.desempilhar();
                        if (valorA.equals(valorB)) {
                            contAux++;
                        }
                        pilhaAuxA.empilhar(valorA);
                        pilhaAuxB.empilhar(valorB);
                    } while (!pilhaA.estahVazia());
                    for (int i = 0; i < quantA; i++) {
                        pilhaA.empilhar(pilhaAuxA.desempilhar());
                        pilhaB.empilhar(pilhaAuxB.desempilhar());
                    }
                }
                if(contAux == quantB){
                    return true;
                } else {
                    return false;
                }
            }
        }
        return false;
    }
}
