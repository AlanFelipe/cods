package Pilha;

public interface IPilha {
    void push(Object dado) throws Exception;
    Object pop() throws Exception;
    Object topo() throws Exception;
    boolean estahVazia();
}
