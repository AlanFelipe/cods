package Avaliacao20182.Q1;

import java.util.Scanner;

public class Principal {
    public static void main(String[] args) {
        Principal p = new Principal();
        p.menu();
    }


    final static int TAMANHO = 5;
    static int quantA = 0;
    static int quantB = 0;

    public void menu() {
        Scanner input = new Scanner(System.in);

        Fila filaA = new Fila(TAMANHO);
        Fila filaB = new Fila(TAMANHO);

        int opc;
        do {
            System.out.println("[1] Enfileirar elemento");
            System.out.println("[2] Desenfileirar elemento");
            System.out.println("[3] Listar Filas");
            System.out.println("[4] Verificar se são iguais");
            System.out.println("[0] Sair");
            System.out.print("OPÇÃO: ");
            opc = input.nextInt();

            switch (opc) {
                case 1:
                    int opc1;
                    do {
                        System.out.println(" ");
                        System.out.println("[1]Fila A");
                        System.out.println("[2]Fila B");
                        System.out.println("[0]Voltar");
                        System.out.print("OPÇÃO: ");
                        opc1 = input.nextInt();

                        switch (opc1) {
                            case 1:
                                System.out.print("[FILA A] Informe o numero: ");
                                int numA = input.nextInt();
                                try {
                                    filaA.queue(numA);
                                    quantA++;
                                    listarElementos(filaA);
                                } catch (Exception e) {
                                    System.out.println(e.getMessage());
                                }
                                break;
                            case 2:
                                System.out.print("[FILA B] Informe o numero: ");
                                int numB = input.nextInt();
                                try {
                                   filaB.queue(numB);
                                    quantB++;
                                    listarElementos(filaB);
                                } catch (Exception e) {
                                    System.out.println(e.getMessage());
                                }
                                break;
                            default:
                                opc1 = 0;
                                break;
                        }
                    } while (opc1 != 0);
                    break;
                case 2:
                    int opc2;
                    do {
                        System.out.println("[1]Fila A");
                        System.out.println("[2]Fila B");
                        System.out.println("[0]Voltar");
                        System.out.print("OPÇÃO: ");
                        opc2 = input.nextInt();

                        switch (opc2) {
                            case 1:
                                try {
                                    System.out.println("Removendo elemento " + filaA.deQueue());
                                    quantA--;
                                    listarElementos(filaA);
                                } catch (Exception e) {
                                    System.out.println(e.getMessage());
                                }
                                break;
                            case 2:
                                try {
                                    System.out.println("Removendo elemento " + filaB.deQueue());
                                    quantB--;
                                    listarElementos(filaB);
                                } catch (Exception e) {
                                    System.out.println(e.getMessage());
                                }
                                break;
                            default:
                                opc2 = 0;
                                break;
                        }
                    } while (opc2 != 0);
                    break;
                case 3:
                    System.out.println("-- FILA A --");
                    listarElementos(filaA);
                    System.out.println("-- FILA B --");
                    listarElementos(filaB);
                    break;
                case 4:
                    System.out.println(" ");
                    try {
                        System.out.println("Fila A é igual a Fila B ? -> " + iguais(filaA,filaB) + " <-");
                    } catch (Exception e) {
                        e.getMessage();
                    }
                    break;
                default:
                    opc = 0;
                    System.out.println("Saindo...");
            }
        } while (opc != 0);
    }

    public static void listarElementos(Fila fila) {
        System.out.print(fila + "\n");
    }

    public static boolean iguais(IFila filaA, IFila filaB) throws Exception {
        int contAux = 0;
        Object valorA, valorB;
        Fila filaAuxA = new Fila(TAMANHO);
        Fila filaAuxB = new Fila(TAMANHO);
        if (filaA.estahVazia() && filaB.estahVazia()) {
            return true;
        } else {
            if (!filaA.estahVazia() && !filaB.estahVazia()) {
                if(quantA == quantB){
                    do {
                        valorA = filaA.deQueue();
                        valorB = filaB.deQueue();
                        if (valorA.equals(valorB)) {
                            contAux++;
                        }
                        filaAuxA.queue(valorA);
                        filaAuxB.queue(valorB);
                    } while (!filaA.estahVazia());
                    for (int i = 0; i < quantA; i++) {
                        filaA.queue(filaAuxA.deQueue());
                        filaB.queue(filaAuxB.deQueue());
                    }
                }
                if(contAux == quantB){
                    return true;
                } else {
                    return false;
                }
            }
        }
        return false;
    }
}
