package Avaliacao20182.Q1;

public class Fila implements IFila {
    private Object[] dados;
    private int iniFila;
    private int fimFila;

    public Fila() {
        this.iniFila = 0;
        this.fimFila = 0;
    }

    public Fila(int capacidade) {
        this.dados = new Object[capacidade];
        this.iniFila = 0;
        this.fimFila = 0;
    }

    public Fila(Object[] dados, int iniFila, int fimFila) {
        this.dados = dados;
        this.iniFila = iniFila;
        this.fimFila = fimFila;
    }

    @Override
    public void queue(Object dado) throws Exception {
        aumentaCapacidade();
        if(this.fimFila < this.dados.length){
            this.dados[fimFila] = dado;
            this.fimFila++;
        } else {
            throw new Exception("## ERRO: A FILA ESTA CHEIA ##");
        }
    }

    @Override
    public Object deQueue() throws Exception {
        if(!estahVazia()){
            int inicioFila = 0;
            Object dadoRemovido = this.dados[inicioFila];
            for (int i = 0; i < this.fimFila - 1; i++) {
                this.dados[i] = this.dados[i+1];
            }
            this.fimFila--;
            return dadoRemovido;
        } else {
            throw new Exception("## A FILA ESTA VAZIA ##");
        }
    }

    @Override
    public boolean estahVazia() {
        if(this.fimFila == iniFila){
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int tamanho() {
        return (this.fimFila - this.iniFila);
    }

    private void aumentaCapacidade(){
        if(this.fimFila == this.dados.length){
            Object[] dadosNovos = new Object[this.dados.length * 2];
            for (int i = 0; i < this.dados.length; i++) {
                    dadosNovos[i] = this.dados[i];
            }
            this.dados = dadosNovos;

        }
    }

    private void organizar(){
        for (int i = 0; i < this.fimFila - 1; i++) {
            if(iniFila < fimFila) {
                this.dados[i] = this.dados[iniFila];
                this.iniFila++;
            }
        }
        this.iniFila = 0;
    }

    public Object[] getDados() {
        return dados;
    }

    public void setDados(Object[] dados) {
        this.dados = dados;
    }

    public int getIniFila() {
        return iniFila;
    }

    public void setIniFila(int iniFila) {
        this.iniFila = iniFila;
    }

    public int getFimFila() {
        return fimFila;
    }

    public void setFimFila(int fimFila) {
        this.fimFila = fimFila;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < this.fimFila; i++) {
            result.append("[" + this.dados[i] + "]");
        }
        return result.toString();
    }
}
