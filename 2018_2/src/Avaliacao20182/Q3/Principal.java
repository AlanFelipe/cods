package Avaliacao20182.Q3;

import java.util.Arrays;
import java.util.Scanner;

public class Principal {
    String[] palavras = new String[10];
    int quantidade = 0;

    public static void main(String[] args) throws Exception {
        Principal p = new Principal();

        Scanner input = new Scanner(System.in);

        for (int i = 0; i < 10; i++) {
            System.out.println("Informe: ");
            String palavra = input.nextLine();
            p.push(palavra);
        }

        quicksort(p.palavras,0,p.palavras.length);

        for (int i = 0; i < 10; i++) {
            System.out.println(p.palavras[i]);
        }
    }

    public void push(String dado) throws Exception {
        if(this.quantidade < this.palavras.length){
            this.palavras[this.quantidade] = dado;
            this.quantidade++;
        } else {
            throw new Exception("## ERRO: A PILHA ESTA CHEIA ##");
        }
    }

    public static String[] quicksort(String v[],int direita,int esquerda){
        int esq = esquerda;
        int dir = direita;
        String pivo = v[(esq+dir)/2];
        String troca;

        while(esq<=dir){
            while(v[esq].compareTo(pivo)<0){
                esq = esq +1;

            }
            while(v[dir].compareTo(pivo)>0){
                dir = dir -1;
            }
            if(esq<=dir){
                troca = v[esq];
                v[esq] = v[dir];
                v[dir] = troca;
                esq = esq+1 ;
                dir = dir +1;
            }
        }
        if(dir>esquerda){
            quicksort(v,esquerda,dir);
        }
        if(esq<direita){
            quicksort(v,esq,direita);
        }
        return v;

    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < this.quantidade; i++) {
            result.append("[" + this.palavras[i] + "]");
        }
        return result.toString();
    }
}
