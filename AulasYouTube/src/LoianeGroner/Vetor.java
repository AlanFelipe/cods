package LoianeGroner;

import java.util.Arrays;

public class Vetor<T> {
    private T[] elementos;
    private int tamanho;

    public Vetor(int capacidade){
        this.elementos = (T[])new Object[capacidade];
        this.tamanho = 0;
    }

    /*public Vetor(int capacidade, Class<T> tipoClasse){
        this.elementos = (T[])Array.newInstance(tipoClasse, capacidade);
        this.tamanho = 0;
    }*/

    /*public void adiciona(String elemento){ //adiciona elementos no vetor
        for (int i = 0; i < this.elementos.length; i++) {
            if(this.elementos[i] == null){ //verifica se a posição esta vazia(nula)
                this.elementos[i] = elemento;
                break;
            }
        }
    }
    */

    /*public void adiciona(String elemento) throws Exception{
        if(this.tamanho < elementos.length){
            this.elementos[this.tamanho] = elemento;
            this.tamanho++;
        } else {
            throw new Exception("O vetor ja está cheio");
        }
    }
    */

    public boolean adiciona(T elemento) { //adicionar elemento no vetor
        aumentaCapacidade();
        if(this.tamanho < elementos.length){
            this.elementos[this.tamanho] = elemento;
            this.tamanho++;
            return true;
        } else {
            return false;
        }
    }

    public boolean adiciona(int posicao, T elemento){
        aumentaCapacidade();
        if(!(posicao >= 0 && posicao < tamanho)){ //quando a posição não poder ser acessada
            throw new IllegalArgumentException("Posição Invalida"); //lanca o erro
        }

        //mover todos elementos
        for (int i = this.tamanho-1; i >= posicao; i--) {
            this.elementos[i+1] = this.elementos[i];
        }
        this.elementos[posicao] = elemento;
        this.tamanho++;

        return false;
    }

    public void remover(int posicao){
        if(!(posicao >= 0 && posicao < tamanho)){ //quando a posição não poder ser acessada
            throw new IllegalArgumentException("Posição Invalida"); //lanca o erro
        }

        for (int i = posicao; i < this.tamanho - 1; i++) {
            this.elementos[i] = this.elementos[i+1];
        }
        this.tamanho--;
    }

    public void listar(){
        for (int i = 0; i < this.tamanho; i++) {
            System.out.print(elementos[i] + " ");
        }
    }

    public Object busca(int posicao){
        if(!(posicao >= 0 && posicao < tamanho)){ //quando a posição não poder ser acessada
            throw new IllegalArgumentException("Posição Invalida"); //lanca o erro
        }
        return this.elementos[posicao];
    }

    public boolean busca(Object elemento){ //informa se o elemento existe
        for (int i = 0; i < this.tamanho; i++) {
            if(this.elementos[i].equals(elemento)){
                return true; // retorna verdadeiro se existir
            }
        }
        return false;
    }

    /*public int busca(String elemento){ //informa se o elemento existe
        for (int i = 0; i < this.tamanho; i++) {
            if(this.elementos[i].equals(elemento)){
                return i; // retorna a posicao
            }
        }
        return -1;
    }*/

    private void aumentaCapacidade(){ //aumentar a capacidade do vetor
        if(this.tamanho == this.elementos.length){
            T[] elementosNovos = (T[]) new Object[this.elementos.length*2];
            for (int i = 0; i < this.elementos.length; i++) {
                elementosNovos[i] = this.elementos[i];
            }
            this.elementos = elementosNovos;
        }
    }

    @Override
    public String toString() {
        return "Vetor{" +
                "elementos=" + Arrays.toString(elementos) +
                '}';
    }
}
