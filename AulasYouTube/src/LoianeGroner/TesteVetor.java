package LoianeGroner;

import java.util.Scanner;

public class TesteVetor { public static void main(String[] args) {
    Scanner input = new Scanner(System.in);

    Vetor<String> vet = new Vetor(10);
    int opc;

    do{
        System.out.println(" ");
        System.out.println("[1] Adicionar elementos");
        System.out.println("[2] Listar elementos");
        System.out.println("[3] Buscar elementos");
        System.out.println("[4] Verificar se o elemento existe");
        System.out.println("[5] Acidionar elemento em qualquer posicao");
        System.out.println("[6] Remover elemento");
        System.out.println("[0] Sair");
        System.out.print("Opcao: ");
        opc = input.nextInt();

        switch (opc){
            case 1:
                System.out.print("Informe o elemento: ");
                String elemento = input.next();
                vet.adiciona(elemento);
                break;
            case 2:
                vet.listar();
                break;
            case 3:
                System.out.print("Informe a posicao: ");
                int posi = input.nextInt();
                System.out.println(vet.busca(posi));
                break;
            case 4:
                System.out.print("Informe o elemento: ");
                String ele = input.next();
                System.out.println(vet.busca(ele));
                break;
            case 5:
                System.out.print("Informe a posicao desejada: ");
                int pos = input.nextInt();
                System.out.print("Informe o elemento: ");
                String elem = input.next();
                vet.adiciona(pos,elem);
                break;
            case 6:
                System.out.print("Informe a posicao: ");
                int po = input.nextInt();
                vet.remover(po);
            default:
                opc = 0;
                System.out.println("Saindo...");
                break;
        }
    }while(opc != 0);
}
}
