package LoianeGroner.TratamentoErros;

public class TestandoFinally {
    public static void main(String[] args) {
        int[] num = {4, 8, 16, 24, 32, 64, 128};
        int[] denom = {2, 0, 4, 8, 0};

        for (int i = 0; i < num.length; i++) {
            try{
                System.out.println(num[i] + "/" + denom[i] + " = " + (num[i]/denom[i]));
            } catch (ArithmeticException e1){ //ou usar Throwable pra exception
                System.out.println("Erro ao dividir por zero");
            } catch (ArrayIndexOutOfBoundsException e1){
                System.out.println("Posição Invalida");
            } finally {
                System.out.println("Essa linha é impressa sempre após o try ou catch");
            }
        }
    }
}
