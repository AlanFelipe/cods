package LoianeGroner.TratamentoErros;

public class Excecao  {
    public static void main(String[] args) {

        try{
            int[] vet  = new int[4];
            System.out.println("Antes da exception");

            vet[4] = 1;

            System.out.println("Esse texto nao sera impresso");
        } catch (ArrayIndexOutOfBoundsException exception){
            System.out.println("Exceção ao acessar um indice do vetor que nao existe");
        }

        System.out.println("Esse texte sera impresso apos a exception");
    }
}