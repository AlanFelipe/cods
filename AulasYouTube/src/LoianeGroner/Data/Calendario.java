package LoianeGroner.Data;

import java.util.Calendar;

public class Calendario {
    public static void main(String[] args) {
        Calendar hoje = Calendar.getInstance();

        int ano = hoje.get(Calendar.YEAR); //ano
        int mes = hoje.get(Calendar.MONTH); //mes
        int dia = hoje.get(Calendar.DAY_OF_MONTH); //dia
        int hora = hoje.get(Calendar.HOUR_OF_DAY); //hora
        int min = hoje.get(Calendar.MINUTE); //minutos
        int segun = hoje.get(Calendar.SECOND); //segundos

        hoje.add(Calendar.DAY_OF_MONTH, 1); // adicionar data

        System.out.printf("Hoje é : %02d/%02d/%d", dia, (mes+1), ano);
    }
}
