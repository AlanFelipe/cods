package LoianeGroner;

import java.util.Stack;

public class TorreHanoi {
    public static void main(String[] args) {
        Stack<Integer> pilhaOrig = new Stack<>();
        Stack<Integer> pilhaDest = new Stack<>();
        Stack<Integer> pilhaAux = new Stack<>();

        pilhaOrig.push(3);
        pilhaOrig.push(2);
        pilhaOrig.push(1);

        torreDeHanoi(pilhaOrig.size(), pilhaOrig, pilhaDest, pilhaAux);
    }

    public static void torreDeHanoi(int n, Stack<Integer> orig, Stack<Integer> dest, Stack<Integer> aux){
        if(n > 0){
            torreDeHanoi(n-1, orig, aux, dest);
            dest.push(orig.pop());
            System.out.println("-------");
            System.out.println("Original: " + orig);
            System.out.println("Destino: " + dest);
            System.out.println("Auxiliar: " + aux);
            torreDeHanoi(n-1, aux, dest, orig);
        }
    }
}
