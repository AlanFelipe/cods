package DevDojo.File;

import java.io.File;
import java.io.IOException;
import java.util.Date;

public class FileTest {
    public static void main(String[] args) {
        File file = new File("Arquivo.txt");
        try {
            System.out.println(file.createNewFile()); //criar o arquivo, imprime se foi criado ou não
            boolean existe = file.exists(); // verificar se o arquivo existe
            if(existe){
                System.out.println(file.delete()); // deletar o arquivo
                System.out.println(file.canRead()); //saber se tem permição de leitura
                System.out.println(file.getPath()); //saber o path dele
                System.out.println(file.getAbsolutePath()); //saber todoo path que ele esta
                System.out.println(file.isDirectory()); //saber se é um diretorio
                System.out.println(file.isHidden()); //saber se é um arquivo oculto
                System.out.println(new Date(file.lastModified())); //saber quando foi modificado pela ultima vez
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
