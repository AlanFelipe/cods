package DevDojo.File;

import java.io.File;
import java.io.IOException;

public class DiretoriosTest {
    public static void main(String[] args) {
        File diretorio =  new File("arquivo");
        System.out.println(diretorio.mkdir()); //criando diretorio
        File arquivo = new File("D:\\Codigos IntelliJ\\AulasYouTube\\arquivo\\Arquivo.txt"); //ou diretorio,arquivo.txt
        File arquivoNovo = new File(diretorio,"arquivoNovo.txt");
        arquivo.renameTo(arquivoNovo); //renomear arquivo
        try {
            arquivo.createNewFile(); //criando arquivo na pasta
        } catch (IOException e) {
            e.printStackTrace();
        }
        buscarArquivos();

    }

    public static void buscarArquivos(){
        File file = new File("arquivo");
        String[] list = file.list();
        for (String arquivo: list) {
            System.out.println(arquivo);
        }
    }

}
