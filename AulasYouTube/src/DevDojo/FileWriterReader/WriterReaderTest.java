package DevDojo.FileWriterReader;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class WriterReaderTest {
    public static void main(String[] args) {
        File file = new File("Arquivo.txt");
        try {
            FileWriter fw = new FileWriter(file); // escrever no arquivo
            fw.write("Escrevendo mensagem no arquivo \n" +
                    "pulando uma linha");
            fw.flush(); //bota tudo no arquivo
            fw.close();

            FileReader fr = new FileReader(file); //ler arquivo
            char[] in = new char[500];
            int size = fr.read(in); // saber quantos caracteres foram lidos
            System.out.println("tamanho: " + size);
            for (char c : in) {
                System.out.print(c);
            }
            fr.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
