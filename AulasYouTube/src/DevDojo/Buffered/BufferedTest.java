package DevDojo.Buffered;

import java.io.*;

public class BufferedTest {
    public static void main(String[] args) {

    File file = new File("Arquivo.txt");
    try {
        FileWriter fw = new FileWriter(file); // escrever no arquivo
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write("Escrevendo mensagem no arquivo");
        bw.newLine(); //pulando linha
        bw.write("pulando uma linha");
        bw.flush(); //bota tudo no arquivo
        bw.close();

        FileReader fr = new FileReader(file); //ler arquivo
        BufferedReader br = new BufferedReader(fr);
        String a = null;
        while ((a = br.readLine()) != null){
            System.out.println(a);
        }
        br.close();
    } catch (IOException e) {
        e.printStackTrace();
    }
}
}

