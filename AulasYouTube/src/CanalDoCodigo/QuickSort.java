package CanalDoCodigo;

import java.util.Arrays;

public class QuickSort {
    private static void quickSort(int[] vetor, int esq, int dir) {
        if(esq < dir){
            int j = separar(vetor, esq, dir);
            quickSort(vetor,esq,j-1);
            quickSort(vetor,j+1,dir);
        }
    }

    private static int separar(int[] vetor, int esq, int dir) {
        int i = esq;
        int j = dir;

        while (i < j){
            while (i < dir && vetor[i] <= vetor[esq]){
                i++;
            }
            while (j > esq && vetor[j] >= vetor[esq]){
                j--;
            }
            if(i < j){
                trocar(vetor,i,j);
                i++;
                j--;
            }
        }
        trocar(vetor,esq,j);
        return j;
    }

    private static void trocar(int[] vetor, int i, int j) {
        int aux = vetor[i];
        vetor[i] = vetor[j];
        vetor[j] = aux;
    }

    public static void main(String[] args) {
        int[] vet = {6,3,4,5,2,7,1,9,8,0};
        quickSort(vet, 0, vet.length-1);
        System.out.println(Arrays.toString(vet));
    }
}
