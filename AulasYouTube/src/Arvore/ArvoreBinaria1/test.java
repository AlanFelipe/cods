package Arvore.ArvoreBinaria1;

import Arvore.Elemento;

public class test {
    public static void main(String[] args) {
        ArvoreBinaria arvore = new ArvoreBinaria(new Elemento(10));
        arvore.inserir(new Elemento(5));
        arvore.inserir(new Elemento(1));
        arvore.inserir(new Elemento(8));
        arvore.inserir(new Elemento(15));
        arvore.inserir(new Elemento(12));
        arvore.inserir(new Elemento(18));
    }
}
