package Arvore.ArvoreBinaria1;


import Arvore.Elemento;

public class ArvoreBinaria {
    private Elemento raiz;
    private ArvoreBinaria esq;
    private ArvoreBinaria dir;

    public ArvoreBinaria(){
        inicializar();
    }

    public ArvoreBinaria(Elemento elemento) {
        this.raiz = elemento;
        System.out.println("Criando arvore com o valor: " + elemento);
    }

    private void inicializar(){
        this.raiz = null;
    }

    public void inserir(Elemento novo){
        if(estahVazia()){
            this.raiz = novo;
        } else {
            ArvoreBinaria novaArvore = new ArvoreBinaria(novo);
            if(novo.getValor() < this.raiz.getValor()){
                if(this.esq == null){
                    this.esq = novaArvore;
                    System.out.println("inserindo o elemento " + novo.getValor() + "à esquerda de " + this.raiz.getValor());
                } else {
                    this.esq.inserir(novo);
                }
            } else if(novo.getValor() > this.raiz.getValor()){
                if(this.dir == null){
                    this.dir = novaArvore;
                    System.out.println("inserindo o elemento " + novo.getValor() + "à direita de " + raiz.getValor());
                } else {
                    this.dir.inserir(novo);
                }
            }
        }
    }

    public boolean estahVazia(){
        return (this.raiz == null);
    }
}
