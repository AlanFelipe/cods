public class CriadorDocumento implements ICriadorDocumento{

    @Override
    public IDocumento criarDocumento(Tipo tipo) {
        switch (tipo){
            case HTML:
                return new HTML();
            case Markdown:
                return new Markdown();
            case LaTeX:
                return new LaTeX();
                default:
                    break;
        }
        return null;
    }
}
