package Lista2.PilhaEncadeada;


public class Pilha implements IPilha{

    private No inicio;
    private No topo;
    private int tamanho;

    public Pilha() {
        this.inicio = null;
        this.topo = null;
        this.tamanho = 0;
    }

    public Pilha(No inicio, No topo, int tamanho) {
        this.inicio = inicio;
        this.topo = topo;
        this.tamanho = tamanho;
    }

    @Override
    public void push(Object dado) throws Exception {
        if(this.inicio == null){
            No novo = new No();
            novo.setInfo(dado);
            novo.setProx(null);
            this.inicio = novo;
            this.topo= this.inicio;
        } else {
            No novo = new No();
            novo.setInfo(dado);
            novo.setProx(null);
            this.topo.setProx(novo);
            this.topo = novo;
        }
        this.tamanho++;
    }

    @Override
    public Object pop() throws Exception {
        No objeto;

        if (!estahVazia()) {
            if (this.topo == this.inicio) {
                objeto = this.inicio;
                this.inicio = null;
                this.topo = null;
                this.tamanho--;
            } else {
                No aux, aux2;
                aux = this.inicio;
                aux2 = aux;
                while (aux.getProx() != null) {
                    aux2 = aux;
                    aux = aux.getProx();
                }
                aux2.setProx(null);
                objeto = aux;
                this.topo = aux2;
                this.topo = aux2;
                this.tamanho--;
            }
        } else {
            throw new Exception("## A PILHA ESTÁ VAZIA ##");
        }

        return objeto;
    }

    @Override
    public Object topo() throws Exception {
        return this.topo.getInfo();
    }

    @Override
    public boolean estahVazia() {
        if(this.tamanho == 0){
            return true;
        } else {
            return false;
        }
    }

    public No getInicio() {
        return inicio;
    }

    public void setInicio(No inicio) {
        this.inicio = inicio;
    }

    public No getTopo() {
        return topo;
    }

    public void setTopo(No topo) {
        this.topo = topo;
    }

    public int getTamanho() {
        return tamanho;
    }

    public void setTamanho(int tamanho) {
        this.tamanho = tamanho;
    }

    @Override
    public String toString() {
        return this.inicio.toString();
    }
}
