package Lista2.ListaCircular;

public class ListaCircular implements ILista {
    private No inicio;
    private No fim;
    private int tamanho;
    private boolean inicializar;

    public ListaCircular() {
        inicializar();
    }

    public ListaCircular(No inicio, No fim, int tamanho, boolean inicializar) {
        this.inicio = inicio;
        this.fim = fim;
        this.tamanho = tamanho;
        this.inicializar = inicializar;
    }

    @Override
    public void inicializar() {
        this.inicio = null;
        this.fim = null;
        this.tamanho = 0;
        this.inicializar = true;
    }

    @Override
    public void incluir(Object elemento) throws Exception {
        if(this.inicio == null){
            No novo = new No(elemento);
            novo.setProx(novo);
            this.inicio = novo;
            this.fim = novo;
        } else {
            No novo = new No(elemento);
            this.fim.setProx(novo);
            novo.setProx(this.inicio);
            this.fim = novo;
        }
        this.tamanho++;
    }

    @Override
    public void incluirInicio(Object elemento) throws Exception {
        if(this.inicio == null){
            No novo = new No(elemento);
            novo.setProx(novo);
            this.inicio = novo;
            this.fim = this.inicio;
        } else {
            No novo = new No(elemento);
            this.fim.setProx(novo);
            novo.setProx(this.inicio);
            this.inicio = novo;
        }
        this.tamanho++;
    }

    @Override
    public void incluir(Object elemento, int posicao) throws Exception {
        No novo = new No(elemento);
        No anterior = null;
        No aux = this.inicio;
        int cont = 0;

        if (posicao == 0) {
            incluirInicio(elemento);
        } else if (posicao == (this.tamanho)) {
            incluir(elemento);
        } else if (posicao > this.tamanho) {
            incluir(elemento);
            throw new Exception("## ERRO: POSIÇÃO INVALIDA, FOI ADCIONADO NO FINAL DA LISTA ##");
        } else {
            while ((aux != null) && (cont != posicao)){
                anterior = aux;
                aux = aux.getProx();
                cont++;
            }
            anterior.setProx(novo);
            novo.setProx(aux);
            this.tamanho++;
        }
    }

    @Override
    public Object obterDaPosicao(int posicao) throws Exception {
        if(!estahVazia()){
            if(posicao < this.tamanho){
                No aux = this.inicio;
                for (int i = 1; i <= posicao; i++) {
                    aux = aux.getProx();
                }
                return aux.getInfo();
            } else {
                throw new Exception("## ERRO: POSIÇÃO INVALIDA ##");
            }
        } else {
            throw new Exception("## A LISTA ESTÁ VAZIA ##");
        }
    }

    @Override
    public int obter(Object item) throws Exception {
        No aux = this.inicio;
        for (int i = 0; i < this.tamanho; i++) {
            if(aux.getInfo().equals(item)){
                return i;
            }
            aux = aux.getProx();
        }
        throw new Exception("## ERRO: NÃO EXISTE ESTE ELEMENTO NA LISTA ##");
    }

    @Override
    public void remover(int posicao) throws Exception {
        if(!estahVazia()){
            No aux = this.inicio;
            No anterior = null;

            if (posicao == 0) {
                this.removeInicio();
            } else if (posicao == this.tamanho - 1) {
                this.removeFim();
            } else if (posicao >= this.tamanho) {
                throw new Exception("## ERRO: POSIÇÃO INVALIDA ##");
            } else {
                for (int i = 0; i < posicao; i++) {
                    anterior = aux;
                    aux = aux.getProx();
                }
                anterior.setProx(aux.getProx());
                this.tamanho--;
            }
        } else {
            throw new Exception("## A LISTA ESTÁ VAZIA ##");
        }
    }

    public void removeInicio() throws Exception {
        if (!estahVazia()) {
            No aux, aux2;
            aux = this.inicio;
            aux2 = this.inicio;
            while(aux.getProx() != this.inicio){
                aux = aux.getProx();
            }
            aux.setProx(aux2.getProx());
            this.inicio = this.inicio.getProx();
            this.fim.setProx(aux.getProx());
            this.tamanho--;
        } else {
            throw new Exception("## A LISTA ESTÁ VAZIA ##");
        }
    }

    public void removeFim() throws Exception {
        if (!estahVazia()) {
            No aux, aux2;
            aux = this.inicio;
            aux2 = null;
            while (aux.getProx() != this.inicio) {
                aux2 = aux;
                aux = aux.getProx();
            }
            aux2.setProx(aux.getProx());
            this.fim = aux2;
            this.tamanho--;
        } else {
            throw new Exception("## A LISTA ESTÁ VAZIA ##");
        }
    }


    @Override
    public void limpar() throws Exception {
        if(this.tamanho != 0){
            inicializar();
            System.out.println("REMOVENDO TODOS OS DADOS...");
        } else {
            throw new Exception("## A LISTA ESTÁ VAZIA ##");
        }
    }

    @Override
    public int getTamanho() throws Exception {
        return this.tamanho;
    }

    @Override
    public boolean contem(Object item) throws Exception {
        No aux = this.inicio;
        for (int i = 0; i < this.tamanho; i++) {
            if(aux.getInfo().equals(item)){
                return true;
            }
            aux = aux.getProx();
        }
        throw new Exception("## ERRO: NÃO CONTÉM ESTE ELEMENTO NA LISTA ##");
    }

    @Override
    public boolean verificarInicializacao() throws Exception {
        if (this.inicializar) {
            return true;
        } else {
            throw new Exception("## ERRO: A LISTA NÃO FOI INICIALIZADA ##");
        }
    }

    @Override
    public boolean estahCheia() throws Exception {
        return false;
    }

    public boolean estahVazia() throws Exception {
        if(this.tamanho == 0){
            return true;
        } else {
            return false;
        }
    }

    public Object getInicio() {
        if( this.inicio != null ) {
            return this.inicio.getInfo();
        } else {
            return null;
        }
    }

    public void setInicio(No inicio) {
        this.inicio = inicio;
    }

    public No getFim() {
        return fim;
    }

    public void setFim(No fim) {
        this.fim = fim;
    }

    public void setTamanho(int tamanho) {
        this.tamanho = tamanho;
    }

    public boolean isInicializar() {
        return inicializar;
    }

    public void setInicializar(boolean inicializar) {
        this.inicializar = inicializar;
    }

    public String toString(){
        if(this.tamanho != 0){
            StringBuilder s = new StringBuilder();

            No aux = this.inicio;

            if (aux == this.inicio) {
                s.append("[" + aux.getInfo() + "]" + "\n");
                aux = aux.getProx();
            }

            while (aux != this.inicio) {
                s.append("[" + aux.getInfo() + "]" + "\n");
                aux = aux.getProx();

            }

            return s.toString();
        } else {
            return "## A LISTA ESTÁ VAZIA ##";
        }
    }
}
