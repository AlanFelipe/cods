package Lista2.ListaCircular;

public interface ILista {
    void inicializar();
    void incluir(Object elemento) throws Exception;
    void incluirInicio(Object elemento) throws Exception;
    void incluir(Object elemento, int posicao) throws Exception;
    Object obterDaPosicao(int posicao)  throws Exception ;
    int obter(Object item)  throws Exception ;
    void remover(int posicao) throws Exception ;
    void limpar() throws Exception ;
    int getTamanho()  throws Exception ;
    boolean contem(Object item) throws Exception;
    boolean verificarInicializacao()  throws Exception;
    boolean estahCheia() throws Exception;
}
